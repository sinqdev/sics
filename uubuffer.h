/*---------------------------------------------------------------------------
                          U U B U F F E R
                          
    uubuffer defines one function which encodes a given buffer using
    the uuencode algorithm. 
    
    Mark Koennecke, April 1998
-----------------------------------------------------------------------------*/
#ifndef UUBUFFER
#define UUBUFFER

int UUencodeBuffer(void *pBuffer, int iBufLen, char *pName,
                   void **pEncoded, int *iLength);

   /*
      - pBuffer        is the buffer to encode
      - iBufLen        is the length of buffer to encode
      - pName          is the name to write instead of a filename
      - *pEncoded      is the pointer to the encoded buffer
      - *iLength       is the length of the encoded buffer.
      On success, a 1 will be returned, 0 else.
      UUencodeBuffer allocates memory for pEncoded. It is the callers
      responsability to free this memory after use.
    */

#endif
