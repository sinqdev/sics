
/*----------------------------------------------------------------------
  Internal data structures for the XYTable class. Do not modify
------------------------------------------------------------------------*/


    typedef struct {
                     float x;
                     float y;
                   } TableEntry, *pTableEntry;

    typedef struct __XYTABLE {
                              pObjectDescriptor pDes;
                              int iList;            
                              int iCount;
                             }XYTable;

