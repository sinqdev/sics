/*
 * This is the trace facility for SICS. En detail logging for debugging complex
 * problems and more.
 *
 * trace.h
 *
 *  Created on: Apr 27, 2011
 *      Author: koennecke
 */

#ifndef TRACE_H_
#define TRACE_H_
#include <stdarg.h>

/**
 * printf style functions for printing trace information
 * from various sub systems. The id field is something like
 * the socket number, a host:port or something appropriate
 * which tells us where the stuff is going to or coming from.
 */
#if __GNUC__ > 2
#define G_GNUC_PRINTF( format_idx, arg_idx )    \
  __attribute__((__format__ (__printf__, format_idx, arg_idx)))
#else
#define G_GNUC_PRINTF( format_idx, arg_idx )
#endif
void traceIO(char *id, char *format, ...) G_GNUC_PRINTF (2, 3);
void traceDevice(char *id, char *format, ...) G_GNUC_PRINTF (2, 3);
void tracePar(char *id, char *format, ...) G_GNUC_PRINTF (2, 3);
void traceSys(char *id, char *format, ...) G_GNUC_PRINTF (2, 3);
void traceprint(char *sub, char *id,char *data);

/**
 * A debugging trace. This has to be switched on separately
 */
void traceDebug(char *id, char *format, ...) G_GNUC_PRINTF (2, 3);
#undef G_GNUC_PRINTF

/*
 * 1 when tracing active, 0 else
 */
int traceActive();

#endif /* TRACE_H_ */
