\subsubsection{Simulated histogram memory driver}
This is a driver for a simulated histogram memory needed for software
testing.

@d Protos @{
  pHistDriver CreateSIMHM(pStringDict pOpt);
@}

The only visible function defined is a creation function. Please note that
all the interface functions  need to be defined as well. For implementation
of the countable interface this thing will use the simulated counter. All
data retrieval members will return random numbers in the required amount.
The call to configure will actually do nothing.

@o histsim.h -d @{
/*--------------------------------------------------------------------------
			H I S T S I M

  A simulated histogram memory for software testing.

  Mark Koennecke, April 1997

  copyright: see implementation file.
---------------------------------------------------------------------------*/
#ifndef SICSHISTSIM
#define SICSHISTSIM
@<Protos@>
#endif
@} 

