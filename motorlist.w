\subsection{The Motorlist Module}
The motorlist is e helper module for implementing complex movements of 
multiple motors. A good example is the coordination of the reflectometer AMOR.
The general idea is to have a list (using the lld implementation used in
SICS) which contains one of the  following data structure for each motor
to run:

@d motlistmot @{
typedef struct{
	char name[80];
	float target;
	float position;
	pIDrivable pDriv;
	void *data;
	int running;
}MotControl, *pMotControl;
@}

The motorlist module then takes care of starting all these motors,
 checking their status etc. A client module then only needs to calculate 
 targets for its motors and populate a list with them. All other 
 drivable tasks will then be performed by motorlist.
 
The interface provided by this module looks like this:
@d motlistint @{
pIDrivable makeMotListInterface();
int addMotorToList(int listHandle, char *name, float targetValue);
int setNewMotorTarget(int listHandle, char *name, float value);
int getMotorFromList(int listHandle, char *name, pMotControl tuk);
float      getListMotorPosition(int listHandle, char *name);
void printMotorList(int listHandle, SConnection *pCon); 
@}
\begin{description}
\item[makeMotListInterface] creates a drivable interface which is initialized
 with the motlist implementations. Each of the drivabel functions expects 
 as pData pointer a pointer to the listHandle describing the list of motors
 to run
 \item[addMotorToList] adds motor name with target value targetValue to
  the list identified by listHandle. Retruns 1 on success, 0 on failure (The 
  motor could not be found)
\item[setNewMotorTarget] does what it says. 
\item[getMotorFromList] rets a motor entry for name from listHandle. Used in 
order to retrieve positions.
\item[getListMotorPosition] retrieves the current position of motor name.
\end{description}
All the rest of the interface is invoked through the drivable interface 
functions which thus can be used in implementing own drivable interfaces.

@o motorlist.h @{
/*----------------------------------------------------------------------
  Support module which manages a list of motors and their target values
  when running complex movements. See accompanying tex file for
  more info.

  copyright: see file COPYRIGHT

  Mark Koennecke, September 2005
-----------------------------------------------------------------------*/
#ifndef SICSMOTLIST
#define SICSMOTLIST
#include "sics.h"
@<motlistmot@>
/*======================================================================*/
@<motlistint@>
#endif

@}
