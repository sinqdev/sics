
/*-----------------------------------------------------------------------
                     N X S C R I P T

  This is a class for scripting the contents of NeXus files from
  SICS.

  copyright: see file COPYRIGHT

  Mark Koennecke, February 2003
------------------------------------------------------------------------*/
#ifndef NXSCRIPT
#define NXSCRIPT
#include "nexus/napi.h"
#include "nxdict.h"

int MakeNXScript(SConnection * pCon, SicsInterp * pSics, void *pData,
                 int argc, char *argv[]);
int NXScriptAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                   int argc, char *argv[]);

char *makeFilename(SicsInterp * pSics, SConnection * pCon);
void changeExtension(char *filename, char *newExtension);
int isNXScriptWriting(void);
/*==============  a personal data structure ============================*/
typedef struct {
  pObjectDescriptor pDes;
  NXhandle fileHandle;
  NXdict dictHandle;
  int timeDivisor;
} NXScript, *pNXScript;

#endif
