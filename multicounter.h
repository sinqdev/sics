/**
 * The MultiCounter is another counter which coordinates multiple 
 * counting objects, counters and histogram memories. It also calls a 
 * script function after TransferData which collects counters and monitors.
 * The purpose is to have a flexible counter abstraction for upper level
 * code such as maximizers and scan functions. The script can deal with 
 * counting on monitors or on sums of histogram memories. 
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koennecke, September 2006
 */
#ifndef MULTICOUNTER_H_
#define MULTICOUNTER_H_
#include "sics.h"

int MakeMultiCounter(SConnection * pCon, SicsInterp * pSics,
                     void *pData, int argc, char *argv[]);
#endif                          /*MULTICOUNTER_H_ */
