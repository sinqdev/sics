/**
 * This is a second generation histogram memory object. In contrast 
 * to counters and motors no attempt is made to provide backwards 
 * compatability with the old SICS way of doing things. This 
 * histogram memory object quite sensibly is derived from 
 * the counter object. It adds a rank, dimensions and variable 
 * data and  a configuration method. For TOF, methods and variables 
 * for generating and maintaining time binnings are provided too.
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koennecke, May 2009
 */
#ifndef HISTMEMSEC_H_
#define HISTMEMSEC_H_
#include <sics.h>
int MakeSecHM(SConnection * pCon, SicsInterp * pSics, void *pData,
                int argc, char *argv[]);

const float *GetSecHistTimeBin(pHistMem self, int *iLength);
HistInt *GetSecHistogramPointer(pHistMem self,SConnection *pCon);
int GetSecHistogram(pHistMem self, SConnection *pCon,
                    int i,int iStart, int iEnd, HistInt *lData, int iDataLen);
void SecHistDirty(pHistMem self);        
int GetSecHistLength(pHistMem self);


#endif /*HISTMEMSEC_H_*/
