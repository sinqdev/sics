#include <signal.h>
#include <errno.h>
#include <stddef.h>
#include "uselect.h"

/* for logging */
#include "sics.h"

/* an uninterruptable version of select. M.Z. Oct 2008 */

int uselect(int nfds,
            fd_set * readfds, fd_set * writefds, fd_set * exceptfds,
            struct timeval *timeout)
{

  sigset_t sigmask;
  struct timespec tmo, *tmoPtr;
  int result;
  char buffer[80];

  sigfillset(&sigmask);
  if (timeout) {
    tmo.tv_sec = timeout->tv_sec;
    tmo.tv_nsec = timeout->tv_usec * 1000;
    tmoPtr = &tmo;
  } else {
    tmoPtr = NULL;
  }
  result = pselect(nfds, readfds, writefds, exceptfds, tmoPtr, &sigmask);
  if (result < 0 && errno == EINTR) {
    Log(WARN,"sys", "%s", "pselect was interrupted!");
  }
  return result;
}
