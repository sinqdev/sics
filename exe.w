\subsection{The Exe Batch Processing System}
This is a reimplementation of the batch processing scheme in SICS. It
addresses a couple of shortcomings of the fileeval command which is
already implemented. In the long run fileeval should be replaced  by
this system. In contrast to fileeval the following feautures will be
provided: \begin{itemize}
\item A list of directories to search for batch files
\item Better feedback about which batch file, which line, which
segement of text is executing. This even for the case when batch files
are nested. 
\item  Callbacks when batch processing proceeds. This in order to
allow a download mode in sicsed.
\item Means of downloading a batch file.
\end{itemize}

The source code for this is divided into two parts:\begin{itemize}
\item A buffer handling component. This handles everything for
managing and executing a single buffer.
\item A control module which handles the management of the batch
buffers. 
\end{itemize}

\subsubsection{Single Buffer Handling Code}
For each batch buffer there is a data structure:
@d exebufdat @{
typedef struct __EXEBUF{
	char *name;
	pDynString bufferContent;
	int start;
	int end;
	int lineno;
} ExeBuf;
@}
The items are:
\begin{description}
\item[name] The name of the batch buffer.
\item[bufferContent] The text of the buffer in a dynamic string.
\item[start] The start index of the currently executing part of the text
\item[end] The end index of the currently executing text. 
\item[lineno] The currently executing line.  
\end{description}

The interface to this buffer system comprises:
@d exebufint @{
  typedef struct __EXEBUF *pExeBuf;

  /**
    * create an exe buffer
    * @@param The name to use for the exe buffer
    * @@return A new exe buffer or NULL if out of memory
    */
  pExeBuf exeBufCreate(char *name);
  /**
    * delete an exe buffer
    * @@param data The exe buffer to delete
    */
  void exeBufKill(void *data);
  void exeBufDelete(pExeBuf data);
  /**
    * add a a line to the exe buffer
    * @@param self The exe buffer the line is to be added to
    * @@param line The text to add
    * @@return 1 on success, 0 when out of memory
    */
  int exeBufAppend(pExeBuf self, char *line);
  /**
    * load an exe buffer from a file
    * @@param self The exe buffer to load
    * @@param filename The name of the file to load
    * @@return 1 on success, 0 if the file could not be opened or
    * memory is exhausted.
    */
  int exeBufLoad(pExeBuf self, char *filename);
  /**
    * save an exe buffer to a file. 
    * @@param self The exe buffer to laod
    * @@param filename The name of the file to laod
    * @@return 1 on success, 0 if the file could not be opened.
    */
  int exeBufSave(pExeBuf self, char *filename);
  /**
    * process an exe buffer
    * @@param self The exe buffer to process
    * @@param pSics The SICS interpreter to use for processing
    * @@param pCon The connection object providing the environment for
    * processing this buffer.
    * @@pCall The callback interface to use for automatic notifications
    * @@return 1 on success, 0 on error
    */
  int exeBufProcess(pExeBuf self, SicsInterp *pSics, 
	SConnection *pCon, pICallBack pCall, int echo);
 /**
    * Process an exe buffer, but store commands causing errors in a list.
    * This is used for restoring status files. 
    * @@param self The exe buffer to process
    * @@param pSics The SICS interpreter to use for processing
    * @@param pCon The connection object providing the environment for
    * processing this buffer.
    * @@param errCommandList A lld string list for storing commands which 
    * had errors.
    * @@return 1 on success, 0 on error
    */
  int exeBufProcessErrList(pExeBuf self, SicsInterp *pSics, 
	SConnection *pCon, int errCommandList);
  /**
    * retrieves the executing range
    * @@param self The exe buffer to query
    * @@param start The start of the executing range
    * @@param end The end of the executing range
    * @@param lineno The current line number
    */
  void exeBufRange(pExeBuf self, int *start, int *end, int *lineno);
  /**
    * retrieves some portion  of text
    * @@param self The exe buffer to query
    * @@param start The start index from which to copy
    * @@param end The end pointer until which to copy
    * @@return A dynamic string holding the currently executing text. It is
    * the clients task to delete this buffer sfter use. Or NULL if out of
    * memory.
    */
  pDynString exeBufText(pExeBuf self, int start, int end);
  /**
    * retrieves the name of the batch buffer
    * @@param self The exe buffer to query
    * @@return A pointer to the buffer name. Do not delete! The name
    * is still owned by the exe beuffer.
    */
  char *exeBufName(pExeBuf self);
@}


\subsubsection{Managing exe Buffers}
This section describes the interface to the management module for
batch buffers. This management module is called the exe manager. This
module also provides the interpreter interface to 
the batch buffer system. The batch buffer system can operate in two
modes of operation:
\begin{itemize}
\item It keeps track of nested batch buffers, i.e. batch buffers which
call each other through exe calls in the batch buffer itself.
\item The management module also provides a stack for batch
buffers. This stack can be preloaded and then can be executed
sequentially by the exe manager. In order to do this provisions are
made for loading batch buffers from files or through commands.
\end{itemize}
Moreover the exe manager provides commands to view the current stack
and to find out what is currently being executed. 

For all this we need a data structure:
@d exemandat @{
typedef struct __EXEMAN{
	pObjectDescriptor pDes;
	pICallBack pCall;
	char *sysPath;
	char *batchPath;
	pDynar exeStack;
	int exeStackPtr;
	int runList;
	pExeBuf uploadBuffer;
	int echo;
	SConnection *runCon;
}ExeMan, *pExeMan;
@}
The fields:
\begin{itemize}
\item[pDes] The standard object descriptor.
\item[pCall] A callback interface to allow for automatic notifications
about the operation of this module.
\item[batchPath] The search path for batch buffers.
\item[exeStack] The stack of nested batch buffers.
\item[exeStackPtr] A pointer into exeStack.
\item[runList] The list of buffers to run when running batch buffers.
\item[uploadBuffer] A exe buffer to which data is uploaded. Also serves 
 as a flag if uploading is possible. In this case uploadBuffer must be
not NULL.   
\item[echo] Echo flag. When set, SICS commands are echoed.
\end{itemize}

The public interface to the exe manager are the interpreter interface
functions. All the rest are module private functions.

@d exemanint @{
int MakeExeManager(SConnection *pCon, SicsInterp *pSics, void *pData,
	int argc, char *argv[]);
int ExeManagerWrapper(SConnection *pCon, SicsInterp *pSics, void *pData,
	int argc, char *argv[]);
int runExeBatchBuffer(void *pData, SConnection *pCon, SicsInterp *pSics,
	char *name);
pDynString findBatchFile(SicsInterp *pSics, char *name);
int exeHdbBuffer(SConnection *pCon, 
              SicsInterp *pSics, char *name);
int exeHdbNode(pHdb exeNode, SConnection *pCon);              
@}

@o exeman.i -d @{
/*-------------------------------------------------------------------
  Internal header file for the exe manager module. Do not edit. This
  is automatically generated from exe.w
-------------------------------------------------------------------*/
@<exemandat@>
@}

@o exeman.h @{
/**
  * The batch buffer manager module. For more information see exe.tex.
  *
  * copyright: see file COPYRIGHT
  *
  * Mark Koennecke, November 2004
*/
#ifndef EXEMAN
#define EXEMAN
@<exemanint@>
#endif 	
@}

@o exebuf.i -d @{
/*--------------------------------------------------------------------
  Internal header file for the exe buffer module. Do not edit. This is
  automatically generated from exe.w
---------------------------------------------------------------------*/
@<exebufdat@>

@}

@o exebuf.h -d @{
/**
  * Buffer handling code for the Exe Buffer batch file processing
  * module.
  *
  * copyright: see file COPYRIGHT
  *
  * Mark Koennecke, November 2004
  */
#ifndef EXEBUF
#define EXEBUF
#include <sics.h>
#include "dynstring.h"
@<exebufint@>

#endif
@}
