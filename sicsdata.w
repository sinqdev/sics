\subsection{SICS Generic Data}
This is an attempt to create a generic interface between SICS data and 
clients. At the bottom of it is a auto resizing array of data. Data
can be entered into this array either manually or copied from data
sources such as histogram memories or scans. Data assembled in this
way, for instance through scripts, can then be forwarded to clients
either in UUencoded form or as a zipped array. 

WARNING: this code only works right when integers and floats are of the same size!

In a later stage this may be extended to support selected mathematical
operations as well. In another stage this may supersede the uuget and
zipget methods in the scan, histogram memory and specialized status
objects. 

This needs a data structure:
@d sidastruc @{
	typedef struct { 
			pObjectDescriptor pDes;
			int *data;
			char *dataType;
			int dataUsed;
			int currentDataSize;
	}SICSData, *pSICSData;
			
@} 
The fields:
\begin{description}
\item[pDes] The standard object descriptor.
\item[data] The actual data array. This code assumes that sizeof(int)
== sizeof(float).
\item[dataType] An array defining the data type for each element of
data. Supported are: 4 byte int and 4 byte float.
\item[dataUsed] is the highest data element adressed so far.
\item[currentDataSize] is the size of data.
\end{description} 

This object exports the following functions:
@d sidafunc @{
 int *getSICSDataPointer(pSICSData self, int start, int end);
 
 pSICSData createSICSData(char *name);

 void assignSICSType(pSICSData self, int start, int end, int type);

 int SICSDataFactory(SConnection *pCon, SicsInterp *pSics, 
	void	*pData,
	int argc, char *argv[]);

 int SICSDataAction(SConnection *pCon, SicsInterp *pSics, 
	void	*pData,
	int argc, char *argv[]);
	
 void clearSICSData(pSICSData self);	
 int getSICSDataInt(pSICSData self, int pos, int *value);
 int getSICSDataFloat(pSICSData self, int pos, float *value);
 int setSICSDataInt(pSICSData self, int pos, int value);
 int setSICSDataFloat(pSICSData self, int pos, float value);
 	
@}
\begin{description}
\item[getSICSDataPointer] returns a pointer to the first element of
start in the data array. It is also ensured that enough space is
available till end.
\item[createSICSData] creates a new SICSData structure.
\item[SICSDataFactory] is an interpreter function for creating and
deleting SICSData things.
\item[SICSDataAction] is the interpreter wrapper function through
which users may interact with the SICSData element.
\end{description}

@o sicsdata.h @{
/*---------------------------------------------------------------------
                           S I C S D A T A 

  An attempt to a generic interface to SICS data for all sorts of SICS
  clients.

  copyright: see file COPYRIGHT

  Mark Koennecke, June 2003
----------------------------------------------------------------------*/
#ifndef SICSDATA
#define SICSDATA
#define INTTYPE 0
#define FLOATTYPE 1

@<sidastruc@>
/*------------------------------------------------------------------*/
@<sidafunc@>
#endif
@}
