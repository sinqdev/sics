					TAS_SRC:[PSI.NOTES]LANTRONIX_SETUP.TXT
					======================================
15-Nov-2000
===========
  The Following setup was made for the

     Lantronix ETS16PR Terminal Server  psts240 (129.129.120.240) for POLDI

       define server name "psts240"
       define server incoming telnet
       define server incoming nopassword
       define server domain "psi.ch"
       define server subnet mask 255.255.0.0

       define protocol ip enable
       define protocol ip ipaddress            129.129.120.240
       define protocol ip gateway              129.129.100.2
       define protocol ip nameserver           129.129.100.1
       define protocol ip secondary nameserver 129.129.110.10
       define protocol ip timeserver enable

       define server dhcp  disabled
       define server bootp disabled
       define server rarp  disabled

       define server serial delay 10	           # (was 30 msec)
       define server buffer 256		           # (was 2048)
       define server ident "POLDI Terminal Server"

       define port 2-16 access remote
       define port 2-16 autobaud disable
       define port 2-16 break disable
       define port 2-16 broadcast disable
       define port 2-16 flow control none
       define port 2-16 loss notif disable
       define port 2-16 modem disable
       define port 2-16 passflow enable
       define port 2-16 telnet pad disable
       define port 2-16 verific disable

       init delay 0
-------------------------------------------------------------------------------
08-Nov-2000
===========
  The following commands:

      define server dhcp  disabled
      define server bootp disabled
      define server rarp  disabled

  were executed on

      psts213  -- Test device in WHGA/247
      psts216  -- Druechal
      psts217  -- TASP
      psts222  -- Loan device for P.Rasmussen
      psts223  -- SANS
      psts224  -- AMOR
      psts225  -- DMC
      psts227  -- FOCUS
      psts228  -- TriCS
      psts229  -- HRPT
      psts230  -- POLDI (on loan from LNS)

  on the recommendation of Lantronix (Brian Tutor)

-------------------------------------------------------------------------------

28-Apr-2000
===========
  The Following setup was made for the

     Lantronix ETS16PR Terminal Server  psts225 (129.129.120.225) for DMC
  and
     Lantronix ETS16PR Terminal Server  psts227 (129.129.120.227) for FOCUS

  Local_18>> define server serial delay 10	(was 30 msec)
  Local_18>> define server buffer 256		(was 2048)
  Local_18>> define server ident "DMC/FOCUS Terminal Server"

  Local_18>> define server dhcp disabled   \
  Local_18>> define server bootp disabled   > Done on 8-Nov-2000 (see above)
  Local_18>> define server rarp disabled   /

  Local_18>> define port 2-16 access remote
  Local_18>> define port 2-16 autobaud disable
  Local_18>> define port 2-16 break disable
  Local_18>> define port 2-16 broadcast disable
  Local_18>> define port 2-16 flow control none
  Local_18>> define port 2-16 loss notif disable
  Local_18>> define port 2-16 modem disable
  Local_18>> define port 2-16 passflow enable
  Local_18>> define port 2-16 telnet pad disable
  Local_18>> define port 2-16 verific disable

  Local_18>> init delay 0

  Local_18>> show server char

      ETS16PR Version V3.5/7(981112)         Uptime:                  0:01:57
      Hardware Addr: 00-80-a3-23-8e-1d       Name/Nodenum:      ETS_238E1D/ 0
      Ident String: DMC/FOCUS Terminal Server

   LAT  Circuit Timer (msec):         80     Password Limit:                3
      Inactive Timer (min):           30     Console Port:                  1
      Queue Limit:                    32     Retrans Limit:                50
      Keepalive Timer (sec):          20     Session Limit:                 4
      Multicast Timer (sec):          30     Node/Host Limits:          50/20

   TCP/IP Address:       129.129.120.225     Subnet Mask:         255.255.0.0
      Nameserver:            (undefined)     Backup Nameserver:   (undefined)
      TCP/IP Gateway:     129.129.40.151     Backup Gateway:    129.129.100.2
      Domain Name:           (undefined)     Daytime Queries:         Enabled
                                             TCP Keepalives:          Enabled
      DHCP Server:                  None     Lease Time:                 0:00

      Serial Delay (msec):            10     Network Buffering:           256
      Prompt:                Local_%n%P>
      Groups: 0

      Characteristics:  Announce  Broadcast  Lock
      Incoming Logins:  Telnet   (No Passwords Required)

-------------------------------------------------------------------------------

6-Mar-2000
==========
  The Following setup was made for the

     Lantronix ETS16PR Terminal Server  psts223 (129.129.120.223) for SANS

  Local_18>> define server serial delay 10	(was 30 msec)
  Local_18>> define server buffer 256		(was 2048)
  Local_18>> define server ident "SANS Terminal Server"

  Local_18>> define server dhcp disabled   \
  Local_18>> define server bootp disabled   > Done on 8-Nov-2000 (see above)
  Local_18>> define server rarp disabled   /

  Local_18>> define port 2-16 access remote
  Local_18>> define port 2-16 autobaud disable
  Local_18>> define port 2-16 break disable
  Local_18>> define port 2-16 broadcast disable
  Local_18>> define port 2-16 flow control none
  Local_18>> define port 2-16 loss notif disable
  Local_18>> define port 2-16 modem disable
  Local_18>> define port 2-16 passflow enable
  Local_18>> define port 2-16 telnet pad disable
  Local_18>> define port 2-16 verific disable

  Local_18>> define port 16 speed 4800

  Local_18>> init delay 0

  Local_18>> show server char

      ETS16PR Version V3.5/7(981112)         Uptime:           22 Days  20:09
      Hardware Addr: 00-80-a3-23-8e-4f       Name/Nodenum:      ETS_238E4F/ 0
      Ident String: SANS Terminal Server                    Daytime: 11:31:15

   LAT  Circuit Timer (msec):         80     Password Limit:                3
      Inactive Timer (min):           30     Console Port:                  1
      Queue Limit:                    32     Retrans Limit:                50
      Keepalive Timer (sec):          20     Session Limit:                 4
      Multicast Timer (sec):          30     Node/Host Limits:          50/20

   TCP/IP Address:       129.129.120.223     Subnet Mask:         255.255.0.0
      Nameserver:            (undefined)     Backup Nameserver:   (undefined)
      TCP/IP Gateway:       129.129.80.1     Backup Gateway:    129.129.100.2
      Domain Name:           (undefined)     Daytime Queries:         Enabled
                                             TCP Keepalives:          Enabled
      DHCP Server:                  None     Lease Time:                 0:00

      Serial Delay (msec):            10     Network Buffering:           256
      Prompt:                Local_%n%P>
      Groups: 0

      Characteristics:  Announce  Broadcast  Lock
      Incoming Logins:  Telnet   (No Passwords Required)

-------------------------------------------------------------------------------

19-May-1999
===========
   The Following setup was made for the

     Lantronix ETS8P Terminal Server  psts217 (129.129.120.217) for TASP
 and Lantronix ETS8P Terminal Server  psts216 (129.129.120.216) for DRUECHAL

  Local_10>> define server serial delay 10	(was 30 msec)
  Local_10>> define server buffer 256		(was 2048)
  Local_10>> define server ident "TASP Terminal Server"

  Local_10>> define server dhcp disabled   \
  Local_10>> define server bootp disabled   > Done on 8-Nov-2000 (see above)
  Local_10>> define server rarp disabled   /

  Local_10>> define port 2-8 access remote
  Local_10>> define port 2-8 autobaud disable
  Local_10>> define port 2-8 break disable
  Local_10>> define port 2-8 broadcast disable
  Local_10>> define port 2-8 flow control none
  Local_10>> define port 2-8 loss notif disable
  Local_10>> define port 2-8 modem disable
  Local_10>> define port 2-8 passflow enable
  Local_10>> define port 2-8 telnet pad disable
  Local_10>> define port 2-8 verific disable

  Local_10>> define port 7 stop 2

  Local_10>> init delay 0

  Local_10>> show server char

      ETS8P Version V3.5/5(980529)           Uptime:                  0:08:00
      Hardware Addr: 00-80-a3-17-09-7f       Name/Nodenum:      ETS_17097F/ 0
      Ident String: TASP Terminal Server

   LAT  Circuit Timer (msec):         80     Password Limit:                3
      Inactive Timer (min):           30     Console Port:                  1
      Queue Limit:                    32     Retrans Limit:                50
      Keepalive Timer (sec):          20     Session Limit:                 4
      Multicast Timer (sec):          30     Node/Host Limits:          50/20

   TCP/IP Address:       129.129.120.202     Subnet Mask:         255.255.0.0
      Nameserver:         129.129.110.10     Backup Nameserver:   (undefined)
      TCP/IP Gateway:      129.129.100.2     Backup Gateway:     129.129.30.4
      Domain Name:                psi.ch     Daytime Queries:         Enabled
                                             TCP Keepalives:          Enabled
      DHCP Server:                  None     Lease Time:                 0:00

      Serial Delay (msec):            10     Network Buffering:           256
      Prompt:                Local_%n%P>
      Groups: 0

      Characteristics:  Announce  Broadcast  Lock
      Incoming Logins:  Telnet   (No Passwords Required)

-------------------------------------------------------------------------------
