%  Copyleft (c) 1997-2000 by Mark Koennecke at PSI, Switzerland.
%
%
%  This software is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%
%  You may already have a copy of the GNU General Public License; if
%  not, write to the Free Software Foundation, Inc., 675 Mass Ave,
%  Cambridge, MA 02139, USA.
%

\documentclass[12pt]{article}
\usepackage{epsf}
\setlength{\oddsidemargin}{-.1in}
\setlength{\evensidemargin}{0in}
\setlength{\topmargin}{0in}
\addtolength{\topmargin}{-\headheight}
\addtolength{\topmargin}{-\headsep}
\setlength{\textheight}{8.9in}
\setlength{\textwidth}{6.2in}
\setlength{\marginparwidth}{0.5in}

\begin{document}
\title{SICS Reference Manual}

\author{Mark K\"onnecke\\
  Labor f\"ur Neutronenstreuung\\
  Paul Scherrer Institut\\
  CH-5232 Villigen PSI\\
  Switzerland\\       
  Mark.Koennecke@psi.ch \\
}

\maketitle
\clearpage
\tableofcontents

\clearpage

\section{SICS Programmers Reference}
This document is mostly a boring listing of the interfaces and functions provided by
the SICS modules. In order to get an idea of what is going on, please look
at the SICS user documentation, the SICS manager documentation, and the SICS
programmer guide. 

{\huge Warning} This document serves as
internal document for SICS developers only. It has not been proofread to any
standards!

\section{Coding Practicalities}
\subsection{Files}
Any module or object has at least the following files associated with it:
\begin{description}
\item[.h] a header file with the external function definitions and
often a typedef for the object data structure. The idea is that object
data structures are exposed only where absolutely necessary. 
\item[.c] the implementation file. Often data structures are only
defined in here. 
\item[.tex] a latex file describing the module or object, its data
structures and its interface functions. To be included into this document. 
\end{description}
Quite often, there are further files:
\begin{description}
\item[.i] descriptions of internal data structures.
\item[.w] nuweb literate programming files. .w file contain a mixture
of latex commands and code. The idea is that the code is embedded with
the documentation. There are special commands to craete additional
output files suitable for a compiler. The .w files are processed by
the nuweb utility which will create tex documentation and code files
from it. In SICS this is used to document data structures and
interfaces and provide general information about each object. Then
.tex, .h and .i files are created by nuweb. The use of this facility is
recommended.  
\end{description}

\subsection{Building SICS}
A SICS distribution is best be obtained from the cvs
repository. Access details are available on demand from Mark.Koennecke@psi.ch.
A source distribution can be downloaded with:
\begin{verbatim}
cvs checkout sics
\end{verbatim}
This creates a new directory, sics, and copies a lot of files.

\subsubsection{SICS Directory Layout}
Underneath the sics directory there are further sub directories:
\begin{description}
\item[doc] Contains documentation. Further subdirectories to doc:
\begin{description}
\item[user] User documentation.
\item[manager] Manager documentation
\item[programmer] Reference documentation for SICS programmers.
\end{description}
\item[matrix]A package for matrix manipulations
\item[tcl]Tcl scripts
\item[dummy]An example directory for a new site
\item[psi]PSI specific parts of SICS
\begin{description}
\item[hardsup] Hardware support routines
\item[sinqhm]The vxWorks histogram memory software
\item[tecs]The tecs environment control software
\item[motor]Utilities for the motor controller
\item[utils]various utilities
\begin{description}
\item[check]The Tcl syntax checker for SICS
\end{description}
\end{description}
\end{description}
The sics directory is meant to contain the generic parts of
SICS. Then there are site specific directories (dummy, psi) which hold
code special for particular instruments or hardware. The selection for
which configuration SICS is being compiled is made in the makefile, by
linking against the appropriate site specific libraries. 

\subsubsection{SICS Makefiles}
The SICS makefile system is not perfect but is far better then
compiling the whole stuff manually. There are various makefiles:
\begin{description}
\item[linux\_def, alpha\_def] contains the path to the HDF libraries,
the flags necessary to run sub makefiles in sub directories etc. This
is included by all other makefiles. 
\item[make\_gen] and children contains most of the makefile content and
is included by the other makefiles.
\item[makefile\_linux, makefile\_alpha] are makefiles for their
respective platforms.  
\end{description}
Some editing of makefiles will always be necessary. The path to the
HDF libraries has usually to be adapted in the \_def files. If there
is a different platfrom a new makefile has to be cloned from the
existing ones. In order to support a new site a new make\_gen and a new
makefile are have to be cloned. This system is similar in the
subdirectories to the SICS directory. Once this has been setup typing:
\begin{verbatim}
make -f makefile_alpha clean
\end{verbatim}
will clean all object files etc. and
\begin{verbatim}
make -f makefile_alpha 
\end{verbatim}
will build everything. May be you need to replace alpha by linux on
some platforms. Please note, that I have been to lazy to generate
dependencies for all the SICS files. This means that if you make
changes to the major SICS header files (especially the kernel files)
it is better do recompile everything. Otherwise you might find
yourself chasing obscure bugs. 
 

\subsubsection{Building Java Clients}
Again the first step is the untaring of tha java.tar file. This creates a
 java subdirectory which holds several other subdirectories:
\begin{description}
\item[focus] contains files special to the FOCUS status display.
\item[lnsutil] a number of utility classes used by many SICs Java applications.
\item[misc] several older classes kept for the purpose of documentation.
\item[param] A package for building parameter displays.
\item[ptplot] A XY-plot package.
\item[sics] The Java command line client.
\item[tablelayout] a special AWT layout manager used in the NeXus network 
 Data Browser.
\item[varwatch] Files special to the Varwatcher client.
\item[cmapper] A package for mapping 2D-data arrays to colour pictures. Used
 for SANS.
\item[htmlv] A html display package used for the help system.
\item[lnet] Special network related utilities for the SICS Java applications.
\item[lnswidgets] custom user interface components.
\item[nndb] The NeXus Network Data Browser.
\item[powder] The powder status display for DMC and HRPT.
\item[sans] The SANS status display.
\item[spread] Another layout manager package.
\item[topsi] The topsi and general scan status display.  
\item[amor] The AMOR user interface program.
\item[tas] The Triple Axis user interface program.
\item[trics] The TRICS user interface program.
\item[JQF] A state machine framework used for implementing a new I/O
system.
\item[psi] The start of a new hierarchy of SICS applications and
library classes.
\end{description} 
Furthermore there are some Java source file in the main directory together 
 with some htm files and makefiles. For each of the Java clients a makefile 
 is provided: make.focus, make.nndb, make.sans, make.topsi, make.powder, 
 make.sics, makr.amor and make.watch. The purpose of each makefile can be deduced from 
 its name. Building the Java clients requires two steps each: 1) compiling and 
 2) building the jar file. The jar-file is a container file which contains
 all the classes necessary for running the application. This is the standard
 distribution format for the SICS Java clients. The necessary commands are 
 using the powder status display as example:
\begin{description}
\item[Compiling] make -f make.powder
\item[Jar-File] make -f make.powder jar  
\end{description}

\section{Kernel Objects and Modules}
This section describes the modules defining the SICS kernel.
\include{task}
\include{nserver}
\include{site}
\include{ini}
\include{passwd}
\include{network}
\include{nread}
\include{telnet}
\include{nconman}
\include{costa}
\include{interface}
\include{event}
\include{devexec}
\include{SCinter}
\include{macro}
\include{emonitor}
\include{perfmon}
\include{interrupt}
\include{ofac}
\include{servlog}
\include{help}
\include{Busy}
\include{hmcontrol}
\subsection{The commandlog}
This is yet another logging facility of SICS. The idea is that all I/O
going to connections with user or manager level rights is logged.
This allows to figure out what really happened at an instrument. From
this base a scheme was introduced to automatically generate command
log files for each day. Additionally time stamps can be printed into
the commandlog at predefined intervalls.  In terms of an external
interface the commandlog just has a few functions for installing and
configuring the commandlog  from the interpreter plus a function for
writing to it. The rest is implemented as file statics in commandlog.c.  


\section{SICS Core Objects}
This section describes the SICS objects implementing commands and objects
common to all SICS instruments.
\include{scan}
\include{userscan}
\include{center}
\include{danu}
\include{drive}
\include{alias}
\include{buffer}
\include{ruli}
\include{o2t}
\include{selector}
\include{sicsexit}
\include{status}
\include{cron}
\include{optimise}
\include{token}
\include{udpquieck}
\include{xytable}
\include{lin2ang}
\include{lomax}
\include{nxscript}
\include{nxupdate}
\include{sicsdata}
\include{simsync}
\include{anticollider}

\section{SICS Hardware Objects}
This section deals with objects and modules representing instrument
hardware. Please be reminded that a hardware object within SICS
usually consists of two layers:
\begin{itemize}
\item The logical layer building the interface to SICS.
\item The driver layer actually interfacing to the hardware. 
\end{itemize}
This allows for the easy adaption to new hardware by exchanging
drivers.
The next thing to note is that most SICS hardware objects implement
at least one of the standard SICS interfaces:
\begin{itemize}
\item The drivable interface
\item The countable interface
\item The environment interface
\item The callback interface. 
\end{itemize} 
Please note, that there is some history in SICS hardware access. At
first there were no interfaces and objects implemented special
functions for themselves. These are still in the code. Some have their
right as utility functions. However, the preferred and supported way of
accessing SICS hardware objects is through the interface functions. 

\include{velo}
\include{evcontroller}
\include{tclev}
\include{evdrivers}
\include{motor}
\include{counter}
\include{hmdata}
\include{histogram}
\include{histsim}
\include{choco}
\include{switchedmotor}
\include{tcldriveable}
\include{rs232controller}
\include{gpib}

\section{PSI Specific Hardware}
\include{velodorn}
\include{itc4}
\include{bruker}
\include{pimotor}
\include{sinqhmdriv}
\include{serial}
\include{serialwait}
\include{sps}
\include{frame}
\include{ecb}

\section{Powder Diffraction Specific Objects}
\include{dmc}
\include{nxdata}

\section{SANS specific Objects}
\include{mumo}
\subsection{NXSans}
The files nxsans.h and nxsans.c implement the NeXus writing functions for SANS. Also interpreter commands for configuring SANS data writing into SICS and for triggering data writing are implemented.  
\include{sanswave}

\section{FOCUS Specific Objects}
\include{faverage}
\include{fowrite}

\section{AMOR Specific Objects}
\include{amor2t}
\include{amorscan}
\include{amorstat}
\include{nxamor}

\section{Four-Circle Diffraction Specific Objects}
\include{hkl}
\include{hklscan}
\include{integrate}
\include{mesure}
\include{tricsnex}
\include{difrac}

\section{Triple Axis Specific Code}
\include{tas}

\section{Helper Objects}
This section describes helper objects which implement useful data
structures or utilities.
\include{serialsinq}
\include{varlog}
\include{dynar}
\include{splitter}
\include{obpar}
\include{stringdict}
\include{fupa}
\include{nxsans}

\subsection{Further Helper Code}
Further helper code includes lld.*. This is a general linked list package.
It is documented in lld.h. All credits go to its original author. lld.c has
been modified slightly to resolve a memory problem.

napi.* is the NeXus API. This is documented in a separate document.

nxdict.* is the NeXus dictionary API, which is documented in another
separate document.

strdup.* is a fortified implementation of the standard utility strdup.

strrepl.c is the implmentation of a string replace.

uubuffer.* is a utility function which UUencodes a buffer. 

uusend.c is some helper code for sending UUencoded messages.

scaldate.* is an implementation of some julian date conversion
utilities. Is used in commandlog for incrementing date.

circular.* is the implementation of a circular buffer.   

\end{document}
