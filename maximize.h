/*--------------------------------------------------------------------------
                             M A X I M I Z E

  This implements the maximize command for centering a peak. The algorithm is
  taken form the program difrac by P. White and E. Gabe. The algorithm is
  described in more detail in the implementation file.

  copyright: see copyright.h

  Mark Koennecke, November 1999
  --------------------------------------------------------------------------*/
#ifndef SICSMAXIMIZE
#define SICSMAXIMIZE

typedef struct __MAXIMIZE *pMax;

int MaximizePeak(pMax self, void *pVar, char *pVarName,
                 float fStep, CounterMode eMode,
                 float fPreset, SConnection * pCon);

  /* 
   * Locates the maximum of a peak with respect to the motor or virtual
   * motor pVar and drives the motor to that point. fStep is the step
   * width to use, eMode the counting mode amd fPreset is the preset for
   * counting. pCon is the connection object to which errors will be
   * reported.
   */
/*------------------------ Wrapper functions ----------------------------*/
int MaximizeFactory(SConnection * pCon, SicsInterp * pSics, void *pData,
                    int argc, char *argv[]);
int MaximizeAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                   int argc, char *argv[]);
#endif
