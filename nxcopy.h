/**
 * This defines a couple of Tcl functions which allow to copy data from the 
 * nxdataset's returned from the swig Tcl interface to NeXus files into 
 * SICS data structures.
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koennecke, October 2007
 */
#ifndef NXCOPY_H_
#define NXCOPY_H_
#include <tcl.h>

int NXcopy_Init(void * pTcl);

#endif                          /*NXCOPY_H_ */
