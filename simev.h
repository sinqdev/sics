/*-------------------------------------------------------------------------

				S I M E V
  A simulation driver for an environment device.
  
  Mark Koennecke, Juli 1997
--------------------------------------------------------------------------*/
#ifndef SICSSIMEV
#define SICSSIMEV
pEVDriver CreateSIMEVDriver(int argc, char *argv[]);

#endif
