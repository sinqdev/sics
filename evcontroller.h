
#line 232 "evcontroller.w"

/*--------------------------------------------------------------------------
             E N V I R O N M E N T   C O N T R O L L E R

  This is the base class for all environment controllers. 

  Mark Koennecke, Juli 1997

  copyright: see implementation file
----------------------------------------------------------------------------*/
#ifndef SICSEVCONTROL
#define SICSEVCONTROL
#include "varlog.h"

#line 153 "evcontroller.w"

/*--------------------------- live & death --------------------------------*/
typedef struct __EVControl *pEVControl;
typedef struct __EVDriver *pEVDriver;

pEVControl CreateEVController(pEVDriver pDriv, char *pName, int *iErr);
void DeleteEVController(void *pData);
pEVControl MakeEVController(pEVDriver pDriv, SConnection * pCon,
                            ObjectFunc wrapper, int argc, char *argv[]);
/*-------------------------- driving  ---------------------------------*/
int EVCDrive(pEVControl self, SConnection * pCon, float fNew);
int EVCGetPos(pEVControl self, SConnection * pCon, float *fVal);

/*------------------------ parameters ----------------------------------*/
EVMode EVCGetMode(pEVControl self);
int EVCSetMode(pEVControl self, EVMode eNew);
int EVCSetPar(pEVControl self, char *name, float fNew, SConnection * pCon);
int EVCGetPar(pEVControl self, char *name, float *fVal);
int EVCList(pEVControl self, SConnection * pCon);
/*------------------------- logging -----------------------------------*/
pVarLog EVCGetVarLog(pEVControl self);
/*----------------- Interface to SICS interpreter -----------------------*/
int EVControlWrapper(SConnection * pCon, SicsInterp * pSics, void *pData,
                     int argc, char *argv[]);

int EVControlFactory(SConnection * pCon, SicsInterp * pSics, void *pData,
                     int argc, char *argv[]);



#line 245 "evcontroller.w"

#endif
