/*******************************************************************************
*
* McStas, neutron ray-tracing package
*         Copyright 1997-2002, All rights reserved
*         Risoe National Laboratory, Roskilde, Denmark
*         Institut Laue Langevin, Grenoble, France
*
* Component: Monitor
*
* %I
* Written by: Kim Lefmann
* Date: October 4, 1997
* Version: $Revision$
* Origin: Risoe
* Release: McStas 1.6
* Modified to write monitor file for SICS: Mark Koennecke, June 2005
*
* Simple single detector/monitor.
*
* %D
* Sums neutrons (0th, 1st, and 2nd moment of p) flying through 
* the rectangular monitor opening. May also be used as detector.
*
* Example: Monitor(xmin=-0.1, xmax=0.1, ymin=-0.1, ymax=0.1)
*
* %P
* INPUT PARAMETERS:
*
* xmin: Lower x bound of opening [m]
* xmax: Upper x bound of opening [m]
* ymin: Lower y bound of opening [m]
* ymax: Upper y bound of opening [m]
*
* OUTPUT PARAMETERS:
*
* Nsum:  Number of neutron hits
* psum:  Sum of neutron weights
* p2sum: 2nd moment of neutron weights
*
* %E
*******************************************************************************/

DEFINE COMPONENT MKMonitor
DEFINITION PARAMETERS ()
SETTING PARAMETERS (xmin, xmax, ymin, ymax,char *controlfile="mon1.dat",int dumpCount=1000)
OUTPUT PARAMETERS (Nsum, psum, p2sum,currentCount)
STATE PARAMETERS (x,y,z,vx,vy,vz,t,s1,s2,p)
SHARE
%{
	void dumpTotal(char *ffilename, long totalCounts){
		FILE *fd = NULL;
		char tmp[1024];

		strncpy(tmp,ffilename, 1000);
		strcat(tmp,"tmp");
		fd = fopen(tmp,"w");
		if(fd != NULL){
			fprintf(fd,"%ld\n",totalCounts);
			fclose(fd);
			rename(tmp,ffilename);
			unlink(tmp);
		}
	}
%}  
DECLARE
  %{
  	long currentCount;
    double Nsum;
    double psum, p2sum;
  %}
INITIALIZE
  %{
    psum = 0;
    p2sum = 0;
    Nsum = 0;
    currentCount = 0;
  %}
TRACE
  %{
    PROP_Z0;
    if (x>xmin && x<xmax && y>ymin && y<ymax)
    {
      Nsum++;
      psum += p;
      p2sum += p*p;
      currentCount++;
      if(dumpCount > 0 && currentCount > dumpCount){
      	dumpTotal(controlfile,(long)Nsum);
      	currentCount = 0;
      }
      SCATTER;
    }
  %}
SAVE
  %{
    DETECTOR_OUT_0D("Single monitor", Nsum, psum, p2sum);
  %}

MCDISPLAY
%{
  magnify("xy");
  multiline(5, (double)xmin, (double)ymin, 0.0,
               (double)xmax, (double)ymin, 0.0,
               (double)xmax, (double)ymax, 0.0,
               (double)xmin, (double)ymax, 0.0,
               (double)xmin, (double)ymin, 0.0);
%}

END
