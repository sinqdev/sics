
/*---------------------------------------------------------------------------
                  T C L E N V I R O N M E N T

   This is the header file for an environment device driver defined in 
   the Tcl macro language. Dor more details see tclev.tex

   copyright: see copyright.h

   Mark Koennecke, February 1998
------------------------------------------------------------------------------*/
#ifndef SICSTCLEV
#define SICSTCLEV


pEVDriver CreateTclDriver(int argc, char *argv[], char *pName,
                          SConnection * pCon);
int UpdateTclVariable(pEVDriver self, char *name, float fVal);

int TclEnvironmentWrapper(SConnection * pCon, SicsInterp * pSics,
                          void *pData, int argc, char *argv[]);


#endif
