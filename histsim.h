
#line 15 "histsim.w"

/*--------------------------------------------------------------------------
                        H I S T S I M

  A simulated histogram memory for software testing.

  Mark Koennecke, April 1997

  copyright: see implementation file.
---------------------------------------------------------------------------*/
#ifndef SICSHISTSIM
#define SICSHISTSIM

#line 5 "histsim.w"

pHistDriver CreateSIMHM(pStringDict pOpt);

#line 27 "histsim.w"

#endif
