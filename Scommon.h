/*-------------------------------------------------------------------------

	Some common types and functions for SICS


	Mark Koennecke,    October 1996

	Copyright:

	Labor fuer Neutronenstreuung
	Paul Scherrer Institut
	CH-5423 Villigen-PSI


        The authors hereby grant permission to use, copy, modify, distribute,
        and license this software and its documentation for any purpose, provided
        that existing copyright notices are retained in all copies and that this
        notice is included verbatim in any distributions. No written agreement,
        license, or royalty fee is required for any of the authorized uses.
        Modifications to this software may be copyrighted by their authors
        and need not follow the licensing terms described here, provided that
        the new terms are clearly indicated on the first page of each file where
        they apply.

        IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
        FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
        ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
        DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
        POSSIBILITY OF SUCH DAMAGE.

        THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
        INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
        IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
        NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
        MODIFICATIONS.
----------------------------------------------------------------------------*/
#ifndef SICSCOMMON
#define SICSCOMMON

/* this enum defines the output types in SICS */
typedef enum {
  eInternal,                    /* internal */
  eCommand,                     /* reserved, not used */
  eHWError,                     /* reserved, used only for SICSLog */
  eInError,                     /* reserved, used as a mark in the handling of output codes */
  eStatus,                      /* reserved, deprecated */
  eValue,                       /* value reponse: copied into Tcl */
  eStart,                       /* start message */
  eFinish,                      /* finish message */
  eEvent,                       /* some callback messages */
  eWarning,                     /* warnings */
  eError,                       /* error: copied into Tcl */
  eHdbValue,                    /* hipadaba value chnage */
  eHdbEvent,                    /* Hipadaba event */
  eLog,                         /* log message: is always written to client */
  eLogError                     /* error message to log: is always written to client */
} OutCode;

const char* OutCodeToTxt(OutCode outcode);
const char* OutCodeToText(OutCode outcode);
int OutCodeFromText(const char *text, OutCode *outcode);

#include "interrupt.h"

/* define some user rights codes */
#define usInternal 0
#define usMugger 1
#define usUser   2
#define usSpy    3

/* status and error codes */
#define OKOK 1
#define HWIdle 2
#define HWBusy 3
#define HWFault 4
#define HWPosFault 5
#define HWCrash    6
#define NOMEMORY   7
#define HWNoBeam   8
#define HWPause    9
#define HWWarn     10
#define HWRedo     11

/* 
 Sics uses some server options for some server configuration parameters.
 These options are held in the global variable pSICSOptions.
  */
#include "ifile.h"
extern IPair *pSICSOptions;

#endif
