/*
 * This is the implementation of a generic container object.
 *
 * A handle is a unique reference to an object. Current handles can be
 * converted to a pointer to the object and stale handles can be recognized and
 * rejected.
 *
 * The objective is for this to be O(1), except during array expansion.
 *
 * Douglas Clowes, March 2016
 */
#include "hndbag.h"
#include <stdlib.h>

/*
 * External Interface
 */
typedef pHNDBAG           pEXTERNAL_CONTAINER;
typedef HNDBAG_COMPARE    COMPARE_FUNCTION;
typedef HNDBAG_VISITOR    VISITOR_FUNCTION;

/*
 * Implementation declarations
 */

typedef struct _entry_object_t ENTRY_OBJECT;
typedef ENTRY_OBJECT *pENTRY_OBJECT;
typedef struct _container_object_t CONTAINER_OBJECT;
typedef CONTAINER_OBJECT *pCONTAINER_OBJECT;

static int local_find_entry(pCONTAINER_OBJECT cp, const HANDLE_T obj_in, ENTRY_OBJECT **ent);
static void local_add_empty(pCONTAINER_OBJECT cp, size_t i);
static void local_add_empties(pCONTAINER_OBJECT cp, size_t old_size, size_t new_size);
static ENTRY_OBJECT *local_get_empty(pCONTAINER_OBJECT cp);
static void **local_array_create(pCONTAINER_OBJECT cp, COMPARE_FUNCTION compare, bool retain);
static void local_array_release(pCONTAINER_OBJECT cp, void **array);
static int local_item_release(pCONTAINER_OBJECT cp, pENTRY_OBJECT entry);

/*
 * Implementation details
 */

/*
 * The entry object contains information for one contained object
 */

struct _entry_object_t {
  uint32_t eo_refcnt;         /**< entry object reference counter */
  void *pointer;              /**< pointer to object or next free entry when free */
  uint32_t index;             /**< index into array for this entry */
  uint32_t nuses;             /**< number of times this entry has been used */
};

/*
 * The container object - contains entry objects
 */

struct _container_object_t {
  uint32_t co_refcnt;         /**< container object reference counter */
  uint32_t array_size;        /**< current size of the entry array */
  uint32_t array_limit;        /**< maximum size of the entry array */
  void (*entry_dtor)(void *); /**< optional destructor function */
  uint32_t num_inuse;         /**< number of objects contained */
  uint32_t check_unique;      /**< if nonzero then check for and ignore duplicates */
  uint32_t num_bits;          /**< number of bits in the index */
  ENTRY_OBJECT *entries;      /**< array of entry object */
  ENTRY_OBJECT *empty_head;   /**< pointer to first free entry or NULL */
  ENTRY_OBJECT *empty_tail;   /**< pointer to last free entry or NULL */
};

inline static HANDLE_T hpack(pCONTAINER_OBJECT cp, uint64_t index, uint64_t nuses) {
  return (HANDLE_T) ((nuses << cp->num_bits) + index);
}

inline static uint64_t local_get_nuses(pCONTAINER_OBJECT cp, HANDLE_T h) {
  return (uint64_t) h >> cp->num_bits;
}

inline static uint64_t local_get_index(pCONTAINER_OBJECT cp, HANDLE_T h) {
  return (uint64_t) h & (((uint64_t)1<<cp->num_bits)-1);
}

inline static uint32_t local_num_bits(uint32_t num) {
  uint32_t nbits = 0;
  while (num) {
    num >>= 1;
    nbits++;
  }
  return nbits;
}

/*
 * Constructor - create a new container
 */
int hndbag_create(pEXTERNAL_CONTAINER *cp_out, int min_size, int max_size)
{
  pCONTAINER_OBJECT cp;
  size_t array_size = min_size;
  *cp_out = NULL;
  cp = calloc(1, sizeof(CONTAINER_OBJECT));
  if (cp == NULL) {
    return -1;
  }
  cp->array_size = array_size;
  cp->array_limit = max_size;
  cp->entries = calloc(array_size, sizeof(ENTRY_OBJECT));
  if (cp->entries == NULL) {
    free(cp);
    return -1;
  }
  local_add_empties(cp, 0, array_size);
  cp->num_inuse = 0;
  cp->co_refcnt = 1;
  cp->num_bits = local_num_bits(max_size);
  *cp_out = (pEXTERNAL_CONTAINER) cp;
  return 0;
}

/*
 * Increment the reference count for the container
 */
int hndbag_retain(pEXTERNAL_CONTAINER cp_in)
{
  pCONTAINER_OBJECT cp = (pCONTAINER_OBJECT) cp_in;
  cp->co_refcnt++;
  return 0;
}

/*
 * Release a reference to the container
 */
int hndbag_release(pEXTERNAL_CONTAINER *cp_io)
{
  pCONTAINER_OBJECT cp = (pCONTAINER_OBJECT) *cp_io;
  *cp_io = NULL;
  if (--cp->co_refcnt > 0)
    return 1;
  /* no more references */
  if (cp->entries) {
    size_t i;
    for (i = 0; i < cp->array_size; ++i) {
      ENTRY_OBJECT *entry = &cp->entries[i];
      if (entry->eo_refcnt > 0) {
        /* fudge the refcnt */
        entry->eo_refcnt = 1;
        local_item_release(cp, entry);
      }
    }
    free(cp->entries);
  }
  free(cp);
  return 0;
}

/*
 * Get the currently unique index part of the handle
 * \param cp_in pointer to the container
 * \param h handle to be extracted
 * \return index portion of the handle
 */
uint64_t hndbag_get_index(pEXTERNAL_CONTAINER cp_in, HANDLE_T h) {
  pCONTAINER_OBJECT cp = (pCONTAINER_OBJECT) cp_in;
  return local_get_index(cp, h);
}

/*
 * Set a destructor function for the item pointed to (optional)
 * \param cp_in pointer to the container
 * \param free_func function to free/destroy/destruct a pointed-to-object
 *
 * This sets a function to be used to release an object when the reference
 * count for the item goes to zero.
 *
 * The default value is NULL meaning no function will be called. The caller
 * should manage object lifetime.
 *
 * The function can be free in which case free(pointer) will be called.
 *
 * A more elaborate destructor can be supplied to, for example, release
 * resources contained within the object.
 *
 * Shared reference counting can be implemented by the provided destructor releasing
 * the reference on the pointed-to-object.
 */
void hndbag_set_item_destructor(pEXTERNAL_CONTAINER cp_in, void (*free_func)(void *ptr))
{
  pCONTAINER_OBJECT cp = (pCONTAINER_OBJECT) cp_in;
  cp->entry_dtor = free_func;
}

/*
 * get the unique_flag.
 */
bool hndbag_get_unique(pEXTERNAL_CONTAINER cp_in)
{
  pCONTAINER_OBJECT cp = (pCONTAINER_OBJECT) cp_in;
  return cp->check_unique;
}

/*
 * set the unique_flag.
 */
bool hndbag_set_unique(pEXTERNAL_CONTAINER cp_in, bool check_unique)
{
  pCONTAINER_OBJECT cp = (pCONTAINER_OBJECT) cp_in;
  int result = cp->check_unique;
  cp->check_unique = check_unique;
  return result;
}

/*
 * Return the count of active objects
 */
int hndbag_count(pEXTERNAL_CONTAINER cp_in)
{
  pCONTAINER_OBJECT cp = (pCONTAINER_OBJECT) cp_in;
  return cp->num_inuse;
}

/*
 * Return an array of pointers to the objects, sorted if compare != NULL
 */
void **hndbag_array(pEXTERNAL_CONTAINER cp_in, COMPARE_FUNCTION compare)
{
  pCONTAINER_OBJECT cp = (pCONTAINER_OBJECT) cp_in;
  return local_array_create(cp, compare, false);
}

/*
 * Return an array of pointers to the objects in the order of compare
 */
void **hndbag_array_retain(pEXTERNAL_CONTAINER cp_in, COMPARE_FUNCTION compare)
{
  pCONTAINER_OBJECT cp = (pCONTAINER_OBJECT) cp_in;
  return local_array_create(cp, compare, true);
}

/*
 * Release the array of pointers
 */
void hndbag_array_release(pHNDBAG cp_in, void **array)
{
  pCONTAINER_OBJECT cp = (pCONTAINER_OBJECT) cp_in;
  local_array_release(cp, array);
}

/*
 * Insert a new object into the collection
 */
int hndbag_item_insert(pEXTERNAL_CONTAINER cp_in, void *ptr_in, HANDLE_T *obj_out)
{
  pCONTAINER_OBJECT cp = (pCONTAINER_OBJECT) cp_in;
  ENTRY_OBJECT *entry = NULL;
  *obj_out = 0;
  if (cp == NULL)
    return -1;
  /* check_unique */
  if (cp->check_unique) {
    size_t i;
    for (i = 0; i < cp->array_size; ++i) {
      if (cp->entries[i].eo_refcnt > 0 && cp->entries[i].pointer == ptr_in) {
        entry = &cp->entries[i];
        ++entry->eo_refcnt;
        *obj_out = hpack(cp, entry->index, entry->nuses);
        return 0;
      }
    }
  }
  /* get an empty entry */
  entry = local_get_empty(cp);
  if (entry == NULL)
    return -1;
  /* populate the entry */
  entry->eo_refcnt = 1;
  entry->pointer = ptr_in;
  /* if this wraps, go again because we dont want zero */
  if (++entry->nuses == 0)
    ++entry->nuses;
  *obj_out = hpack(cp, entry->index, entry->nuses);
  return 0;
}

/*
 * Increment the reference counter for an item
 */
int hndbag_item_retain(pEXTERNAL_CONTAINER cp_in, HANDLE_T obj_in)
{
  pCONTAINER_OBJECT cp = (pCONTAINER_OBJECT) cp_in;
  if (cp) {
    ENTRY_OBJECT *entry;
    int status = local_find_entry(cp, obj_in, &entry);
    /* If entry is valid, increment the reference count */
    if (entry) {
      entry->eo_refcnt++;
      return 0;
    }
  }
  return -1;
}

/*
 * Decrement the reference counter for an item
 */
int hndbag_item_release(pEXTERNAL_CONTAINER cp_in, HANDLE_T *obj_io)
{
  pCONTAINER_OBJECT cp = (pCONTAINER_OBJECT) cp_in;
  HANDLE_T obj_in = *obj_io;
  ENTRY_OBJECT *entry = NULL;
  int status;
  int result;
  *obj_io = 0;
  /* find the entry */
  status = local_find_entry(cp, obj_in, &entry);
  if (status < 0)
    return status;
  /* If last reference, release resources */
  return local_item_release(cp, entry);
}

/*
 * Convert a handle to a pointer
 */
int hndbag_get_pointer(pEXTERNAL_CONTAINER cp_in, HANDLE_T obj_in, void **ptr_out)
{
  pCONTAINER_OBJECT cp = (pCONTAINER_OBJECT) cp_in;
  ENTRY_OBJECT *entry;
  int status = local_find_entry(cp, obj_in, &entry);
  if (status != 0) {
    if (ptr_out)
      *ptr_out = NULL;
    return status;
  }
  if (ptr_out)
    *ptr_out = entry->pointer;
  return 0;
}

/*
 * Test if an item is already in the collection
 */
bool hndbag_contains(pEXTERNAL_CONTAINER cp_in, const HANDLE_T obj_in)
{
  pCONTAINER_OBJECT cp = (pCONTAINER_OBJECT) cp_in;
  ENTRY_OBJECT *entry;
  int status = local_find_entry(cp, obj_in, &entry);
  if (entry)
    return true;
  return false;
}

/*
 * Find a handle based on the comparison function and the test value
 */
int hndbag_find_handle(pHNDBAG cp_in, HNDBAG_COMPARE compare, const void *ctx, HANDLE_T *obj_out)
{
  pCONTAINER_OBJECT cp = (pCONTAINER_OBJECT) cp_in;
  size_t i;
  for (i = 0; i < cp->array_size; ++i)
    if (cp->entries[i].eo_refcnt > 0)
      if (compare(cp->entries[i].pointer, ctx) == 0) {
        *obj_out = hpack(cp, cp->entries[i].index, cp->entries[i].nuses);
        return 0;
      }
  return -1;
}

/*
 * Find a pointer based on the comparison function and the test value
 */
void *hndbag_find(pEXTERNAL_CONTAINER cp_in, COMPARE_FUNCTION compare, const void *ctx)
{
  pCONTAINER_OBJECT cp = (pCONTAINER_OBJECT) cp_in;
  size_t i;
  for (i = 0; i < cp->array_size; ++i)
    if (cp->entries[i].eo_refcnt > 0)
      if (compare(cp->entries[i].pointer, ctx) == 0)
        return cp->entries[i].pointer;
  return NULL;
}

/*
 * Visitor pattern - visit each entry with the given function in order of compare
 */
void hndbag_visit(pEXTERNAL_CONTAINER cp_in, VISITOR_FUNCTION visitor,
    void *ctx, COMPARE_FUNCTION compare)
{
  pCONTAINER_OBJECT cp = (pCONTAINER_OBJECT) cp_in;
  size_t i;
  void **list = hndbag_array(cp_in, compare);
  if (list) {
    for (i = 0; i < cp->num_inuse; ++i) {
      visitor(list[i], ctx);
    }
    free(list);
  }
}

/*==============================================================================
 * Private Implementation Procedures
 */

/*
 * Find the entry for the given object, no checks
 */
static int local_find_entry(pCONTAINER_OBJECT cp, const HANDLE_T obj_in, ENTRY_OBJECT **ent)
{
  ENTRY_OBJECT *entry = NULL;
  size_t index;
  int result = -1;
  index = local_get_index(cp, obj_in);
  if (cp->entries && index < cp->array_size) {
    entry = &cp->entries[index];
    if (entry->eo_refcnt == 0) {
      /* Rejected as not-in-use */
      result = -2;
      entry = NULL;
    }
    else if (local_get_nuses(cp, obj_in) != entry->nuses) {
    /* Rejected as in-use-but-stale */
      result = -3;
      entry = NULL;
    }
  }
  if (ent)
    *ent = entry;
  if (entry)
    return 0;
  return result;
}

/*
 * Initialise and add a new empty.
 */
static void local_add_empty(pCONTAINER_OBJECT cp, size_t i)
{
  if (i < cp->array_size) {
    cp->entries[i].eo_refcnt = 0;
    cp->entries[i].pointer = NULL;
    cp->entries[i].index = i;
    cp->entries[i].nuses = 0;
    if (cp->empty_tail == NULL) {
      cp->empty_head = cp->empty_tail = &cp->entries[i];
    } else {
      cp->empty_tail->pointer = &cp->entries[i];
      cp->empty_tail = &cp->entries[i];
    }
  }
}

/*
 * Initialise and add a set of new empties.
 */
static void local_add_empties(pCONTAINER_OBJECT cp, size_t old_size, size_t new_size)
{
  size_t i;
  for (i = old_size; i < new_size; ++i) {
    local_add_empty(cp, i);
  }
}

/*
 * Get an empty entry from the free list or expand the array
 *
 * Note that this only works because we reallocate only when the free list is empty.
 */
static ENTRY_OBJECT *local_get_empty(pCONTAINER_OBJECT cp)
{
  ENTRY_OBJECT *entry;
  /*
   * Check for an available empty entry
   */
  if (cp->empty_head == NULL) {
    /*
     * There are no empties so we must grow the array
     */
    void *result = NULL;
    size_t old_size = cp->array_size;
    size_t new_size = old_size << 1;
    if (new_size > cp->array_limit)
      new_size = cp->array_limit;
    /*
     * If we cannot grow then we failed
     */
    if (new_size == old_size)
      return NULL;
    /*
     * accept failure thru a new_pointer, if NULL then we failed
     */
    result = realloc(cp->entries, sizeof(ENTRY_OBJECT) * (new_size));
    if (result == NULL)
      return NULL;
    /*
     * Array has been reallocated, update the pointer and size
     */
    cp->entries = result;
    cp->array_size = new_size;
    /*
     * Initialise the new entries and add them to the free list
     */
    local_add_empties(cp, old_size, new_size);
  }
  if (cp->empty_head == NULL) {
    return NULL;
  }
  /*
   * Pop one off the front of the free list
   */
  entry = cp->empty_head;
  cp->empty_head = entry->pointer;
  if (cp->empty_head == NULL)
    cp->empty_tail = NULL;
  entry->pointer = NULL;
  ++cp->num_inuse;
  return entry;
}

/*
 * Get a NULL terminated array of pointers to contained objects
 * @param cp    pointer to container
 * @param compare optional sort comparisson
 * @param retain TRUE iff the items should be retained
 * @return pointer to NULL terminated array or NULL
 * pointer should be passed to to local_array_release
 */
static void **local_array_create(pCONTAINER_OBJECT cp, COMPARE_FUNCTION compare, bool retain)
{
  void **result;
  size_t i;
  size_t j;
  if (cp->num_inuse == 0)
    return NULL;
  result = malloc((cp->num_inuse + 2) * sizeof(void *));
  if (result == NULL)
    return NULL;
  for (i = 0, j = 0; i < cp->num_inuse && j < cp->array_size; ++j) {
    if ( cp->entries[j].eo_refcnt > 0) {
      result[i++] = cp->entries[j].pointer;
      if (retain)
        cp->entries[j].eo_refcnt++;
    }
  }
  result[cp->num_inuse] = NULL;
  result[cp->num_inuse + 1] = retain ? cp : NULL;
  if (compare != NULL)
    qsort(result, cp->num_inuse, sizeof(void *), compare);
  return result;
}

static void local_array_release(pCONTAINER_OBJECT cp, void **array)
{
  void **p = array;
  size_t i;

  if (array == NULL)
    return;
  while (*p)
    ++p;
  ++p;
  if (cp == *p) {
    for (i = 0; array[i]; ++i) {
      size_t j;
      for (j = 0; j < cp->array_size; ++j) {
        if (cp->entries[j].eo_refcnt > 0 && cp->entries[j].pointer == array[i]) {
          /* fudge the refcnt */
          cp->entries[j].eo_refcnt = 1;
          local_item_release(cp, &cp->entries[j]);
        }
      }
    }
  }
  free(array);
}

static int local_item_release(pCONTAINER_OBJECT cp, pENTRY_OBJECT entry)
{
  int result = 0;
  /* If last reference, release resources */
  if (entry->eo_refcnt > 0) {
    result = --entry->eo_refcnt;
    if (result == 0) {
      --cp->num_inuse;
      if (cp->entry_dtor)
        cp->entry_dtor(entry->pointer);
      entry->pointer = NULL;
      local_add_empty(cp, entry->index);
    }
  }
  return result;
}
/*==============================================================================
 * Test program
 */

#ifdef UNIT_TEST
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
static int fails = 0;

bool assert_true(bool condition, const char *fmt, ...)
{
  va_list ap;
  if (condition != true) {
    va_start(ap, fmt);
    printf("assert_true %3d: ", ++fails);
    vprintf(fmt, ap);
    printf("\n");
    va_end(ap);
  }
  return condition;
}

/*
 * Check that the entry count is as expected
 */
static void check_count(pEXTERNAL_CONTAINER cp, int expected, const char* msg)
{
  int count = hndbag_count(cp);
  assert_true(count == expected, "%s count (%d) does not match expected (%d)\n", msg, count, expected);
}

static int finder(const void *pLeft, const void *pRight)
{
#if 0
  return strcasecmp(pLeft, pRight);
#else
  return atoi(strchr(pLeft, ':')+1) - atoi(strchr(pRight, ':')+1);
#endif
}

static void free_me(void *ptr)
{
  free(ptr);
}

/*
 * Just compare the integer part numerically
 */
static int reorder(const void *p1, const void *p2)
{
  const char *pLeft = *(const char **)p1;
  const char *pRight = *(const char **)p2;
  int delta = atoi(strchr(pLeft, ':')+1) - atoi(strchr(pRight, ':')+1);
  return delta;
}

/*
 * Count the number of entries for each integer value
 */
static int counter(void *in, void *int_arr) {
  char *pointer = (char *) in;
  int idx = atoi(strchr(pointer, ':')+1);
  ((int *) int_arr)[idx]++;
  return 0;
}

static void unit_test_01(void)
{
  const int samples = 100;
  int one = 2, two = 10;
  int i, status, myCount;
  char text[132];
  void *texts[samples + 2];
  void *vp;
  void **vpp;
  int int_arr[samples];
  pHNDBAG cp = NULL;
  HANDLE_T handle[samples + 2];

  status = hndbag_create(&cp, 1, 1024);
  if (!assert_true(status == 0, "Failed to create container: %d", status))
    return;
  assert_true(hndbag_get_index(cp, 7) == 7, "Failed to get index 7");
  assert_true(hndbag_get_index(cp, 2048+7) == 7, "Failed to get index 2048+7");
  myCount = hndbag_count(cp);
  assert_true(myCount == 0, "New container not empty: %d", myCount);
  hndbag_set_unique(cp, true);
  hndbag_set_item_destructor(cp, free_me);
  /*
   * Fill the container with objects
   */
  for (i = 0; i < samples; ++i) {
    sprintf(text, "Text instance: %d", i);
    texts[i] = strdup(text);
    status = hndbag_item_insert(cp, texts[i], &handle[i]);
    assert_true(status == 0, "bad insert status: %d", status);
    ++myCount;
    assert_true(myCount == hndbag_count(cp), "Incrementing(%d) failure (expect=%d, get=%d)", i, myCount, hndbag_count(cp));
  }
  assert_true(myCount == samples, "Full container not full: %d", myCount);
  /*
   * Retain two objects and add them to the list
   */
  status = hndbag_item_retain(cp, handle[i++]=handle[one]);
  assert_true(status == 0, "bad retain status: %d", status);
#if 0
  status = hndbag_item_retain(cp, handle[i++]=handle[two]);
  assert_true(status == 0, "bad retain status: %d", status);
#else
    status = hndbag_item_insert(cp, texts[two], &handle[i++]);
  assert_true(status == 0, "bad reinsert status: %d", status);
#endif
  assert_true(hndbag_count(cp) == samples, "Overfull handbag not full: %d", hndbag_count(cp));
  /*
   * Fetch and check the contents
   */
  myCount = hndbag_count(cp);
  vpp = hndbag_array(cp, reorder);
  if (assert_true(vpp, "array failed")) {
    for (i = 0; i < myCount; ++i) {
      char *text = vpp[i];
      char *colon = strchr(text, ':');
      if (assert_true(colon && *colon == ':', "Text(%d) not as expected: %s", i, text))
        assert_true(atoi(colon + 1) == i, "Value(%d) not as expected: %s => %d", i, text, atoi(colon + 1));
    }
    hndbag_array_release(cp, vpp);
  }
  vpp = hndbag_array_retain(cp, reorder);
  hndbag_array_release(cp, vpp);
  /*
   * Check that each element is in there once
   */
  memset(int_arr, 0, sizeof(int_arr));
  hndbag_visit(cp, counter, int_arr, NULL);
  for (i = 0; i < samples; ++i)
      assert_true(int_arr[i] == 1, "Entry %d appears %d times\n", i, int_arr[i]);
  /*
   * Fetch and check by handle
   */
  for (i = 0; i < samples + 2; ++i) {
    char *text;
    status = hndbag_get_pointer(cp, handle[i], (void**) &text);
    if (assert_true(status == 0, "Fetch pointer failed")) {
      char *colon = strchr(text, ':');
      if (assert_true(colon && *colon == ':', "Text(%d) not as expected: %s", i, text)) {
        if (i < samples)
          assert_true(atoi(colon + 1) == i, "Value(%d) not as expected: %s => %d", i, text, atoi(colon + 1));
        else
          assert_true(atoi(colon + 1) == (i == samples ? one : two), "Value(%d) not as expected: val=%d", i, atoi(colon + 1));
      }
    }
  }
  /*
   * Release the original two objects
   */
  status = hndbag_item_release(cp, &handle[one]);
  assert_true(status >= 0, "bad release status: %d", status);
  status = hndbag_item_release(cp, &handle[two]);
  assert_true(status >= 0, "bad release status: %d", status);
  assert_true(hndbag_count(cp) == samples, "Sparse container not full: %d", hndbag_count(cp));
  /*
   * Check that each element is in there once
   */
  memset(int_arr, 0, sizeof(int_arr));
  hndbag_visit(cp, counter, int_arr, NULL);
  for (i = 0; i < samples; ++i)
      assert_true(int_arr[i] == 1, "Entry %d appears %d times\n", i, int_arr[i]);
  /*
   * Take out two
   */
  vp = hndbag_find(cp, finder, texts[0]); /* first entry */
  if (assert_true(vp, "Failed to find 0")) {
    status = hndbag_item_release(cp, &handle[0]);
    if (assert_true(status >= 0, "bad release status on 0: %d", status)) {
      assert_true(handle[0] == 0, "vp not NULL on 0: %p", vp);
      --myCount;
    }
  }
  check_count(cp, myCount, "Release 0");
  vp = hndbag_find(cp, finder, texts[5]);
  if (assert_true(vp, "Failed to find 5")) {
    status = hndbag_item_release(cp, &handle[5]);
    if (assert_true(status >= 0, "bad release status on 5: %d", status)) {
      assert_true(handle[5] == 0, "vp not NULL on 5: %p", vp);
      --myCount;
    }
  }
  check_count(cp, myCount, "Release 5");
  /*
   * Release the rest
   */
  myCount = hndbag_count(cp);
  for (i = 0; i < samples + 2; ++i) {
    if (handle[i] == 0)
      continue;
    --myCount;
    status = hndbag_item_release(cp, &handle[i]);
    assert_true(status == 0, "bad release status: %d", status);
    assert_true(myCount == hndbag_count(cp), "Decrementing(%d) failure (expect=%d, get=%d)", i, myCount, hndbag_count(cp));
  }
  /*
   * Should now be empty
   */
  assert_true(0 == hndbag_count(cp), "Decremented failure (expect=%d, get=%d)", 0, hndbag_count(cp));
  hndbag_release(&cp);
}

int main(int argc, char *argv[])
{
  assert_true(local_num_bits(0) == 0, "Nbits 0");
  assert_true(local_num_bits(1) == 1, "Nbits 1");
  assert_true(local_num_bits(2) == 2, "Nbits 2");
  assert_true(local_num_bits(7) == 3, "Nbits 7");
  assert_true(local_num_bits(2047) == 11, "Nbits 2047");
  assert_true(local_num_bits(1000000) == 20, "Nbits 1000000");
  unit_test_01();
  if (fails > 0) {
    printf("FAILURE: there were %d errors.\n", fails);
    return EXIT_FAILURE;
  } else {
    printf("SUCCESS\n");
    return EXIT_SUCCESS;
  }
}
#endif /* UNIT_TEST */

