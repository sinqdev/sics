\subsection{SICS Telnet Connection}
SICS support communication through the standard TCP/IP telnet protocoll as
described in RFC-854. SICS implements telnet in the most primitive way: i.e.
no options are supported. In a later stage binary data transfer and
authentication options may be supported. But not in its first
implementation. After a telnet connection has been accepted a SICS task is   
 installed. In the first stage this task will answer any commands with a
'Fuck--off' message. Only if a special login command is given, the
connection will be verified and finally opened for commands or rejected.
If no login command has been sent within a reasonable intervall of time the
connection will be closed as well. This logic is implemented into a special
task function associated with a telnet connection. The rest of the telnet
logic is so tightly integrated with the network reading, that it has to live
in the network reader module. This section describes the special task
function and its associated data structure.

@d teldat @{
   typdef struct __TelTask {
                             SConnection *pCon;
                             int iLogin;
                             char pLoginWord[132];
                             time_t tStart;
                           } TelTask;
@}

The fields are: \begin{description}
\item[pCon] The connection object to act upon.
\item[iLogin] If the connection is logged in or not.
\item[pLoginWord] The login word to use. This word will be set as a SICS
server option. 
\item[tStart] The start time of the telnet connection. Used to check for
timeout on the connection.
\end{description}

The interface to this module looks like this:
@d telint @{
   typedef struct __TelTask *pTelTask;
/*--------------------------------------------------------------------------*/
   pTelTask CreateTelnet(SConnection *pCon);
   void     DeleteTelnet(void *pData);
/*---------------------------------------------------------------------------*/
   int TelnetTask(void *pData);
   void TelnetSignal(void *pData, int iSignal, void *pSigData);
/*--------------------------------------------------------------------------*/
   void InstallTelnet(void);
   void KillTelnet(void);
@}
\begin{description}
\item[CreateTelnet] creates a new telnet task object. Returns NULL on
failure, else apointer to a new data structure.
\item[DeleteTelnet] The deletion function for a telnet task.
\item[TelnetTask] The telnet task function.
\item[TelnetSignal] The telnet signal function.
\item[InstallTelnet] installs a telnet server port into the SICS system.
\item[KillTelnet] deletes the telnet server port from the system.
\end{description}

@o telnet.h @{
/*-------------------------------------------------------------------------
           S I C S  T E L N E T  S U P P O R T

  This file defines the telnet task function and its associated data
  structures. The body of the telnet handling code lives in the network
  reader module.

  Mark Koennecke, January 1998
---------------------------------------------------------------------------*/
#ifndef SICSTELNET
#define SICSTELNET
@<telint@>

#endif
 
@}
