/*--------------------------------------------------------------------------
                            P E R F M O N
                            
  A performance monitor for SICS.
  
  Mark Koennecke, Juli 1997
 
       Copyright:

       Labor fuer Neutronenstreuung
       Paul Scherrer Institut
       CH-5423 Villigen-PSI


      The authors hereby grant permission to use, copy, modify, distribute,
      and license this software and its documentation for any purpose, provided
      that existing copyright notices are retained in all copies and that this
      notice is included verbatim in any distributions. No written agreement,
      license, or royalty fee is required for any of the authorized uses.
      Modifications to this software may be copyrighted by their authors
      and need not follow the licensing terms described here, provided that
      the new terms are clearly indicated on the first page of each file where
      they apply.

      IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
      FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
      ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
      DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
      POSSIBILITY OF SUCH DAMAGE.

      THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
      INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
      FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
      IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
      NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
      MODIFICATIONS.
----------------------------------------------------------------------------*/
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include "fortify.h"
#include "sics.h"
#include "perfmon.h"
#include "perfmon.i"
/*-------------------------------------------------------------------------*/
static void *PerfMonInter(void *pData, int iInterface)
{
  pPerfMon self = NULL;

  self = (pPerfMon) pData;
  assert(self);

  if (iInterface == CALLBACKINTERFACE) {
    return self->pCall;
  }
  return NULL;
}

/*--------------------------------------------------------------------------*/
pPerfMon CreatePerfMon(int iInteg)
{

  pPerfMon pNew = NULL;
  time_t tCurrent;

  pNew = (pPerfMon) malloc(sizeof(PerfMon));
  if (!pNew) {
    return NULL;
  }
  memset(pNew, 0, sizeof(PerfMon));

  /* initialise Descriptor  */
  pNew->pDes = CreateDescriptor("PerfMon");
  if (!pNew->pDes) {
    free(pNew);
    return NULL;
  }
  pNew->pDes->GetInterface = PerfMonInter;

  /* initalise callback */
  pNew->pCall = CreateCallBackInterface();
  if (!pNew->pCall) {
    DeleteDescriptor(pNew->pDes);
    free(pNew);
    return NULL;
  }

  pNew->tLast = time(&tCurrent);
  pNew->tTarget = pNew->tLast + iInteg;
  pNew->iInteg = iInteg;

  return pNew;
}

/*--------------------------------------------------------------------------*/
void DeletePerfMon(void *pData)
{
  pPerfMon self = NULL;

  self = (pPerfMon) pData;
  assert(self);

  if (self->pDes) {
    DeleteDescriptor(self->pDes);
  }
  if (self->pCall) {
    DeleteCallBackInterface(self->pCall);
  }
  free(self);
}

/*--------------------------------------------------------------------------*/
int IncrementPerfMon(pPerfMon self)
{
  time_t tCurrent;
  char pBueffel[80];

  self->iCount++;

  time(&tCurrent);
  if (tCurrent > self->tTarget) {       /* recalculation necessary */
    self->fCPS = (float) self->iCount / (float) (tCurrent - self->tLast);
    self->iCount = 0;
    self->tLast = tCurrent;
    self->tTarget = tCurrent + self->iInteg;
    InvokeCallBack(self->pCall, VALUECHANGE, &self->fCPS);
    LogIS(VERBOSE,SSYS,"perfmon","%d:%f", self->iInteg, self->fCPS);
  }
  return 1;
}

/*---------------------------------------------------------------------------*/
float GetPerformance(pPerfMon self)
{
  assert(self);
  return self->fCPS;
}

/*------------------- The CallBack function for interest ------------------*/
static int InterestCallback(int iEvent, void *pEvent, void *pUser)
{
  float *fPos;
  SConnection *pCon;
  char pBueffel[80];

  assert(pEvent);
  assert(pUser);

  fPos = (float *) pEvent;
  pCon = (SConnection *) pUser;

  if (pCon == NULL || !SCisConnected(pCon)) {
    return -1;
  }

  snprintf(pBueffel,sizeof(pBueffel)-1, "Performance = %f", *fPos);
  SCWrite(pCon, pBueffel, eEvent);
  return 1;
}

/*----------------------------------------------------------------------------
  PerfMon understands this syntax:
          PerfMon               : prints current CPS value
          PerfMon on            : starts writing performance to ServerLog
          PerfMon off           : stops writing performance to ServLog
          PerfMon interest      : makes perfmon writes any change of its
                                  value to the connection regsitering this.
-----------------------------------------------------------------------------*/
int PerfMonWrapper(SConnection * pCon, SicsInterp * pSics, void *pData,
                   int argc, char *argv[])
{
  pPerfMon self = NULL;
  char pBueffel[132];
  long lID;

  self = (pPerfMon) pData;
  assert(self);
  assert(pCon);

  argtolower(argc, argv);
  if (argc < 2) {               /* print value */
    snprintf(pBueffel,sizeof(pBueffel)-1, "Performance =  %f", self->fCPS);
    SCWrite(pCon, pBueffel, eValue);
    return 1;
  }

  if (strcmp(argv[1], "on") == 0) {
    self->iLog = 1;
    SCSendOK(pCon);
    return 1;
  }

  if (strcmp(argv[1], "off") == 0) {
    self->iLog = 0;
    SCSendOK(pCon);
    return 1;
  }

  if (strcmp(argv[1], "interest") == 0) {
    lID = RegisterCallback(self->pCall,
                           VALUECHANGE, InterestCallback,
                           SCCopyConnection(pCon), SCDeleteConnection);
    SCSendOK(pCon);
    return 1;
  }

  if (strcmp(argv[1], "uninterest") == 0) {
    lID = RemoveCallbackCon(self->pCall, pCon);
    SCSendOK(pCon);
    return 1;
  }

  printf(pBueffel, "ERROR: unknown command %s", argv[1]);
  SCWrite(pCon, pBueffel, eError);
  return 0;
}

/*---------------------------------------------------------------------------*/
int PerfMonTask(void *pData)
{
  pPerfMon self = NULL;

  self = (pPerfMon) pData;
  assert(self);

  if (self->iEnd) {
    return 0;
  }
  IncrementPerfMon(self);
  return 1;
}

/*------------------------------------------------------------------------*/
void PerfMonSignal(void *pUser, int iSignal, void *pEventData)
{
  pPerfMon self = NULL;
  int *iInt;

  self = (pPerfMon) pUser;
  assert(self);
  iInt = (int *) pEventData;

  if (iSignal == SICSINT) {
    iInt = (int *) pEventData;
    if (*iInt == eEndServer) {
      self->iEnd = 1;
    }
  }
}
