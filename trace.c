/*
 * This is the trace facility for SICS. En detail logging for debugging complex
 * problems and more.
 *
 * This is ugly: I have to copy the code in the trace functions because I can only pass
 * down the argptr to lower level functions. But if I need space I have to reinitialize
 * that pointer....
 *
 * Another ugliness: this works fine as long as the messages are printed immediately.
 * Due to the implementation in conman.c and rs232controller.c the id may change if
 * there is a delay. If you need a queue, queue copies.
 *
 * There is another dependency here: the code in getTracePath relies on MakeSICSOBj
 * setting the property sicsdev.
 *
 * trace.c
 *
 *  Created on: Apr 27, 2011
 *      Author: koennecke
 *
 * Enhanced to write time stamps any ten minutes.
 * 
 * Mark Koennecke, October 2013
 *
 * Modified to write to the new joined log 
 *
 * Mark Koennecke, February 2016
 */

#include <time.h>
#include <math.h>
#include <trace.h>
#include <stdio.h>
#include <sics.h>
#include <sicshipadaba.h>
#include <sicsobj.h>

static FILE *logFD = NULL;
static char *logfile = NULL;
static int hdbInit = 0;
static int filterProv = 0;
static int debug = 0;
static int lastTen = -10;
static TaskTaskID traceStamperID = TASK_ID_BAD;
/*----------------------------------------------------------------------------------------*/
int traceActive()
{
  if (logFD == NULL){
    return 0;
  } else {
    return 1;
  }
}
/*----------------------------------------------------------------------------------------*/
static char *GetTracePath(pHdb node)
{
	  pHdb nodeStack[64];
	  int depth = 0, length = 1, i;
	  pHdb current = NULL;
	  char *pPtr = NULL;
	  char sicsdev[80];

	    /**
	     * build a nodestack and find out required string length for path
	     */
	  current = node;
	  while (current != NULL && GetHdbProperty(current,"sicsdev",sicsdev,sizeof(sicsdev)) == 0) {
		  length += strlen(current->name) + 2;
		  nodeStack[depth] = current;
		  depth++;
		  assert(depth < 64);
		  current = current->mama;
	  }
	  if(current != NULL){
		  length += strlen(current->name) + 2;
		  nodeStack[depth] = current;
		  depth++;
	  }

	  pPtr = malloc(length * sizeof(char));
	  if (pPtr == NULL) {
	    return NULL;
	  }
	  memset(pPtr, 0, length * sizeof(char));

	  /*
	   * we wish to decremement by one because above loop
	   * increments one to many and we wish to ignore the
	   * root node
	   */
	  for (i = depth - 1; i >= 0; i--) {
	    strcat(pPtr, "/");
	    strcat(pPtr, nodeStack[i]->name);
	  }
	  return pPtr;
}
/*----------------------------------------------------------------------------------------*/
static hdbCallbackReturn TraceCallback(pHdb node, void *userData,
                                            pHdbMessage message)
{
	pHdbDataMessage mm = NULL;
	char *pPath = NULL;
	pDynString result, printedData;

	  if ((mm = GetHdbUpdateMessage(message)) == NULL) {
	    return hdbContinue;
	  }

	  if(getHdbCheckSum(&node->value) == getHdbCheckSum(mm->v)){
		  return hdbContinue;
	  }

	  pPath = GetTracePath(node);
	  if(pPath == NULL){
		  return hdbContinue;
	  }

	  if (mm->v->arrayLength < 20) {
	    printedData = formatValue(*(mm->v), node);
	    if (printedData == NULL ) {
	    	return hdbContinue;
	    }
	    tracePar(pPath,"%s",GetCharArray(printedData));
	    DeleteDynString(printedData);
	  } else {
		  tracePar(pPath,"!!datachange!!");
	  }
	  free(pPath);

	  return hdbContinue;
}
/*---------------------------------------------------------------------------*/
int InstallTrace(pHdb node)
{
  pHdb currentChild = NULL;
  pHdbCallback noty = NULL;

  if(node->value.dataType == HIPOBJ || node->value.dataType == HIPFUNC){
	  return 1;
  }

  noty = MakeHipadabaCallback(TraceCallback, NULL, NULL);
  if (noty == NULL ) {
     return 0;
  }
  AppendHipadabaCallback(node, noty);

  currentChild = node->child;
  while (currentChild != NULL) {
	  InstallTrace(currentChild);
      currentChild = currentChild->next;
  }
  return 1;
}
/*-----------------------------------------------------------------*/
static void TraceObjects(void)
{
	CommandList *com = NULL;
	pDummy dum = NULL;

	com = pServ->pSics->pCList;
	while(com != NULL){
		dum = (pDummy)com->pData;
		if(dum->pDescriptor->parNode != NULL){
			InstallTrace(dum->pDescriptor->parNode);
		}
		com = com->pNext;
	}
}
/*-----------------------------------------------------------------*/
static int filterProvenance(char *sub, char *id)
{
	if(strstr(sub,"io") != NULL){
		return 0;
	}
	if(strstr(sub,"sys") != NULL && strstr(id,"perfmon") != NULL){
		return 0;
	}
	return 1;
}
/*-----------------------------------------------------------------*/
static int filter(char *sub, char *id)
{
	if(filterProv != 0){
		return filterProvenance(sub,id);
	} else {
		return 1;
	}
}
/*-----------------------------------------------------------------*/
static int strrepc(char *pszStr, char cFrom, char cTo)
{
      char  *ptr ;                              /* Pointer to string */
      int   iReturn = 0 ;                       /* No of replacements */

      /*----------------------------------------------------------------*/

      for (ptr = pszStr; ptr && *ptr; ++ptr) {
            if (*ptr == cFrom) {
              *ptr = cTo;
              ++iReturn;
            }
      }

      return( iReturn ) ;
}
/*-----------------------------------------------------------------*/
void traceprint(char *sub, char *id, char *data)
{
  unsigned int severity;
  
  if(strstr(data,"ERROR") != NULL){
    severity = ERROR;
  } else if(strstr(data,"WARNING") != NULL){
    severity = WARN;
  } else if(strstr(sub,"com") != NULL){
    severity = INFO;
  } else if(strstr(sub,"sys") != NULL){
    severity = INFO;
  } else if(strstr(sub,"par") != NULL){
    severity = VERBOSE;
  } else {
    severity = DEBUG;
  }
  Log(severity,sub,"%s:%s",id,data);
}
/*-----------------------------------------------------------------*/
void traceIO(char *id, char *format, ...)
{
	va_list argptr;
	char buffer[1024], *buf = NULL;
	int len;

	if(logFD != NULL && filter("io","id")){
		va_start(argptr,format);
		len = vsnprintf(buffer, sizeof(buffer),format,argptr);
		va_end(argptr);
		if(len >= sizeof(buffer)){
			buf = malloc(len+1);
			memset(buf,0,len+1);
			if(buf != NULL){
				va_start(argptr,format);
				len = vsnprintf(buf, len+1,format,argptr);
				va_end(argptr);
				traceprint("io",id,buf);
				free(buf);
			}
		} else {
			traceprint("io",id,buffer);
		}
	}
}
/*-----------------------------------------------------------------*/
void traceDevice(char *id, char *format, ...)
{
	va_list argptr;
	char buffer[1024], *buf = NULL;
	int len;

	if(logFD != NULL && filter("dev","id")){
		va_start(argptr,format);
		len = vsnprintf(buffer, sizeof(buffer),format,argptr);
		va_end(argptr);
		if(len >= sizeof(buffer)){
			buf = malloc(len+1);
			memset(buf,0,len+1);
			if(buf != NULL){
				va_start(argptr,format);
				len = vsnprintf(buf, len+1,format,argptr);
				va_end(argptr);
				traceprint("dev",id,buf);
			}
		} else {
			traceprint("dev",id,buffer);
		}
	}
}
/*-----------------------------------------------------------------*/
void tracePar(char *id, char *format, ...)
{
	va_list argptr;
	char buffer[1024], *buf = NULL;
	int len;

	if(logFD != NULL && filter("par","id")){
		va_start(argptr,format);
		len = vsnprintf(buffer, sizeof(buffer),format,argptr);
		va_end(argptr);
		if(len >= sizeof(buffer)){
			buf = malloc(len+1);
			memset(buf,0,len+1);
			if(buf != NULL){
				va_start(argptr,format);
				len = vsnprintf(buf, len+1,format,argptr);
				va_end(argptr);
				traceprint("par",id,buf);
			}
		} else {
			traceprint("par",id,buffer);
		}
	}
}
/*-----------------------------------------------------------------*/
void traceSys(char *id, char *format, ...)
{
	va_list argptr;
	char buffer[1024], *buf = NULL;
	int len;

	if(logFD != NULL && filter("sys","id")){
		va_start(argptr,format);
		len = vsnprintf(buffer, sizeof(buffer),format,argptr);
		va_end(argptr);
		if(len >= sizeof(buffer)){
			buf = malloc(len+1);
			memset(buf,0,len+1);
			if(buf != NULL){
				va_start(argptr,format);
				len = vsnprintf(buf, len+1,format,argptr);
				va_end(argptr);
				traceprint("sys",id,buf);
			}
		} else {
			traceprint("sys",id,buffer);
		}
	}
}
/*-----------------------------------------------------------------*/
void traceDebug(char *id, char *format, ...)
{
	va_list argptr;
	char buffer[1024], *buf = NULL;
	int len;

	if(logFD != NULL && debug == 1){
		va_start(argptr,format);
		len = vsnprintf(buffer, sizeof(buffer),format,argptr);
		va_end(argptr);
		if(len >= sizeof(buffer)){
			buf = malloc(len+1);
			memset(buf,0,len+1);
			if(buf != NULL){
				va_start(argptr,format);
				len = vsnprintf(buf, len+1,format,argptr);
				va_end(argptr);
				traceprint("debug",id,buf);
			}
		} else {
			traceprint("debug",id,buffer);
		}
	}
}
/*----------------------------------------------------------------*/
static void saveInitialParameters()
{
	CommandList *pCom = NULL;
	char prefix[512];
	pIDrivable pDriv = NULL;
	float value;
	pDummy pDum = NULL;

	if(logFD == NULL){
		return;
	}

	pCom = pServ->pSics->pCList;
	while(pCom != NULL){
		if(strcmp(pCom->pName,"restore") == 0){
			pCom = pCom->pNext;
			continue;
		}
		snprintf(prefix,sizeof(prefix),"par:start:%s", pCom->pName);
		pDum = (pDummy)pCom->pData;
		if(pDum != NULL){
			pDum->pDescriptor->SaveStatus(pCom->pData,prefix, logFD);
			pDriv = pDum->pDescriptor->GetInterface(pCom->pData, DRIVEID);
			if(pDriv != NULL){
				value = pDriv->GetValue(pCom->pData,pServ->dummyCon);
				fprintf(logFD,"%s:%f\n", prefix,value);
			}
		}
		pCom = pCom->pNext;
	}
	fprintf(logFD,"par:start:EOF\n");
}
/*----------------------------------------------------------------*/
static int TraceLogTask(void *data) 
{
  time_t iDate;
  struct tm *psTime;
  char pBuffer[132];
  int tenmin;

  if(logFD == NULL){
    return 0;
  }
  iDate = time(NULL);
  psTime = localtime(&iDate);
  tenmin = (int)floor(1.0*psTime->tm_min/10.);
  if(tenmin != lastTen){
    memset(pBuffer, 0, sizeof(pBuffer));
    strftime(pBuffer, sizeof(pBuffer)-1, "%Y-%m-%d@%H-%M-%S", psTime);
    lastTen = tenmin;
    traceSys("TIMESTAMP","%s",pBuffer);
  }

  return 1;
}
/*----------------------------------------------------------------*/
static int TraceLog(pSICSOBJ ccmd, SConnection * con,
		    Hdb * cmdNode, Hdb * par[], int nPar)
{
  char *filename = NULL;

  if(nPar < 1) {
    if(logFD != NULL){
      SCPrintf(con,eValue,"Tracing to %s", logfile);
    } else {
      SCWrite(con,"Tracing is off", eValue);
    }
    return 1;
  }

  filename = par[0]->value.v.text;
  if(strcmp(filename,"close") == 0 ){
    if(logFD != NULL){
      fclose(logFD);
      logFD = NULL;
      SCPrintf(con,eValue,"Tracing to %s closed", logfile);
      free(logfile);
    }
  } else {
    if(logFD != NULL){
      fclose(logFD);
      free(logfile);
    }
    logFD = fopen(filename,"w");
    if(logFD == NULL){
      SCPrintf(con,eError,"ERROR: failed to open %s for logging", filename);
      return 0;
    } else {
      logfile = strdup(filename);
      saveInitialParameters();
      SCPrintf(con,eValue,"Logging to %s", filename);
      if(hdbInit == 0){
	TraceObjects();
	hdbInit = 1;
      }
      if(!isTaskIDValid(pServ->pTasker, traceStamperID)){
	      traceStamperID = TaskRegisterN(pServ->pTasker,"tracestamper",
			   TraceLogTask, NULL, NULL, NULL, TASK_PRIO_HIGH);
	  }
    }
  }
  return 1;
}
/*----------------------------------------------------------------*/
static int TraceAppend(pSICSOBJ ccmd, SConnection * con,
		       Hdb * cmdNode, Hdb * par[], int nPar)
{
  char *filename = NULL;

  if(nPar < 1) {
    if(logFD != NULL){
      SCPrintf(con,eValue,"Tracing to %s", logfile);
    } else {
      SCWrite(con,"Tracing is off", eValue);
    }
    return 1;
  }

  filename = par[0]->value.v.text;
  /*
    This is good even if appending to the same file:
    it flushes all buffers
  */
  if(logFD != NULL){
    fclose(logFD);
    free(logfile);
  }
  logFD = fopen(filename,"a+");
  if(logFD == NULL){
    SCPrintf(con,eError,"ERROR: failed to open %s for logging", filename);
    return 0;
  } else {
    logfile = strdup(filename);
    saveInitialParameters();
    SCPrintf(con,eValue,"Logging to %s", filename);
    if(hdbInit == 0){
      TraceObjects();
      hdbInit = 1;
    }
    TaskRegisterN(pServ->pTasker,"tracestamper",
		  TraceLogTask, NULL, NULL, NULL, TASK_PRIO_HIGH);
  }
  return 1;
}

/*-----------------------------------------------------------------------------*/
static int TraceDebug(pSICSOBJ ccmd, SConnection * con,
                         Hdb * cmdNode, Hdb * par[], int nPar)
{
	if(nPar >= 1){
		debug = par[0]->value.v.intValue;
	}
	SCPrintf(con,eValue, "trace.debug = %d", debug);
	return 1;
}
/*-----------------------------------------------------------------------------*/
static int TraceFilter(pSICSOBJ ccmd, SConnection * con,
                         Hdb * cmdNode, Hdb * par[], int nPar)
{
	if(nPar >= 1){
		filterProv = par[0]->value.v.intValue;
	}
	SCPrintf(con,eValue, "trace.filter = %d", filterProv);
	return 1;
}
/*-----------------------------------------------------------------------------*/
static void KillTrace(void *data)
{
  
	/* if(logFD != NULL){ */
	/* 	fclose(logFD); */
	/* 	free(logfile); */
	/* 	logFD = NULL; */
	/* 	logfile = NULL; */
	/* } */
}
/*-----------------------------------------------------------------------------------------*/
void MakeTrace(void)
{
  SICSOBJ *ccmd;
  pHdb cmd;

  ccmd = MakeSICSOBJv("trace", "SicsTrace", HIPNONE, usSpy);
  if(ccmd == NULL){
	  SCWrite(pServ->dummyCon,"ERROR: out of memory creating trace", eError);
	  return;
  }

  /* cmd = AddSICSHdbPar(ccmd->objectNode, */
  /*                     "log", usMugger, MakeSICSFunc(TraceLog)); */
  /* AddSICSHdbPar(cmd, "filename", usMugger, MakeHdbText("")); */

  /* cmd = AddSICSHdbPar(ccmd->objectNode, */
  /*                     "append", usMugger, MakeSICSFunc(TraceAppend)); */
  /* AddSICSHdbPar(cmd, "filename", usMugger, MakeHdbText("")); */


  cmd = AddSICSHdbPar(ccmd->objectNode,
                      "filter", usMugger, MakeSICSFunc(TraceFilter));
  AddSICSHdbPar(cmd, "selection", usMugger, MakeHdbInt(0));

  cmd = AddSICSHdbPar(ccmd->objectNode,
                      "debug", usMugger, MakeSICSFunc(TraceDebug));
  AddSICSHdbPar(cmd, "switch", usMugger, MakeHdbInt(0));

  AddCommand(pServ->pSics, "trace", InterInvokeSICSOBJ, KillTrace, ccmd);

  /*
    This is a really ugly hack to make tracing happen while we are switching 
    to the logging system
  */
  logFD = (FILE*) 27;
}
