
/*---------------------------------------------------------------------------
                          F I T  C E N T E R

 A simple peak finding and center of gravity determination facility for
 SICS.

 copyright: see copyright.h

 Mark Koennecke, October 1997

 Added center of edge finding

 Mark Koennecke, Octover 2011
----------------------------------------------------------------------------*/
#ifndef SICSFITCENTER
#define SICSFITCENTER

typedef struct __FitCenter *pFit;

/*--------------------------------------------------------------------------*/
pFit CreateFitCenter(pScanData pScan);
void DeleteFitCenter(void *pData);
/*-------------------------------------------------------------------------*/
int CalculateFit(pFit self);
  /* 
     CalcluateFit returns: -1 when left FWHM could not be found
     -2 when right FWHM could not be found
     1 on success
   */
int CalculateFitFromData(pFit self, float fAxis[], long lSum[], int iLen);
void GetFitResults(pFit self, float *fNewCenter, float *fStdDev,
                   float *FWHM, float *fMax);
int DriveCenter(pFit self, SConnection * pCon, SicsInterp * pSics);
/*-------------------------------------------------------------------------*/
int FitFactory(SConnection * pCon, SicsInterp * pSics, void *pData,
               int argc, char *argv[]);
int FitWrapper(SConnection * pCon, SicsInterp * pSics, void *pData,
               int argc, char *argv[]);
int EdgeWrapper(SConnection * pCon, SicsInterp * pSics, void *pData,
               int argc, char *argv[]);
int CenterWrapper(SConnection * pCon, SicsInterp * pSics, void *pData,
                  int argc, char *argv[]);

#endif
