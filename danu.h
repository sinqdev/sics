
#line 53 "danu.w"

/*-----------------------------------------------------------------------
                        D A T A N U M B E R

  A module to provide a unique data number for data file writing.

  Mark Koennecke, Juli 1997
  
  copyright: see implementation file.

---------------------------------------------------------------------------*/
#ifndef SICSDATANUMBER
#define SICSDATANUMBER


#include "SCinter.h"
#include "conman.h"

#line 15 "danu.w"

typedef struct __DataNumber *pDataNumber;

pDataNumber CreateDataNumber(char *pFilename);
void DeleteDataNumber(void *pData);

int IncrementDataNumber(pDataNumber self, int *iYear);

int DecrementDataNumber(pDataNumber self);

int DNWrapper(SConnection * pCon, SicsInterp * pSics, void *pData,
              int argc, char *argv[]);

int DEWrapper(SConnection * pCon, SicsInterp * pSics, void *pData,
              int argc, char *argv[]);

int DNFactory(SConnection * pCon, SicsInterp * pSics, void *pData,
              int argc, char *argv[]);

#line 67 "danu.w"


#endif
