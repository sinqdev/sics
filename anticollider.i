
/*-------------------------------------------------------------------------
  Anticollider internal data structure definition. Generated from
  anticollider.w. Do not edit.
-------------------------------------------------------------------------*/

        typedef struct __ANTICOLLIDER{
                pObjectDescriptor pDes;
                pIDrivable pDriv;
                int motorList;
                int sequenceList;
                char *colliderScript;
                int isDirty;
                int level;
        }AntiCollider, *pAntiCollider;


  typedef struct {
                        int level;
                        char pMotor[80];
                        float target;
                }Sequence;

  int StartLevel(int level, int sequenceList, int motorList,
                SConnection *pCon);



