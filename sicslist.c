/*
  A module for displaying various bits of information about SICS
  internals and objects.
  
  copyright: see file COPYRIGHT
  
  Mark Koennecke, January 2006 
 */
#include "sicslist.h"
#include "stringdict.h"
#include "dynstring.h"
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <tcl.h>
#ifdef SITE_ANSTO
const char *LIST_SEP = "";
const char *LIST_TRM = "";
const char *listmode = "ANSTO";
#else
const char *LIST_SEP = ",";
const char *LIST_TRM = "ENDLIST";
const char *listmode = "PSI";
#endif
/*------------------------------------------------------------------*/
static void listAllObjects(SConnection * pCon, SicsInterp * pSics)
{
  CommandList *pCom = NULL;
  pStringDict dict;
  const char *keyVal;
  char value[20];
  pDynString lst;

  pCom = pSics->pCList;
  dict = CreateStringDict();
  while (pCom != NULL) {
    StringDictAddPair(dict, pCom->pName, "");
    pCom = pCom->pNext;
  }

  StringDictKillScan(dict);
  lst = DynStringCreate(100, 100);
  while ((keyVal = StringDictGetNext(dict, value, sizeof(value)))) {
    DynStringPrintf(lst, "%s", keyVal);
    DynStringPrintf(lst, "%s ", LIST_SEP);
  }
  DynStringPrintf(lst, "%s", LIST_TRM);
  SCWrite(pCon, DynStringGetArray(lst), eValue);
  DynStringDelete(lst);
  DeleteStringDict(dict);
}

/*------------------------------------------------------------------*/
static void listAllObjectData(SConnection * pCon, char *name, pDummy data)
{
  pDynString lst;
  IPair *prop = NULL;

  lst = DynStringCreate(100, 100);
  if (data == NULL) {
    DynStringPrintf(lst, "%s = command, ", name);
  } else {
    DynStringPrintf(lst, "%s=obj, ", name);
    DynStringPrintf(lst, "type=%s, ", data->pDescriptor->name);
    if (data->pDescriptor->GetInterface(data, DRIVEID) != NULL) {
      DynStringPrintf(lst, "drivable=true, ");
    } else {
      DynStringPrintf(lst, "drivable=false, ");
    }
    if (data->pDescriptor->GetInterface(data, COUNTID) != NULL) {
      DynStringPrintf(lst, "countable=true, ");
    } else {
      DynStringPrintf(lst, "countable=false, ");
    }
    if (data->pDescriptor->GetInterface(data, CALLBACKINTERFACE) != NULL) {
      DynStringPrintf(lst, "callback=true, ");
    } else {
      DynStringPrintf(lst, "callback=false, ");
    }
    if (data->pDescriptor->GetInterface(data, ENVIRINTERFACE) != NULL) {
      DynStringPrintf(lst, "environment=true, ");
    } else {
      DynStringPrintf(lst, "environment=false, ");
    }
    prop = data->pDescriptor->pKeys;
    while (prop != NULL) {
      DynStringPrintf(lst, "%s=%s, ", prop->name, prop->value);
      prop = prop->pNext;
    }
  }
  DynStringPrintf(lst, "%s", LIST_TRM);
  SCWrite(pCon, DynStringGetArray(lst), eValue);
  DynStringDelete(lst);
}

/*------------------------------------------------------------------*/
static int getObjectData(pDummy obj, char *key, char buffer[512])
{
  char *ptr = NULL;
  void *iPtr = NULL;
  int status = 0;

  /*
   * first check attributes
   */
  ptr = IFindOption(obj->pDescriptor->pKeys, key);
  if (ptr != NULL) {
    snprintf(buffer, 511, "%s", ptr);
    return 1;
  }
  /*
   * check indirect attributes
   */
  strtolower(key);
  if (strcmp(key, "type") == 0) {
    snprintf(buffer, 511, "%s", obj->pDescriptor->name);
    return 1;
  } else if (strcmp(key, "drivable") == 0) {
    iPtr = obj->pDescriptor->GetInterface(obj, DRIVEID);
    if (iPtr != NULL) {
      status = 1;
      snprintf(buffer, 511, "true");
    } else {
      status = 0;
      snprintf(buffer, 511, "false");
    }
  } else if (strcmp(key, "countable") == 0) {
    iPtr = obj->pDescriptor->GetInterface(obj, COUNTID);
    if (iPtr != NULL) {
      status = 1;
      snprintf(buffer, 511, "true");
    } else {
      status = 0;
      snprintf(buffer, 511, "false");
    }
  } else if (strcmp(key, "callback") == 0) {
    iPtr = obj->pDescriptor->GetInterface(obj, CALLBACKINTERFACE);
    if (iPtr != NULL) {
      status = 1;
      snprintf(buffer, 511, "true");
    } else {
      status = 0;
      snprintf(buffer, 511, "false");
    }
  } else if (strcmp(key, "environment") == 0) {
    iPtr = obj->pDescriptor->GetInterface(obj, ENVIRINTERFACE);
    if (iPtr != NULL) {
      status = 1;
      snprintf(buffer, 511, "true");
    } else {
      status = 0;
      snprintf(buffer, 511, "false");
    }
  } else {
    return -1;
  }
  return status;
}

/*-----------------------------------------------------------------*/
static int printObjectData(SConnection * pCon, pDummy obj, char *key)
{
  char value[512];
  char buffer[1024];
  int status;

  status = getObjectData(obj, key, value);
  if (status >= 0) {
    snprintf(buffer, 1023, "%s=%s", key, value);
  } else {
    snprintf(buffer, 1023, "%s=UNDEFINED", key);
  }
  SCWrite(pCon, buffer, eValue);
  if (status < 0) {
    return 0;
  } else {
    return 1;
  }
}

static int existsObjectData(SConnection * pCon, pDummy obj, char *key)
{
  char *ptr = NULL;
  ptr = IFindOption(obj->pDescriptor->pKeys, key);
  if (ptr != NULL)
    SCPrintf(pCon, eValue, "%s", "true");
  else
    SCPrintf(pCon, eValue, "%s", "false");

  return 1;
}

/*-----------------------------------------------------------------
 * this function implements a set on top of a dict. This means that
 * the dict is first searched for the occurence of name. name is only
 * added when it does not exist in dict
 * ----------------------------------------------------------------*/

/*----------------------------------------------------------------*/
static void printKeyTypes(SicsInterp * pSics, SConnection * pCon,
                          char *key)
{
  CommandList *pCom = NULL;
  pStringDict dict;
  int status;
  char value[512];
  const char *keyVal;
  pDynString result;

  if (strcmp(key, "interface") == 0) {
    SCWrite(pCon, "drivable, countable, callback, environment", eValue);
    return;
  }

  dict = CreateStringDict();
  pCom = pSics->pCList;
  while (pCom != NULL) {
    status = getObjectData((pDummy) pCom->pData, key, value);
    if (status >= 0) {
      StringDictAddPair(dict, value, "");
    }
    pCom = pCom->pNext;
  }

  result = DynStringCreate(100, 100);
  StringDictKillScan(dict);
  while ((keyVal = StringDictGetNext(dict, value, sizeof(value)))) {
    DynStringPrintf(result, "%s", keyVal);
    DynStringPrintf(result, "%s ", LIST_SEP);
  }
  DynStringPrintf(result, "%s", LIST_TRM);
  SCWrite(pCon, DynStringGetArray(result), eValue);
  DynStringDelete(result);
  DeleteStringDict(dict);
}

/*----------------------------------------------------------------*/
static void printObjectsMatchingKeyVal(SicsInterp * pSics,
                                       SConnection * pCon, char *key,
                                       char *value)
{
  CommandList *pCom = NULL;
  pStringDict dict;
  pDynString result;
  int status;
  const char *keyVal;
  char buffer[512];

  dict = CreateStringDict();
  pCom = pSics->pCList;
  strtolower(value);
  while (pCom != NULL) {
    if (strcmp(key, "interface") == 0) {
      status = getObjectData((pDummy) pCom->pData, value, buffer);
    } else {
      status = getObjectData((pDummy) pCom->pData, key, buffer);
      if (status == 1) {
        strtolower(buffer);
        if (strcmp(buffer, value) == 0) {
          status = 1;
        } else {
          status = 0;
        }
      }
    }
    if (status == 1) {
      StringDictAddPair(dict, pCom->pName, "");
    }
    pCom = pCom->pNext;
  }
  result = DynStringCreate(100, 100);
  StringDictKillScan(dict);
  while ((keyVal = StringDictGetNext(dict, buffer, sizeof(buffer)))) {
    DynStringPrintf(result, "%s", keyVal);
    DynStringPrintf(result, "%s ", LIST_SEP);
  }
  DynStringPrintf(result, "%s", LIST_TRM);
  SCWrite(pCon, DynStringGetArray(result), eValue);
  DynStringDelete(result);
}

/*----------------------------------------------------------------*/
static int setAttribute(SConnection * pCon, SicsInterp * pSics,
                        char *obj, char *key, char *value)
{
  CommandList *pCom = NULL;
  pDummy pDum = NULL;

  if (!SCMatchRights(pCon, usMugger)) {
    return 0;
  }

  strtolower(key);
  if (strcmp(key, "type") == 0 || strcmp(key, "interface") == 0) {
    SCWrite(pCon, "ERROR: this key is forbidden", eError);
    return 0;
  }

  pCom = FindCommand(pSics, obj);
  if (pCom == NULL) {
    SCWrite(pCon, "ERROR: object not found", eError);
    return 0;
  }
  pDum = (pDummy) pCom->pData;

  if (pDum != NULL) {
    if (IFindOption(pDum->pDescriptor->pKeys, key) == NULL) {
      pDum->pDescriptor->pKeys = IFAddOption(pDum->pDescriptor->pKeys,
                                             key, value);
    } else {
      IFSetOption(pDum->pDescriptor->pKeys, key, value);
    }
  }
  SCSendOK(pCon);

  return 1;
}

/*-----------------------------------------------------------------*/
static int printObjStatus(SConnection * pCon, SicsInterp * pSics,
                          char *name)
{
  CommandList *pCom = NULL;
  pDummy pDum = NULL;
  pIDrivable pDriv = NULL;
  pICountable pCount = NULL;
  pEVInterface pEV = NULL;
  int status = -1, evStatus = -1;
  char buffer[256];

  pCom = FindCommand(pSics, name);
  if (pCom == NULL) {
    SCWrite(pCon, "ERROR: object not found", eError);
    return 0;
  }
  pDum = (pDummy) pCom->pData;
  if (pDum != NULL) {
    pDriv = GetDrivableInterface(pDum);
    if (pDriv != NULL) {
      status = pDriv->CheckStatus(pDum, pCon);
    }
    pCount = GetCountableInterface(pDum);
    if (pCount != NULL) {
      status = pCount->CheckCountStatus(pDum, pCon);
    }
    pEV = pDum->pDescriptor->GetInterface(pDum, ENVIRINTERFACE);
    if (pEV != NULL) {
      evStatus = pEV->IsInTolerance(pDum);
    }
  }
  switch (status) {
  case HWBusy:
    snprintf(buffer, 255, "%s = running", pCom->pName);
    break;
  case HWIdle:
  case OKOK:
    if (evStatus < 0) {
      snprintf(buffer, 255, "%s = idle", pCom->pName);
    } else if (evStatus == 1) {
      snprintf(buffer, 255, "%s = intolerance", pCom->pName);
    } else {
      snprintf(buffer, 255, "%s = outoftolerance", pCom->pName);
    }
    break;
  case HWFault:
  case HWPosFault:
    snprintf(buffer, 255, "%s = faulty", pCom->pName);
    break;
  case HWPause:
    snprintf(buffer, 255, "%s = paused", pCom->pName);
    break;
  case HWNoBeam:
    snprintf(buffer, 255, "%s = nobeam", pCom->pName);
    break;
  default:
    snprintf(buffer, 255, "%s = nostatus", pCom->pName);
    break;
  }
  SCWrite(pCon, buffer, eValue);
  return 1;
}

/*-----------------------------------------------------------------*/
static int printServer(SConnection * pCon)
{
  pStringDict dict;
  const char *keyVal;
  pDynString txt;
  IPair *current = NULL;
  char buffer[512];

  dict = CreateStringDict();
  current = pSICSOptions;
  while (current != NULL) {
    StringDictAddPair(dict, current->name, current->value);
    current = current->pNext;
  }
  txt = DynStringCreate(100, 100);
  StringDictKillScan(dict);
  while ((keyVal = StringDictGetNext(dict, buffer, sizeof(buffer)))) {
    DynStringPrintf(txt, "%s=%s%s ", keyVal, buffer, LIST_SEP);
  }
  DynStringPrintf(txt, "%s", LIST_TRM);
  SCWrite(pCon, DynStringGetArray(txt), eValue);
  DynStringDelete(txt);
  DeleteStringDict(dict);
  return 1;
}

/*------------------------------------------------------------------*/
static int printObjectPar(SConnection * pCon, SicsInterp * pSics,
                          char *obj)
{
  CommandList *pCom = NULL;
  FILE *fd = NULL;
  char *buffer = NULL, tmpfile[80];
  pDummy pDum = NULL;
  long length;
  struct stat statbuf;

  snprintf(tmpfile, 80, "SICS%6.6d.dat", getpid());
  pCom = FindCommand(pSics, obj);
  if (pCom == NULL) {
    SCWrite(pCon, "ERROR: object to list not found", eError);
    return 0;
  }
  fd = fopen(tmpfile, "w");
  if (fd == NULL) {
    SCWrite(pCon, "ERROR: failed to open tmpfile", eError);
    return 0;
  }
  pDum = (pDummy) pCom->pData;
  if (pDum != NULL) {
    pDum->pDescriptor->SaveStatus(pDum, obj, fd);
    fclose(fd);
    stat(tmpfile, &statbuf);
    length = statbuf.st_size;
    fd = fopen(tmpfile, "r");
    if (fd == NULL) {
      SCWrite(pCon, "ERROR: failed to open tmpfile", eError);
      return 0;
    }
    buffer = malloc(length + 1);
    if (buffer == NULL) {
      SCWrite(pCon, "ERROR: out of memory in list par", eError);
      fclose(fd);
      unlink(tmpfile);
      return 0;
    }
    memset(buffer, 0, length + 1);
    fread(buffer, length, 1, fd);
    fclose(fd);
    SCWrite(pCon, buffer, eValue);
    free(buffer);
    unlink(tmpfile);
    return 1;
  }
  return 0;
}

/*-----------------------------------------------------------------*/
extern int match(const char *mask, const char *name);   /* from wwildcard.c */

static void printMatch(SConnection * pCon, SicsInterp * pSics, char *mask)
{
  CommandList *current = NULL;
  pStringDict dict;
  const char *keyVal;
  pDynString txt;
  char buffer[512];

  dict = CreateStringDict();
  current = pSics->pCList;
  while (current != NULL) {
    if (!match(mask, current->pName)) {
      StringDictAddPair(dict, current->pName, "");
    }
    current = current->pNext;
  }
  txt = DynStringCreate(100, 100);
  StringDictKillScan(dict);
  while ((keyVal = StringDictGetNext(dict, buffer, sizeof(buffer)))) {
      DynStringPrintf(txt, "%s%s ", keyVal, LIST_SEP);
  }
  DynStringPrintf(txt, "%s", LIST_TRM);
  SCWrite(pCon, DynStringGetArray(txt), eValue);
  DynStringDelete(txt);
  DeleteStringDict(dict);
}

/*==================================================================*/
int SicsList(SConnection * pCon, SicsInterp * pSics, void *pData,
             int argc, char *argv[])
{
  CommandList *pCom = NULL;

  if (argc < 2) {
    listAllObjects(pCon, pSics);
    return 1;
  }

  if (strcmp(argv[1], "objstatus") == 0) {
    if (argc < 3) {
      SCWrite(pCon, "ERROR: Insufficient number of arguments to status",
              eError);
      return 0;
    }
    return printObjStatus(pCon, pSics, argv[2]);
  }

  if (strcmp(argv[1], "server") == 0) {
    printServer(pCon);
    return 1;
  }

  if (strcmp(argv[1], "par") == 0) {
    if (argc > 2) {
      return printObjectPar(pCon, pSics, argv[2]);
    } else {
      SCWrite(pCon, "ERROR: not enough arguments to list par", eError);
      return 0;
    }
  }

  if (strcmp(argv[1], "match") == 0) {
    if (argc > 2) {
      printMatch(pCon, pSics, argv[2]);
      return 1;
    } else {
      SCWrite(pCon, "ERROR: not enough arguments to list match", eError);
      return 0;
    }
  }

  if (strcmp(argv[1], "exists") == 0) {
    if (argc < 4) {
      SCWrite(pCon, "ERROR: not enough arguments", eError);
      return 0;
    } else {
      pCom = FindCommand(pSics, argv[2]);
      if (pCom == NULL) {
        SCWrite(pCon, "ERROR: Object doesn't exist", eError);
        return 0;
      } else
        return existsObjectData(pCon, (pDummy) pCom->pData, argv[3]);
    }
  }


  /*
   * object properties
   */
  pCom = FindCommand(pSics, argv[1]);
  if (pCom != NULL) {
    if (argc < 3) {
      listAllObjectData(pCon, argv[1], (pDummy) pCom->pData);
      return 1;
    } else {
      return printObjectData(pCon, (pDummy) pCom->pData, argv[2]);
    }
  }

  /*
   * attribute setting and status
   */
  if (strcmp(argv[1], "setatt") == 0) {
    if (argc < 5) {
      SCWrite(pCon, "ERROR: Insufficient number of arguments to setatt",
              eError);
      return 0;
    }
    return setAttribute(pCon, pSics, argv[2], argv[3], argv[4]);
  }

  if (strcasecmp(argv[1], "listmode") == 0) {
    if (argc == 3) {
      if (strcasecmp(argv[2], "ansto") == 0) {
        listmode = "ANSTO";
        LIST_SEP = "";
        LIST_TRM = "";
        return 1;
      }
      if (strcasecmp(argv[2], "psi") == 0) {
        listmode = "PSI";
        LIST_SEP = ",";
        LIST_TRM = "ENDLIST";
        return 1;
      }
    }
    SCPrintf(pCon, eValue, "listmode = %s", listmode);
    return 1;
  }
  /*
   * classes and class membership
   */
  if (argc < 3) {
    printKeyTypes(pSics, pCon, argv[1]);
    return 1;
  } else {
    printObjectsMatchingKeyVal(pSics, pCon, argv[1], argv[2]);
    return 1;
  }
  return 0;
}
