
/*------------------------------------------------------------------------
                            X Y T A B L E 

 a helper class for holding an X-Y list of values.

 copyright: see copyright.h

 Mark Koennecke, June 1999
----------------------------------------------------------------------------*/
#ifndef XYTABLE
#define XYTABLE

typedef struct __XYTABLE *pXYTable;
/*------------------------------------------------------------------------*/
int XYClear(pXYTable self);
int XYAdd(pXYTable self, float x, float y);
int XYWrite(pXYTable self, FILE * fd);
int XYSendUU(pXYTable self, SConnection * pCon);
int XYList(pXYTable self, SConnection * pCon);
/*----------------------- interpreter interface --------------------------*/
int XYFactory(SConnection * pCon, SicsInterp * pSics, void *pData,
              int argc, char *argv[]);
int XYAction(SConnection * pCon, SicsInterp * pSics, void *pData,
             int argc, char *argv[]);

#endif
