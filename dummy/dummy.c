/*------------------------------------------------------------------------
                                  D U M M Y

 This is an empty site interface for SICS. Can be used as a starting
 point for own site specific stuff.

 copyright: see file COPYRIGHT

 Mark Koennecke, June 2003
 -----------------------------------------------------------------------*/
#include <stdlib.h>
#include <assert.h>
#include <fortify.h>
#include <sics.h>
#include <motor.h>
#include <tcl.h>
#include <site.h>

static pSite siteDummy = NULL;

/*----------------------------------------------------------------------*/
static void AddDummyCommands(SicsInterp *pInter){
}
/*---------------------------------------------------------------------*/
static void RemoveDummyCommands(SicsInterp *pSics){
}
/*-------------------------------------------------------------------*/
static pMotor CreateDummyMotor(SConnection *pCon, int argc, char *argv[]){
  pMotor pNew = NULL;
  return pNew;
}
/*-------------------------------------------------------------------*/
static pCounterDriver CreateDummyCounterDriver(SConnection *pCon,
					     int argc,
					     char *argv[]){
  pCounterDriver pNew = NULL;
  return pNew;
}
/*-------------------------------------------------------------------*/
static HistDriver *CreateDummyHistMem(char *name, pStringDict pOptions){
  HistDriver *pNew = NULL;
  
  return pNew;
}
/*-------------------------------------------------------------------*/
static pVelSelDriv CreateDummyVelSelDriv(char *name, char *array, 
				       Tcl_Interp *pTcl){
  pVelSelDriv pNew = NULL;
  return pNew;
}
/*-------------------------------------------------------------------*/
static pCodri CreateDummyController(SConnection *pCon,int argc, char *argv[]){
  pCodri pNew = NULL;
  return pNew;
}
/*------------------------------------------------------------------*/
static pEVControl InstallDummyEnvironmentController(SicsInterp *pSics,
						  SConnection *pCon,
						 int argc, char *argv[]){
  pEVControl pNew = NULL;
  pEVDriver pDriv = NULL;

  return pNew;
}
/*-----------------------------------------------------------------*/
static int ConfigureDummyScan(pScanData self, char *option){
  return 0;
}
/*--------------------------------------------------------------------*/
static void KillDummySite(void *site){
  free(site);
  siteDummy = NULL;
}
/*---------------------------------------------------------------------
  The scheme here goes along the lines of the singleton design pattern
  ---------------------------------------------------------------------*/
pSite getSite(void){
  if(siteDummy == NULL){
    siteDummy = (pSite)malloc(sizeof(Site));
    /*
      we  cannot go on if we do not even have enough memory to allocate
      the site data structure
    */
    assert(siteDummy); 
    /*
      initializing function pointers
    */
    siteDummy->AddSiteCommands = AddDummyCommands;
    siteDummy->RemoveSiteCommands = RemoveDummyCommands;
    siteDummy->CreateMotor = CreateDummyMotor;
    siteDummy->CreateCounterDriver = CreateDummyCounterDriver;
    siteDummy->CreateHistogramMemoryDriver = CreateDummyHistMem; 
    siteDummy->CreateVelocitySelector = CreateDummyVelSelDriv;
    siteDummy->CreateControllerDriver = CreateDummyController;
    siteDummy->InstallEnvironmentController = 
          InstallDummyEnvironmentController;
    siteDummy->ConfigureScan = ConfigureDummyScan;
    siteDummy->KillSite = KillDummySite;
  }
  return siteDummy;
}

