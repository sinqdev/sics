/*----------------------------------------------------------------------------
   This is a counter driver which wraps the McStas controller into a
   counter driver. 
   
   copyright: see file COPYRIGHT
   
   Mark Koennecke, June 2005
-----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include "sics.h"
#include "countdriv.h"
#include "mccontrol.h"
/*-------------------------------------------------------------------------*/
#define SCRIPTERROR      -110
#define NOTIMPLEMENTED   -111
/*====================== interface functions ==============================*/
static int McStatus(struct __COUNTER *self, float *fControl)
{
  int status;
  pMcStasController pMcStas = NULL;

  pMcStas = (pMcStasController) self->pData;

  status = McStasStatus(pMcStas, fControl);
  if (status == HWFault) {
    self->iErrorCode = SCRIPTERROR;
    return status;
  }
  return status;
}

/*--------------------------------------------------------------------------*/
static int McStart(struct __COUNTER *self)
{
  int status;
  pMcStasController pMcStas = NULL;

  pMcStas = (pMcStasController) self->pData;

  memset(self->lCounts, 0, MAXCOUNT * sizeof(long));
  status = McStasStart(pMcStas, self->eMode, self->fPreset);
  if (status == HWFault) {
    self->iErrorCode = SCRIPTERROR;
  }
  return status;
}

/*--------------------------------------------------------------------------*/
static int McPause(struct __COUNTER *self)
{
  self->iErrorCode = NOTIMPLEMENTED;
  return HWFault;
}

/*--------------------------------------------------------------------------*/
static int McContinue(struct __COUNTER *self)
{
  self->iErrorCode = NOTIMPLEMENTED;
  return HWFault;
}

/*--------------------------------------------------------------------------*/
static int McHalt(struct __COUNTER *self)
{
  int status;
  pMcStasController pMcStas = NULL;

  pMcStas = (pMcStasController) self->pData;

  status = McStasStop(pMcStas);
  if (status == HWFault) {
    self->iErrorCode = SCRIPTERROR;
  }
  return status;
}

/*--------------------------------------------------------------------------*/
static int McReadValues(struct __COUNTER *self)
{
  int status;
  pMcStasController pMcStas = NULL;

  pMcStas = (pMcStasController) self->pData;

  status = McStasTransferData(pMcStas);
  if (status == HWFault) {
    self->iErrorCode = SCRIPTERROR;
  }
  self->fTime = time(NULL) - pMcStas->startTime;
  return status;
}

/*--------------------------------------------------------------------------*/
static int McGetError(struct __COUNTER *self, int *iCode, char *error,
                      int iErrLen)
{
  pMcStasController pMcStas = NULL;
  pMcStas = (pMcStasController) self->pData;

  switch (self->iErrorCode) {
  case SCRIPTERROR:
    McStasGetError(pMcStas, error, iErrLen);
    break;
  case NOTIMPLEMENTED:
    strlcpy(error, "Feauture not implemented", iErrLen);
    break;
  default:
    strlcpy(error, "Unknown error code in McStas Driver", iErrLen);
    break;
  }
  return 1;
}

/*------------------------------------------------------------------------*/
static int McTryAndFixIt(struct __COUNTER *self, int iCode)
{
  int status;
  pMcStasController pMcStas = NULL;

  pMcStas = (pMcStasController) self->pData;

  status = McStasFix(pMcStas);
  return status;
}

/*------------------------------------------------------------------------*/
static int McSet(struct __COUNTER *self, char *name, int iCter, float FVal)
{
  return 1;
}

/*------------------------------------------------------------------------*/
static int McGet(struct __COUNTER *self, char *name, int iCter,
                 float *fVal)
{
  *fVal = 25.99;
  return 1;
}

/*-------------------------------------------------------------------------*/
static int McSend(struct __COUNTER *self, char *pText,
                  char *pReply, int iReplyLen)
{
  strlcpy(pReply, "WARNING: McStas simulation cannot be sent anything",
          iReplyLen);
  return 1;
}

/*-------------------------------------------------------------------------*/
pCounterDriver NewMcStasCounter(char *name)
{
  pMcStasController pMcStas = NULL;
  pCounterDriver pNew = NULL;

  /*
   * cannot do it without a McStas Controller
   */
  pMcStas = FindCommandData(pServ->pSics, "mccontrol", "McStasController");
  if (pMcStas == NULL) {
    return NULL;
  }

  pNew = CreateCounterDriver(name, "McStas");
  if (pNew == NULL) {
    return NULL;
  }
  pNew->iNoOfMonitors = 10;
  pNew->pData = pMcStas;

  pNew->GetStatus = McStatus;
  pNew->Start = McStart;
  pNew->Halt = McHalt;
  pNew->ReadValues = McReadValues;
  pNew->GetError = McGetError;
  pNew->TryAndFixIt = McTryAndFixIt;
  pNew->Pause = McPause;
  pNew->Continue = McContinue;
  pNew->Set = McSet;
  pNew->Get = McGet;
  pNew->Send = McSend;
  pNew->KillPrivate = NULL;

  return pNew;
}
