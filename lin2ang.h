
/*-------------------------------------------------------------------------
                          L I N 2 A N G

  Drive an angle through a translation table. As of now only for
  TOPSI.

  copyright: see copyright.h

  Mark Koennecke, February 2000
--------------------------------------------------------------------------*/
#ifndef LIN2ANG
#define LIN2ANG
#include "motor.h"

int MakeLin2Ang(SConnection * pCon, SicsInterp * pSics, void *pData,
                int argc, char *argv[]);
int Lin2AngAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                  int argc, char *argv[]);


#endif
