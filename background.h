/**
 * This is for backgrounding operations in SICS. They run in an own 
 * task.
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koennecke, February 2009
 */
#ifndef BACKGROUND_H_
#define BACKGROUND_H_

#include "SCinter.h"
#include "conman.h"

/**
 * interpreter inteface
 */
int BackgroundAction(SConnection * pCon, SicsInterp * pSics,
                     void *pData, int argc, char *argv[]);
/*
 * actual function which does the backgrounding
 */
int BackgroundCommand(SConnection * pCon, char *command);
/*
 * used only once for installing Background
*/
void InstallBackground(void);

#endif                          /*BACKGROUND_H_ */
