\subsection{Dynamic Array}
This section describes a dynamic array of arbitrary pointers. Dynamic means,
that the array gets enlarged automatically when an index outside of the
current bounds has been requested. Negative values are supported as well.

The interface to this module looks like this:

@d dynarint @{
   typedef struct __SDynar *pDynar;
/*-------------------------------------------------------------------------*/
   pDynar CreateDynar(int iStart, int iEnd, int iGrain, 
                      void (*DataFree)(void *pData));
   void   DeleteDynar(pDynar self);
/*------------------------------------------------------------------------*/
   int  DynarPut(pDynar self, int iIndex, void *pData);
   int  DynarPutCopy(pDynar self, int iIndex, void *pData, int iDataLen);
   int  DynarReplace(pDynar self, int iIndex, void *pData, int iDatLen);
   
   int DynarGet(pDynar self, int iIndex, void **pData);
   int DynarGetCopy(pDynar self, int iIndex, void *pData, int iDataLen);

@}

If not stated otherwise, all functions return 1 on success and 0 on failure.

\begin{description}
\item[CreateDynar] creates a new dynamic array. On success a pointer to a
new dynamic array is returned, else NULL. The parameters are: iStart, the
initial start index of the array, iEnd, the initial end index of the array,
 iGrain, the packet of new space to allocate when the array is enlarged
dynamically, DataFree, a function which takes a pointer to one of the data 
items stored in the dynamic array and the free's all the memory associated
 with the data item. DataFree will be used automatically by Dynar for
releasing memory when data items are replaced or the whole array is deleted.
\item[DeleteDynar] removes the dynamic arary from memory. The pointer to
self will point to rubbish afterwards and shall not be used.
\item[DynarPut] puts the pointer pData at position iIndex in the dynamic
array. pData needs to be allocated from dynamic memory, otherwise you'll
experience core dumps. A data item which was previously at the position
denoted by iIndex will be freed. 
\item[DynarPutCopy] does the same as DynarPut, but copies the data item.
iDataLen bytes of new memory will be allocated and the data from pData be
put into it. Then this will be entered into the dynamic array at position
iIndex.   
\item[DynarReplace] replaces the data at iIndex by the data pointed to by
 pData by a memcpy. If there was no data defined previously, this call has
the same effect as DynarPutCopy.
\item[DynarGet] retrieves the pointer pData at position iIndex from the dynamic
array. 
\item[DynarGetCopy] copies iDataLen bytes from the data at position iIndex
into the buffer pData. pData must be large enough to hold iDataLen bytes. 

\end{description}

Nowe, here is the definition of the internal datastructure for the 
dynamic array class.

@d dynardat @{
   typedef struct __SDynar {
                           int iStart;
                           int iEnd;
                           int iGrain;
                           void **pArray;
                           void (*DataFree)(void *pData);
                           } Dynar;
                           
                            
@}

@o sdynar.h @{
/*---------------------------------------------------------------------------
                       D Y N A M I C    A R R A Y

  This file describes the interface to a dynamic array module for pointers.
  This sort of array resizes dynamically.
 
  Mark Koennecke, September 1997

  copyright: see copyright.h

-----------------------------------------------------------------------------*/
#ifndef SICSDYNAR
#define SICSDYNAR
@<dynarint@>
#endif

@} 

@o sdynar.i @{
/*----------------------------------------------------------------------------
                       D Y N A M I C    A R R A Y

      This file defines the internal data structure used by the dynamic
      array module.
  
      More info: see sdynar.h
-----------------------------------------------------------------------------*/
@<dynardat@>
@}
