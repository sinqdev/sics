/**
 * This is the implementation of various SICS internal interfaces based on 
 * parameters in a generic controller.
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koennecke, November 2007 
 */
#ifndef GENINTER_H_
#define GENINTER_H_
/**
 * make a drivable parameter:
 * Usage:
 *    MakeGenDrivable  name par-node control-node
 */
int GenDrivableFactory(SConnection * pCon, SicsInterp * pSics,
                       void *pData, int argc, char *argv[]);

#endif                          /*GENINTER_H_ */
