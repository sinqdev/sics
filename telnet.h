
/*-------------------------------------------------------------------------
           S I C S  T E L N E T  S U P P O R T

  This file defines the telnet task function and its associated data
  structures. The body of the telnet handling code lives in the network
  reader module.

  Mark Koennecke, January 1998
---------------------------------------------------------------------------*/
#ifndef SICSTELNET
#define SICSTELNET

typedef struct __TelTask *pTelTask;
/*--------------------------------------------------------------------------*/
pTelTask CreateTelnet(SConnection * pCon);
void DeleteTelnet(void *pData);
/*---------------------------------------------------------------------------*/
int TelnetTask(void *pData);
void TelnetSignal(void *pData, int iSignal, void *pSigData);
/*--------------------------------------------------------------------------*/
void InstallTelnet(void);
void KillTelnet(void);


#endif
