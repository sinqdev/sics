/**
 * Hdbtable is a more generic implementation for a table based on a Hipadaba 
 * tree structure. This table will be used at various instances in SICS.
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koennecke, March 2009
 */
#ifndef HDBTABLE_H_
#define HDBTABLE_H_
#include "sicsobj.h"

pSICSOBJ MakeHdbTable(char *name, char *hdbclass);
int ReadTableTemplate(pSICSOBJ self, SConnection *con);
int HdbTableFactory(SConnection *pCon, SicsInterp *pSics, void *pData, int argc, char *argv[]);
int SaveHdbTable(void *data, char *name, FILE * fd);

#endif /*HDBTABLE_H_*/
