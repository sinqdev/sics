
/*---------------------------------------------------------------------
                 R S 2 3 2 C o n t r o l l e r
  
  A general object which represents a controller connected to the network
  via a terminal server. This bypasses David Maden's SerPortServer software.
  Basic facilities are provided for writinga nd reading to and from the 
  device. For more information see the rs232controller.tex file.

  copyright: see copyright.h

  Mark Koennecke, October 2001
-----------------------------------------------------------------------*/
#ifndef RS232CONTROLLER
#define RS232CONTROLLER
#include "network.h"
/*
  own error codes
*/
#define NOTCONNECTED  -2700
#define BADMEMORY     -2701
#define TIMEOUT       -2702
#define FAILEDCONNECT -2703
#define INCOMPLETE    -2704
#define BADREAD       -2705
#define BADSEND       -2706

/*----------------------- a data structure ----------------------------*/

typedef struct {
  pObjectDescriptor pDes;
  char *sendTerminator;
  char *replyTerminator;
  int timeout;
  mkChannel *pSock;
  char *pHost;
  int iPort;
  int debug;
  int registered;
} rs232, *prs232;



/*----------------------- the interface functions --------------------*/

int RS232Action(SConnection * pCon, SicsInterp * pSics,
                void *pData, int argc, char *argv[]);
int RS232Factory(SConnection * pCon, SicsInterp * pSics,
                 void *pData, int argc, char *argv[]);

void setRS232SendTerminator(prs232 self, char *term);
void setRS232ReplyTerminator(prs232 self, char *term);
void setRS232Timeout(prs232 self, int timeout);
void setRS232Debug(prs232 self, int deb);

int writeRS232(prs232 self, void *data, int dataLen);
int readRS232(prs232 self, void *data, int *dataLen);
int readRS232TillTerm(prs232 self, void *data, int *datalen);
int readRS232UntilWord(prs232 self, char *buffer, int buflen, char *word);
int availableRS232(prs232 self);
int availableNetRS232(prs232 self);
int transactRS232(prs232 self, void *send, int sendLen,
                  void *reply, int replylen);

void getRS232Error(int iCode, char *errorBuffer, int errorBufferLen);
int fixRS232Error(prs232 self, int iCode);
int getRS232Timeout(prs232 self);
int initRS232(prs232 self);
int initRS232WithFlags(prs232 self, int flags);
int initRS232Finished(prs232 self);
void closeRS232(prs232 self);
prs232 createRS232(char *host, int iPort);
void KillRS232(void *pData);


#endif
