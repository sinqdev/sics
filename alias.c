/*-----------------------------------------------------------------------

				A L I A S
       Copyright:

       Labor fuer Neutronenstreuung
       Paul Scherrer Institut
       CH-5423 Villigen-PSI

      First Version: 1998, Mark Koennecke
      updated: November 1999, Mark Koennecke


      The authors hereby grant permission to use, copy, modify, distribute,
      and license this software and its documentation for any purpose, provided
      that existing copyright notices are retained in all copies and that this
      notice is included verbatim in any distributions. No written agreement,
      license, or royalty fee is required for any of the authorized uses.
      Modifications to this software may be copyrighted by their authors
      and need not follow the licensing terms described here, provided that
      the new terms are clearly indicated on the first page of each file where
      they apply.

      IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
      FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
      ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
      DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
      POSSIBILITY OF SUCH DAMAGE.

      THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
      INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
      FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
      IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
      NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
      MODIFICATIONS.
----------------------------------------------------------------------------*/
#include <string.h>
#include <stdlib.h>
#include <tcl.h>
#include <assert.h>
#include "fortify.h"
#include "sics.h"
#include "splitter.h"
#include "alias.h"

/*
   Usage: SicsAlias object newname
*/

int SicsAlias(SConnection * pCon, SicsInterp * pSics, void *pData,
              int argc, char *argv[])
{
  CommandList *pCom = NULL;
  char pBueffel[256];
  int iRet;

  if (!SCMatchRights(pCon, usMugger)) {
    SCWrite(pCon, "ERROR: aliasing only allowed to Managers", eError);
    return 0;
  }

  if (argc < 3) {
    SCWrite(pCon, "ERROR: insufficient number of arguments to SicsAlias",
            eError);
    return 0;
  }

  argtolower(argc, argv);
  /* first parameter should be an registered SICS object */
  pCom = FindCommand(pSics, argv[1]);
  if (!pCom) {
    snprintf(pBueffel,255, "ERROR: cannot find %s, no alias created", argv[1]);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }

  /* alright: create the alias */
  iRet = AddCommand(pSics, argv[2], pCom->OFunc, NULL, pCom->pData);
  if (!iRet) {
    snprintf(pBueffel,255, "ERROR: duplicate command %s not created", argv[2]);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }
  return 1;
}

/*--------------------------------------------------------------------
  Make Alias: a command which installs a general alias into SICS.
*/

typedef struct {
  pObjectDescriptor pDes;
  char *pCommand;
} Alias, *pAlias;
/*----------------------------------------------------------------------*/
static void FreeAlias(void *pData)
{
  pAlias self = (pAlias) pData;
  if (!self)
    return;

  if (self->pDes)
    DeleteDescriptor(self->pDes);

  if (self->pCommand)
    free(self->pCommand);

  free(self);
}

/*----------------------------------------------------------------------
  In order to make alias most general alias tries to find the interfaces
  defined by the object corresponding to the first word in the command.
  Note: does not work, the object pointer with which a interface function
  will be called refers to the alias and not the proper thing: core dump!
  Therefore disabled!
*/
static void *AliasInterface(void *pData, int iID)
{
  CommandList *pCom = NULL;
  pDummy pDum = NULL;
  char *pPtr = NULL;
  pAlias self = (pAlias) pData;

  assert(self);
  pPtr = strtok(self->pCommand, " \t\n");
  pCom = FindCommand(pServ->pSics, pPtr);
  if (!pCom)
    return NULL;

  pDum = (pDummy) pCom->pData;
  if (!pDum)
    return NULL;

  return pDum->pDescriptor->GetInterface(pDum, iID);
}

/*-----------------------------------------------------------------------*/
static int AliasAction(SConnection * pCon, SicsInterp * pSics,
                       void *pData, int argc, char *argv[])
{
  pAlias self = NULL;
  int status;
  char pLine[512];
  char *pPtr;
  Tcl_DString command;

  self = (pAlias) pData;
  assert(self);

  /* 
     build command by appending the alias command and any possible
     arguments given.
   */
  Tcl_DStringInit(&command);
  Tcl_DStringAppend(&command, self->pCommand, -1);
  Tcl_DStringAppend(&command, " ", -1);
  Arg2Text(argc - 1, &argv[1], pLine, 511);
  Tcl_DStringAppend(&command, pLine, -1);

  /* execute the command on the current connection */
  status = SCInvoke(pCon, pSics, Tcl_DStringValue(&command));

  /* finish */
  Tcl_DStringFree(&command);
  return status;
}

/*-----------------------------------------------------------------------*/
int MakeAlias(SConnection * pCon, SicsInterp * pSics, void *pData,
              int argc, char *argv[])
{
  char pBueffel[512];
  int iRet;
  pAlias pNew = NULL;

  if (argc < 3) {
    SCWrite(pCon, "ERROR: insufficient number of arguments to alias",
            eError);
    return 0;
  }

  Arg2Text(argc - 2, &argv[2], pBueffel, 511);

  /* create data structure */
  pNew = (pAlias) malloc(sizeof(Alias));
  if (!pNew) {
    SCWrite(pCon, "ERROR: out of memory while creating alias", eError);
    return 0;
  }
  pNew->pDes = CreateDescriptor("Alias");
  pNew->pCommand = strdup(pBueffel);
  if (!pNew->pDes || !pNew->pCommand) {
    SCWrite(pCon, "ERROR: out of memory while creating alias", eError);
    return 0;
  }

  iRet = AddCommand(pSics, argv[1], AliasAction, FreeAlias, pNew);
  if (!iRet) {
    FreeAlias(pNew);
    SCWrite(pCon, "ERROR: duplicate object name NOT created", eError);
    return 0;
  }
  return 1;
}

/*-------------------------------------------------------------------------------*/
int LocateAliasAction(SConnection * pCon, SicsInterp * pSics,
                      void *pData, int argc, char *argv[])
{
  char *aliases = NULL;

  if (argc < 2) {
    SCWrite(pCon, "ERROR: missing argument aliasname for locating aliases",
            eError);
    return 0;
  }

  strtolower(argv[1]);
  aliases = FindAliases(pSics, argv[1]);
  if (aliases == NULL) {
    SCWrite(pCon, "NONE", eValue);
  } else {
    SCPrintf(pCon, eValue, "%s = %s", argv[1], aliases);
  }
  return 1;
}
