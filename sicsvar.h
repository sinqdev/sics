/*--------------------------------------------------------------------------
	SICS needs variables. This header defines the datastructures
	and functions necessary to handle text, int and float variables.
	
	Mark Koennecke, November 1996
	
	copyright: See Implementation file.
----------------------------------------------------------------------------*/

#ifndef SICSVARIABLE
#define SICSVARIABLE

#include "SCinter.h"
#include "conman.h"
#include "interface.h"
#include "obdes.h"

  /* Variable Types */
typedef enum { veText, veInt, veFloat } VarType;



  /* the actual variable type, all in one to get rid of inheritance */
typedef struct {
  pObjectDescriptor pDescriptor;
  VarType eType;
  int iAccessCode;
  float fVal;
  int iVal;
  int iLock;
  char *text;
  char *name;
  pICallBack pCall;
} SicsVariable;


typedef SicsVariable *pSicsVariable;

 /* Create and Kill */
pSicsVariable VarCreate(int iAccessCode, VarType eTyp, char *name);
  /*
     creates a new Variable 
   */
int VarFactory(SConnection * pCon, SicsInterp * pSics, void *pData,
               int argc, char *argv[]);
  /* 
     the factory method for creating variables
   */

int VarKill(pSicsVariable self);

  /* setting and reading */
int VarSetFloat(pSicsVariable self, float fNew, int iUserRight);
int VarSetInt(pSicsVariable self, int iNew, int iUserRight);
int VarSetText(pSicsVariable self, char *pNew, int iUserRight);

int VarGetFloat(pSicsVariable self, float *fNew);
int VarGetInt(pSicsVariable self, int *iNew);
int VarGetText(pSicsVariable self, char **pNew);

  /* Infomation and Configuration */
VarType GetVarType(pSicsVariable self);
int VarSetRights(pSicsVariable self, int iNewRights, int iYourRights);
  /* 
     sets new userights for the variable, only Muggers are allowed to do 
     this
   */

  /* the  wrapper function to make variables objects */
int VarWrapper(SConnection * pCon, SicsInterp * pInter, void *pData,
               int argc, char *argv[]);

  /* a utility function to find a variable in the Sics interpreter */
pSicsVariable FindVariable(SicsInterp * pSics, char *name);


#endif
