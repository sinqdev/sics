/*---------------------------------------------------------------------------
----------------------------------------------------------------------------*/
#ifndef ANSTO_PROTOCOL
#define ANSTO_PROTOCOL
#include <sics.h>
#include "conman.h"

static const int iNumProTags = 2;
static char *pProTags[3] = {
  "start",
  "finish",
  NULL
};

#define esStart -1
#define esFinish -2

/*---------------------- protocol defines -------------------------------*/
#define PROTSICS 0
#define PROTNORM 1
#define PROTCODE 2
#define PROTJSON 3
#define PROTACT  4
#define PROTALL  5

/*--------------------- lifecycle -------------------------------------- */
int InstallProtocol(SConnection * pCon, SicsInterp * pSics, void *pData,
                    int argc, char *argv[]);
void DeleteProtocol(void *pSelf);
void MakeProtocol(void);

/*--------------------- operations --------------------------------------*/
int ProtocolAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                   int argc, char *argv[]);
/*--------------------- implement protocol API    -----------------------*/
char *GetProtocolName(SConnection * pCon);
int GetProtocolID(SConnection * pCon);
int ProtocolGet(SConnection * pCon, void *pData, char *pProName, int len);
writeFunc GetProtocolWriteFunc(SConnection * pCon);
/*-----------------------------------------------------------------------*/
#endif
