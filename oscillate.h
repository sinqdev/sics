
/*-----------------------------------------------------------------------
  Oscillator runs a motor back and forth between its software limits.
  
  copyright: see file COPYRIGHT

  Mark Koennecke, November 2004
------------------------------------------------------------------------*/
#ifndef SICSOSCILLATOR
#define SICSOSCILLATOR
#include "motor.h"


typedef struct {
  pObjectDescriptor pDes;
  pMotor pMot;
  int oldRights;
  float upperLimit;
  float lowerLimit;
  int nextTargetFlag;
  TaskTaskID taskID;
  int stopFlag;
  SConnection *pCon;
  int errorCount;
  int debug;
  int cycles;
  int currcycle;
} Oscillator, *pOscillator;

/*---------------------------------------------------------------------*/
int MakeOscillator(SConnection * pCon, SicsInterp * pSics, void *pData,
                   int argc, char *argv[]);
int OscillatorWrapper(SConnection * pCon, SicsInterp * pSics, void *pData,
                      int argc, char *argv[]);

#endif
