/**
 * Triple axis monochromator module
 * EIGER made it necessary to abstract out the monochromator. This is 
 * the default implementation for a sane, normal TAS. This implements a 
 * drivable interface. It gets an A2 value from the tasub module and is 
 * supposed to drive and monitor all the dependent monochromator motors.
 *
 * copyright: see file COPYRIGHT
 *
 * Mark Koennecke, February 2013
 */

#ifndef __TASMONO
#define __TASMONO

pIDrivable MakeTasMono();

#endif

