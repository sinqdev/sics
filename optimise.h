
/*-------------------------------------------------------------------------
                   O P T I M I S E

  Optimise a peak with respect to several variables.

  copyright: see copyright.h

  Mark Koennecke, March 1998-1999
-----------------------------------------------------------------------------*/
#ifndef SICSOPTIMISE
#define SICSOPTIMISE

typedef struct __OptimiseStruct *pOptimise;

/*------------------- live & death -----------------------------------------*/
pOptimise CreateOptimiser(pCounter pCount);
void DeleteOptimiser(void *pData);
int MakeOptimiser(SConnection * pCon, SicsInterp * pSics,
                  void *pData, int argc, char *argv[]);
/*------------------- operation  -------------------------------------------*/
#define PEAKLOST   -1
#define MAXCYCLE   -2
#define SCANERROR  -3
#define SCANABORT  -4
#define SYSERROR   -5
#define DRIVEERROR -6
#define VARREDO    -7


void OptimiserClear(pOptimise self);
int OptimiserAdd(pOptimise self,
                 char *pVarName, float fStep, int nStep, float fPrecision);
int OptimiserSetPar(pOptimise self, char *name, float fVal);
int OptimiserGetPar(pOptimise self, char *name, float *fVal);

int OptimiserRun(pOptimise self, SConnection * pCon);

int OptimiserAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                    int argc, char *argv[]);
#endif
