\subsection{nxscript}
Nxscript is a facility for scripting the contents of SICS NeXus
files. For the actual writing bit the NXdict library is used. This
means that nxscript needs both a filename and a dictionary filename
when opening a new file. Then functions are provided for writing
values, motors, histogram memories etc. This module should serve two
purposes: 
\begin{itemize}
\item Replace the instrument specific codes for NeXus file writing.
\item Enable the user to quickly modify the contents of data files by
defining an new dictionary entry and adding another line to the
instrument script.  
\end{itemize} 

The general policy with errors while writing data files is to ignore
them and to write as much as possible. Thus this module will moan and
complain but always return success. 

The module is designed for writing one NeXus file at a time. However
it will be possible to create several instances of this class in order
to support the unlikely event that several files have to be written at
the same time. 

As this is mostly a scripting facility the only interface exposed is
the interface to the interpreter.

@o nxscript.h @{
/*-----------------------------------------------------------------------
                     N X S C R I P T

  This is a class for scripting the contents of NeXus files from
  SICS.

  copyright: see file COPYRIGHT

  Mark Koennecke, February 2003
------------------------------------------------------------------------*/
#ifndef NXSCRIPT
#define NXSCRIPT
#include "napi.h"
#include "nxdict.h"

int MakeNXScript(SConnection *pCon, SicsInterp *pSics, void *pData,
		int argc, char *argv[]);
int NXScriptAction(SConnection *pCon, SicsInterp *pSics, void *pData,
		int argc, char *argv[]);

char *makeFilename(SicsInterp *pSics, SConnection *pCon);
void changeExtension(char *filename, char *newExtension);
/*==============  a personal data structure ============================*/
typedef struct {
  pObjectDescriptor pDes;
  NXhandle fileHandle;
  NXdict dictHandle;
  int timeDivisor;
} NXScript, *pNXScript;

#endif
@}

