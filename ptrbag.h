#ifndef PTRBAG_H
#define PTRBAG_H
/*
 * This declares a generic container object.
 * The container itsef is reference counter with retain and release operations.
 * The entries are pointers
 * Entries are abstract references to objects.
 * Entries are reference counted.
 * Entries may be retained to increment the reference count.
 * Entries may be released to decrement the reference count.
 * Entries within the container are unordered.
 * Entries may be visited in (un)ordered sequence.
 * An array of (un)ordered entry pointers can be obtained.
 * A destructor function can be called when an object is no longer referenced.
 *
 * Douglas Clowes, April 2016
 */
#include <stdint.h>
#include <stdbool.h>

/*
 * Make this an opaque abstract data type
 */
/* typedef void* POINTER_T */
typedef struct _ptrbag_array_t *pPTRBAG;
typedef int (*PTRBAG_COMPARE)(const void *ptr, const void *ctx);
typedef void (*PTRBAG_VISITOR)(void *ptr, void *ctx);

/**
 * Constructor - create a new container
 * \param cp_out the returned pointer to the new container or NULL
 * \param min_size - the initial reservation
 * \param max_size - the expansion limit
 * \return zero on success, -1 on failure
 */
int ptrbag_create(pPTRBAG *cp_out, int min_size, int max_size);

/**
 * Increase the reference count for the container
 */
int ptrbag_retain(pPTRBAG cp_in);

/**
 * Release a reference to the container
 * The pointer will be set to NULL on return.
 */
int ptrbag_release(pPTRBAG *cp_io);

/**
 * Set a destructor function for the item pointed to (optional)
 * \param cp_in pointer to the container
 * \param free_func function to free/destroy/destruct a pointed-to-object
 *
 * This sets a function to be used to release an object when the reference
 * count for the item goes to zero.
 *
 * The default value is NULL meaning no function will be called. The caller
 * should manage object lifetime.
 *
 * The function can be free in which case free(pointer) will be called.
 *
 * A more elaborate destructor can be supplied to, for example, release
 * resources contained within the object.
 *
 * Shared reference counting can be implemented by the provided destructor releasing
 * the reference on the pointed-to-object.
 */
void ptrbag_set_item_destructor(pPTRBAG cp_in, void (*free_func)(void *ptr));

/**
 * get the unique_flag.
 */
bool ptrbag_get_unique(pPTRBAG pb_in);

/**
 * set the unique_flag.
 */
bool ptrbag_set_unique(pPTRBAG pb_in, bool check_unique);

/**
 * Return the count of active objects
 */
int ptrbag_count(pPTRBAG pb_in);

/**
 * Return an array of pointers to the objects
 */
void **ptrbag_array(pPTRBAG pb_in, PTRBAG_COMPARE compare);

/**
 * Return an array of pointers to the objects in the order of compare
 */
void **ptrbag_array_retain(pPTRBAG pb_in, PTRBAG_COMPARE compare);

/*
 * Release the array of pointers
 */
void ptrbag_array_release(pPTRBAG cp_in, void **array);

/**
 * Insert an item into the collection
 * \param cp_in pointer to the container
 * \param ptr_in pointer to be inserted
 * \return zero on success, -1 on failure
 */
int ptrbag_item_insert(pPTRBAG cp_in, void *ptr_in);

/**
 * Increment the reference count for an item
 * \param cp_in pointer to the container
 * \param hnd_in object to be incremented
 * \return zero on success
 */
int ptrbag_item_retain(pPTRBAG cp_in, void *ptr_in);

/**
 * Decrement the reference counter for an item
 * \param cp_in pointer to the container
 * \param ptr_io address of pointer, cleared on exit
 * \return zero on success
 */
int ptrbag_item_release(pPTRBAG cp_in, void **ptr_io);

/**
 * Test if an item is already in the collection
 */
bool ptrbag_contains(pPTRBAG pb_in, const void *ptr_in);


/**
 * Find a pointer based on the comparison function and the test value
 */
void *ptrbag_find(pPTRBAG pb_in, PTRBAG_COMPARE compare, const void *ctx);

/**
 * Visitor pattern - visit each entry with the given function in order of compare
 */
void ptrbag_visit(pPTRBAG pb_in, PTRBAG_VISITOR visitor,
    void *ctx, PTRBAG_COMPARE compare);

#endif /* PTRBAG_H */
