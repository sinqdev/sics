/*--------------------------------------------------------------------------
	Configuration file management.
	The configuration file has one entry:
	name = value 
	per line
	


	Mark Koennecke,    October 1996

	Copyright:

	Labor fuer Neutronenstreuung
	Paul Scherrer Institut
	CH-5423 Villigen-PSI


        The authors hereby grant permission to use, copy, modify, distribute,
        and license this software and its documentation for any purpose, provided
        that existing copyright notices are retained in all copies and that this
        notice is included verbatim in any distributions. No written agreement,
        license, or royalty fee is required for any of the authorized uses.
        Modifications to this software may be copyrighted by their authors
        and need not follow the licensing terms described here, provided that
        the new terms are clearly indicated on the first page of each file where
        they apply.

        IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
        FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
        ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
        DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
        POSSIBILITY OF SUCH DAMAGE.

        THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
        INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
        IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
        NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
        MODIFICATIONS.
----------------------------------------------------------------------------*/
#include "fortify.h"
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <ctype.h>
#include <sics.h>
#include "ifile.h"

/*====================== Locals ============================================*/
IPair *pSICSOptions = NULL;

static IPair *CreateNewEntry(char *name, char *val, IPair * pN)
{
  IPair *pRes = NULL;

  pRes = (IPair *) malloc(sizeof(IPair));
  if (!pRes)
    return NULL;
  memset(pRes, 0, sizeof(IPair));
  if (name)
    pRes->name = strdup(name);
  if (val)
    pRes->value = strdup(val);
  pRes->pNext = pN;
  return pRes;
}

/*--------------------------------------------------------------------------*/
static void RemoveWhiteSpace(char *pText)
{
  int i, ii, i3;
  char *pPtr;

  assert(pText);

  /* find start */
  i = 0;
  while (isspace(pText[i])) {
    i++;
  }

  /* find end */
  ii = strlen(pText);
  ii--;
  while ((isspace(pText[ii])) || (pText[ii] == '\n')) {
    ii--;
  }

  /* copy it */
  pPtr = pText;
  for (i3 = i; i3 < (ii + 1); i3++) {
    *pPtr = pText[i3];
    pPtr++;
  }
  *pPtr = '\0';
}

/*===========================================================================*/
IPair *IFReadConfigFile(FILE * fd)
{
  IPair *pList = NULL;
  char pBueffel[256];
  char pName[132];
  char pValue[132];
  char *pPos;
  int iLen;

  assert(fd);

  while (!feof(fd)) {
    fgets(pBueffel, 255, fd);
    if (feof(fd))
      continue;

    pPos = strchr(pBueffel, '=');
    if (!pPos)
      continue;
    if (pBueffel[0] == '#')
      continue;

    iLen = pPos - pBueffel;
    strncpy(pName, pBueffel, iLen);
    pName[iLen] = '\0';
    strlcpy(pValue, (pPos + 1),131);
    RemoveWhiteSpace(pName);
    RemoveWhiteSpace(pValue);
    pList = CreateNewEntry(pName, pValue, pList);
    if (!pList)
      return NULL;
  }
  return pList;
}

/*--------------------------------------------------------------------------*/
char *IFindOption(IPair * pList, char *name)
{
  IPair *pCurrent;

  if (!pList) {
    return NULL;
  }

  pCurrent = pList;
  while (pCurrent) {
    if (strcmp(name, pCurrent->name) == 0)
      return pCurrent->value;
    pCurrent = pCurrent->pNext;
  }
  return NULL;
}

/*--------------------------------------------------------------------------*/
IPair *IFAddOption(IPair * pList, char *name, char *value)
{
  return CreateNewEntry(name, value, pList);
}

/*--------------------------------------------------------------------------*/
IPair *IFSetOption(IPair * pList, char *name, char *value)
{
  IPair *pCurrent;
  if (NULL != pList) {
    pCurrent = pList;
    while ((NULL != pCurrent) && (0 != strcmp(name, pCurrent->name))) {
      pCurrent = pCurrent->pNext;
    }
    if (NULL != pCurrent) {     /* replace value */
      free(pCurrent->value);
      pCurrent->value = strdup(value);
      return pCurrent;
    }
  }
  return CreateNewEntry(name, value, pList);
}

/*-------------------------------------------------------------------------*/

int IFSaveOptions(IPair * pList, FILE * fd)
{
  IPair *pCurrent;

  assert(fd);
  assert(pList);

  pCurrent = pList;
  while (pCurrent) {
    fprintf(fd, "%s = %s \n", pCurrent->name, pCurrent->value);
    pCurrent = pCurrent->pNext;
  }
  return 1;
}

/*--------------------------------------------------------------------------*/

void IFDeleteOptions(IPair * pList)
{
  IPair *pCurrent, *pTemp;

  pCurrent = pList;
  while (pCurrent) {
    pTemp = pCurrent->pNext;
    if (pCurrent->name) {
      free(pCurrent->name);
    }
    if (pCurrent->value) {
      free(pCurrent->value);
    }
    free(pCurrent);
    pCurrent = pTemp;
  }
}

/*----------------------- Server options creation -------------------------*/
int IFServerOption(SConnection * pCon, SicsInterp * pSics,
                   void *pData, int argc, char *argv[])
{
  char pBueffel[512];

  assert(pSics);
  assert(pCon);

  /* check authorisation */
  if (!SCMatchRights(pCon, usMugger)) {
    SCWrite(pCon, "Insufficient privilege to set options", eError);
    return 0;
  }

  /* test if sufficient arguments */
  if (argc < 3) {
    snprintf(pBueffel,sizeof(pBueffel)-1, "Syntax: %s name value ", argv[0]);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }

  /* just do it */
  pSICSOptions = IFAddOption(pSICSOptions, argv[1], argv[2]);
  return 1;
}

/*===========================================================================
	Testcode: Define MAIN to activate it. Needs a file test.txt
	as input
*/

#ifdef MAIN
int main(int argc, char *argv[])
{
  FILE *fd;
  IPair *pList = NULL;
  char *pPos = NULL;

  fd = fopen("test.txt", "r");
  if (!fd)
    exit(2);


  pList = IFReadConfigFile(fd);
  if (!pList)
    exit(2);
  fclose(fd);

  pPos = IFindOption(pList, "Gabi");
  if (pPos)
    puts(pPos);

  fd = fopen("lala.txt", "w");
  IFSaveOptions(pList, fd);
  IFDeleteOptions(pList);

}
#endif
