/*--------------------------------------------------------------------------
			M U L T I P L E  M O T O R S
			
  multiple motors implements many of the things required for SANS.
  It is an abstraction of a whole group of motors which supports:
  - mapping of real motors to specific names.
  - named collective positions.
  - a position history implemented as back.

  Mark Koennecke, December 1996
  
  copyright: see implementation file

----------------------------------------------------------------------------*/
#ifndef SICSMUMO
#define SICSMUMO
/*-------------------------------------------------------------------------*/
typedef struct __MULMOT *pMulMot;

pMulMot MakeMultiMotor(void);
void KillMultiMotor(void *pData);

const char *FindNamPos(pMulMot self, SConnection * pCon);

int MultiWrapper(SConnection * pCon, SicsInterp * pSics, void *pData,
                 int argc, char *argv[]);

int MakeMulti(SConnection * pCon, SicsInterp * pSics, void *pData,
              int argc, char *argv[]);

int ConfigMulti(SConnection * pCon, SicsInterp * pSics, void *pData,
                int argc, char *argv[]);


#endif
