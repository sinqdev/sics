/**
 * This is the simple reflection indexer. The algorithm is simple:
 * - Three non coplanar reflections at low two theta are selected.
 * - Candidate indices are calculated from the lattice constants 
 * - Permutations of the generated indices are changed for a couple 
 *   of conditions:
 * -- Does the angle between the reflections matches the expectations
 * -- Do the reflections form a right handed set
 * UB matrics matching these conditions are calculated and stored 
 * for later retrieval.
 *
 * The software is organized such that this is a standalone module.
 *
 * Mark Koennecke, August 2008
 */
#ifndef __SIMIDX
#define __SIMIDX
#include <stdio.h>
#include "sginfo.h"

typedef struct {
  int h[3], k[3], l[3];
  int originalID[3];
  double diff;
} IndexSolution;

/**
 * \brief To be called before using simidx
 */
void SimIdxInit();

/**
 * \brief Set the cell constants
 * \param cell The cell constants: a,b,c alpha, beta, gamma
 */
void SimIdxSetCell(double cell[6]);
/**
 * \brief set the wavelength
 * \param lambda The wavelength
 */
void SimIdxSetLambda(double lambda);
/**
 * \brief set the two-theta limit for considering a 
 * valid set of indices
 * \param limit  The two theta  limit
 */
void SimIdxSetSttLim(double limit);
/**
 * \brief set the allowd angular dieviation for 
 * a two combination.
  * \param limit  The angle limit  limit
 */
void SimIdxSetAngLim(double limit);
/**
 * \brief set the spacegroup
 * \param spgrp The new spacegroup
 */
void SimIdxSetSpacegroup(T_SgInfo * spgrp);
/**
 * \brief clear the reflection list
 * */
void SimIdxClearReflection();
/**
 *\brief add a reflection already reduced to a scattering vector
 * \param uvw The coordinates of the scattering vector
 */
void SimIdxAddReflection(double uvw[3]);
typedef void (*OutFunc) (void *userData, char *text);
/**
 * \brief configure the output for the algorithm
 * \param userData A pointer to user data which is passed to the 
 * output function unchanged
 * \param out An output function
 * \param level the vebosity level for output
 */
void SimIdxOutput(void *userData, OutFunc out, int level);

/**
 * \brief Run the algorithm
 * \return 1 on success, 0 on error
 */
int SimIdxRun();
/**
 * \brief get the last error description
 * \param errCode pointer to an integer error code
 * \param error A text buffer to hold a description of the error
 * \param errLen The length of error
 */
void SimIdxGetError(int *errCode, char *error, int errLen);

/**
  * \brief get the  number of solutions
  * \return The number of solutions
  */
int SimIdxGetNSolutions();
/**
 * get the  solution number idx
 * \param id The id of the solution
 * \return A IndexSolution structure
 */
IndexSolution SimIdxGetSolution(int id);
/**
 * Remove the solution with the id given. This allows 
 * for solution filtering though upper level code. Please 
 * note that this changes the number of available solutions
 * @param id The solution to remove
 */
void SimIdxRemoveSolution(int id);

#endif
