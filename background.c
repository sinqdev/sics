/**
 * This is for backgrounding operations in SICS. They run in an own 
 * task. 
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koennecke, February 2009
 */
#include <sics.h>
#include "splitter.h"
#include "background.h"

/*---------------------------------------------------------------------------*/
typedef struct {
  SConnection *con;
  char *command;
} BckTask, *pBckTask;
/*---------------------------------------------------------------------------*/
static void KillBckTask(void *data)
{
  pBckTask self = (pBckTask) data;
  if (self == NULL) {
    return;
  }
  if (self->con) {
    SCDeleteConnection(self->con);
  }
  if (self->command) {
    free(self->command);
  }
  free(self);
}

/*---------------------------------------------------------------------------*/
static int BackgroundTask(void *data)
{
  pBckTask self = (pBckTask) data;

  assert(self != NULL);

  InterpExecute(pServ->pSics, self->con, self->command);
  return 0;
}

/*----------------------------------------------------------------------------*/
int BackgroundCommand(SConnection * pCon, char *command)
{
  TaskTaskID lID;
  pBckTask self = NULL;

  self = calloc(1, sizeof(BckTask));
  if (self == NULL) {
    return 0;
  }

  self->con = SCCopyConnection(pCon);
  self->command = strdup(command);
  if (self->con == NULL || self->command == NULL) {
    free(self);
    return 0;
  }

  lID = TaskRegisterN(pServ->pTasker, self->command, BackgroundTask, NULL, KillBckTask, self, TASK_PRIO_LOW);
  SCPrintf(pCon, eValue, "bg = %ld", lID);
  return 1;
}

/*------------------------------------------------------------------------*/
int BackgroundAction(SConnection * pCon, SicsInterp * pSics,
                     void *pData, int argc, char *argv[])
{
  int status;
  char command[1024];

  memset(command, 0, 1024);
  Arg2Text(argc - 1, &argv[1], command, 1024);
  status = BackgroundCommand(pCon, command);
  if (status == 0) {
    SCWrite(pCon, "ERROR: out of memory starting task", eError);
    return 0;
  }
  return 1;
}

/*--------------------------------------------------------------------------*/
void InstallBackground(void)
{
  AddCmd("bg", BackgroundAction);
}
