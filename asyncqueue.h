/*
 * A S Y N C Q U E U E
 *
 * This module manages communications on an asynchronous connection.
 *
 * Douglas Clowes, May 2007
 *
 */
#ifndef SICSASYNCQUEUE
#define SICSASYNCQUEUE

/**
 * @file
 * @brief Asynchronous connection handling and multiplexing
 */

#include "asyncprotocol.h"
#include "sics.h"

typedef struct __AsyncQueue AsyncQueue, *pAsyncQueue;


/** \brief create an AsyncUnit attached to a named AsyncQueue.
 *
 * \param queueName the name of the AsyncQueue to be used
 * \param unit pointer to the AsyncUnit created on positive return
 * \return positive if successful
 */
int AsyncUnitCreate(const char *queueName, pAsyncUnit * unit);
/** \brief Get an AsyncUnit from a given AsyncQueue
 * \param queue The AsyncQueue for which this AsyncUnit is valid
 * \return a new AsyncUnit or NULL on error
 */
pAsyncUnit AsyncUnitFromQueue(pAsyncQueue queue);

/** \brief create an AsyncUnit attached to an anonymous AsyncQueue.
 *
 * \param host name or address of the target host
 * \param port number or service name on the target host
 * \param unit pointer to the AsyncUnit created on positive return
 * \return positive if successful
 *
 * If <port> is null then <host> points to an existing queue name.
 * If a queue exists for <host>/<port>, then that queue is used.
 * If neither of the above hold, a new queue is created.
 */
int AsyncUnitCreateHost(const char *host,
                        const char *port, pAsyncUnit * unit);

/** \brief destroys an AsyncUnit
 *
 * \param unit pointer to the AsyncUnit to be destroyed
 */
int AsyncUnitDestroy(pAsyncUnit unit);

/** \brief Queue a transaction at the head of the associated AsyncQueue
 *
 * \param unit AsyncUnit
 * \param pTxn pointer to transaction
 */
int AsyncUnitEnqueueHead(pAsyncUnit unit, pAsyncTxn pTxn);

/** \brief Queue a transaction at the tail of the associated AsyncQueue
 *
 * \param unit AsyncUnit
 * \param pTxn pointer to transaction
 */
int AsyncUnitEnqueueTxn(pAsyncUnit unit, pAsyncTxn pTxn);

/** \brief prepare a transaction according to the protocol (default is CRLF)
 *
 * \param unit AsyncUnit
 * \param command text string to be sent
 * \param cmd_len length of data in command
 * \param responseHandler function to handle the response
 * \param context to be used by handler function
 * \param resp_len maximum length to be allowed for response
 */
pAsyncTxn AsyncUnitPrepareTxn(pAsyncUnit unit,
                              const char *command, int cmd_len,
                              AsyncTxnHandler responseHandler,
                              void *context, int rsp_len);

/** \brief clone a pointer to the transaction - must use AsyncUnitFreeTxn
 * \param txn the transaction to clone
 */
pAsyncTxn AsyncUnitHoldTxn(pAsyncTxn txn);

/** \brief free a cloned transaction
 * \param txn the transaction to free (returned by AsyncUnitHoldTxn)
 */
void AsyncUnitFreeTxn(pAsyncTxn txn);

/** \brief prepare and queue a transaction
 *
 * \param unit AsyncUnit
 * \param command text string to be sent
 * \param cmd_len length of data in command
 * \param responseHandler function to handle the response
 * \param context to be used by handler function
 * \param resp_len maximum length to be allowed for response
 */
int AsyncUnitSendTxn(pAsyncUnit unit,
                     const char *command, int cmd_len,
                     AsyncTxnHandler responseHandler, void *context,
                     int rsp_len);

/** \brief send a transaction and wait for the response
 *
 * \param unit AsyncUnit
 * \param command text string to be sent
 * \param cmd_len length of data in command
 * \param responseHandler function to handle the response
 * \param context to be used by handler function
 * \param resp_len [in] maximum length to be allowed for response
 *                 [out] actual length returned
 */
int AsyncUnitTransact(pAsyncUnit unit,
                      const char *command, int cmd_len,
                      char *response, int *rsp_len);

/** \brief write to the AsyncQueue file descriptor
 *
 * The data is transmitted directly to the descriptor without being queued.
 * This may be used by the protocol transmit function or for retrieving error
 * text associated with the current transmission.
 *
 * \param unit AsyncUnit
 * \param data to be transmitted
 * \param buflen lenght of data
 */
int AsyncUnitWrite(pAsyncUnit unit, void *buffer, int buflen);

/** \brief registers a notification callback
 *
 * The notification callback may notify unsolicited or unusual events
 *
 * \param unit AsyncUnit
 * \param context passed in callback
 * \param notify function to be called
 */
typedef void (*AQU_Notify) (void *context, int event);
void AsyncUnitSetNotify(pAsyncUnit unit, void *context, AQU_Notify notify);

/** \brief get the intertransaction delay in milliseconds
 */
int AsyncUnitGetDelay(pAsyncUnit unit);

/** \brief set the intertransaction delay in milliseconds
 */
void AsyncUnitSetDelay(pAsyncUnit unit, int iDelay);

/** \brief get the default transaction timeout in milliseconds
 */
int AsyncUnitGetTimeout(pAsyncUnit unit);

/** \brief set the default transaction timeout in milliseconds
 */
void AsyncUnitSetTimeout(pAsyncUnit unit, int timeout);

/** \brief get the number of retries
 */
int AsyncUnitGetRetries(pAsyncUnit unit);

/** \brief set the number of retries
 */
void AsyncUnitSetRetries(pAsyncUnit unit, int retries);

/** \brief get the associated protocol handler
 */
pAsyncProtocol AsyncUnitGetProtocol(pAsyncUnit unit);

/** \brief set the associated protocol handler
 */
void AsyncUnitSetProtocol(pAsyncUnit unit, pAsyncProtocol protocol);

/** \brief retrieves the socket/channel associated with the AsyncQueue
 *
 * \param unit AsyncUnit
 * \return channel or NULL
 */
mkChannel *AsyncUnitGetSocket(pAsyncUnit unit);

/** \brief attempt to reconnect the socket of the associated AsyncQueue
 *
 * \param unit pointer to AsyncUnit
 */
int AsyncUnitReconnect(pAsyncUnit handle);

/** \brief create an AsyncQueue from the SICS command MakeAsyncQueue
 */
int AsyncQueueFactory(SConnection * pCon, SicsInterp * pSics,
                      void *pData, int argc, char *argv[]);

/** \brief SICS command handler for the AsyncQueue object
 */
int AsyncQueueAction(SConnection * pCon, SicsInterp * pSics,
                     void *pData, int argc, char *argv[]);

/** \brief store queue level context
 */
void *AsyncUnitSetQueueContext(pAsyncUnit handle, void *cntx);

/** \brief retrieve queue level context
 */
void *AsyncUnitGetQueueContext(pAsyncUnit handle);

/** \brief return one if queue is connected, zero otherwise
 */
int AsyncUnitIsQueueConnected(pAsyncUnit handle);

#endif                          /* SICSASYNCQUEUE */
