/*---------------------------------------------------------------------------
initializer.h

initializer list routines

Markus Zolliker, March 2005
----------------------------------------------------------------------------
*/

#ifndef SICSINIT_H
#define SICSINIT_H

typedef void (*Initializer) (void);

void MakeInitializer(const char *type, const char *name, Initializer maker,
                     int startupOnly, const char *desc);
/*
   install an initializer

   type: the type of the initializer
   name: the name of the initalizer
   maker: an initalizer function. The prototype does normally not match,
          a cast (Initializer) is needed on the call. But: all initializers
          with the same type must have the same prototype.
   startupOnly: put 1, if the initializer should only be available during
                startup
*/

Initializer GetInitializer(const char *type, const char *name);
/*
    Get back an initializer with given type and name.
    The result must be casted to the proper prototype for the given
    type. 
*/

/*
static int MakeObject(SConnection *con, SicsInterp *sics,
                   void *data, int argc, char *argv[]);

   MakeObject has the following syntax:
   
     MakeObject objectName driver [ args ... ]
  
   It executes the initializer with the type "Object" and and the
   driver given as name. The found initalizer should use the given arguments
   to create a driver. Objects should be dynamic, i.e. there should be
   a creation command in the status file, except when the creation command
   was MakeStaticObject instead of MakeObject.
   
*/

typedef int (*CmdInitializer) (SConnection * pCon, int argc, char *argv[],
                               int dynamic);
/*
   The initalizer prototype for the type "Object".
   - pCon: the sics connection calling this initializer
   - argc: the total number of args
   - argv: the args (argv[0]: "MakeObject", argv[1]: object name, argv[3]: driver ...)
   - dynamic: 1, if the creation command was MakeObject,
              0, if the creation command was MakeStaticObject
*/

void MakeDriver(const char *driver, CmdInitializer maker, int startupOnly,
                const char *desc);
/*    
   Install a driver of type "Object" with the initializer function maker.
   - startupOnly: the driver creation should only be possible at startup
*/

#endif
