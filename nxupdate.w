\subsection{NeXus File Update}
When performing long term measurements it is advisable to save the
current state of the measurement at regular time intervalls in order
to protect against loss of data caused by computer or software
failures. This is a general object which takes care of it. This is
meant to work closely with the nxscript module for scripting the
content of NeXus files but is not restricted to it. In general, the
user has to specify the names of three scripts or SICS commands which
implement the following functionality:
\begin{description}
\item[startScript] This script is supposed to write all constant data
and is called once when counting starts. 
\item[updateScript] is called whenever an update is called for. This
script is supposed to create and update those datasets which vary in
the course of the measurement.
\item[linkScript]  This script is called once when the measurement
starts after startScript and updateScript. Then the NeXus file is
complete enough to create all the links required. This has to be done
by this script. 
\end{description}
The NeXus file is automatically updated when counting finishes.


The data structure for holding the information required by this
object:
@d nxupdate @{
	typedef struct __NXUPDATE {
	pObjectDescriptor pDes;
	char *startScript;
	char *updateScript;
	char *linkScript;
	int updateIntervall;
	time_t nextUpdate;
	int iEnd;
	SConnection *pCon;
	int onOff;
}NXupdate, *pNXupdate;
@}
The fields:
\begin{description}
\item[pDes] The standard object descriptor.
\item[startScript] The name of the program to call for starting a
file.
\item[updateScript] The name of the program to use for updating a
file.
\item[linkScript] The name of the program to use for creating links in
the NeXus file. 
\item[updateIntervall] The time in seconds between updates.
\item[nextUpdate] The time value for the next scheduled update. 
\item[iEnd] A flag which becomes 1 when counting ends and makes the
UpdateTask to stop. 
\item[pCon] The connection for which we are updating.
\item[onOff] A switch to switch automatic updates on or off.
\end{description}

This object has no public C interface. There is only ainterpreter
function which allows to configure the object and a factory function
for creating an object of this type.

@o nxupdate.h @{
/*-----------------------------------------------------------------------
  Automatic update of NeXus files a scheduled time intervalls. 
  For more information see nxudpate.tex.

  copyright: see file COPYRIGHT

  Mark Koennecke, December 2003
----------------------------------------------------------------------*/
#ifndef NXUPDATE
#define NXUPDATE

 int UpdateAction(SConnection *pCon, SicsInterp *pSics, void *pData,
	int argc, char *argv[]);
 int UpdateFactory(SConnection *pCon, SicsInterp *pSics, void *pData,
	int argc, char *argv[]);

 void KillUpdate(void *pData);
#endif

@}

@o nxupdate.i @{
/*-----------------------------------------------------------------------
  Automatic update of NeXus files a scheduled time intervalls. 
  For more information see nxudpate.tex.

  Internal, automatically generated file, do not modify. Modify in 
  nxudpate.w

  copyright: see file COPYRIGHT

  Mark Koennecke, December 2003
----------------------------------------------------------------------*/
@<nxupdate@>

@}