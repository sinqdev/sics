
#line 295 "velo.w"

/*--------------------------------------------------------------------------
        V E L O C I T Y   S E L E C T O R  D R I V E R

  Header file for the velocity selector driver and its related functions.

  Mark Koennecke, Juli 1997
  copyright: see implementation file
-----------------------------------------------------------------------------*/
#ifndef VELODRIV
#define VELODRIV
#include <tcl.h>

#line 185 "velo.w"

typedef struct __VelSelDriv {
  void *pPrivate;
  void (*DeletePrivate) (void *pData);
  float fTolerance;
  int (*Halt) (pVelSelDriv self);
  int (*GetError) (pVelSelDriv self,
                   int *iCode, char *pError, int iErrlen);
  int (*TryAndFixIt) (pVelSelDriv self, int iCode);
  int (*GetRotation) (pVelSelDriv self, float *fRot);
  int (*SetRotation) (pVelSelDriv self, float fRot);
  int (*GetStatus) (pVelSelDriv self, int *iCall, float *fCur);
  int (*GetDriverText) (pVelSelDriv self, char *pText, int iTextLen);
  int (*GetLossCurrent) (pVelSelDriv self, float *fLoss);
  int (*Init) (pVelSelDriv self, SConnection * pCon);
} VelSelDriv;

#line 307 "velo.w"

/*-------------------- live & death ----------------------------------------*/
pVelSelDriv VSCreateSim(void);
pVelSelDriv VSCreateDornierSINQ(char *name, void * pTcl);

void VSDeleteDriver(pVelSelDriv self);

#endif
