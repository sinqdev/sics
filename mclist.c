#ifndef MC_List_TYPE
#define MC_List_TYPE MC_NAME(List)
#define MC_First_FUN MC_NAME(First)
#define MC_This_FUN MC_NAME(This)
#define MC_Next_FUN MC_NAME(Next)
#define MC_End_FUN MC_NAME(End)
#define MC_Insert_FUN MC_NAME(Insert)
#define MC_Add_FUN MC_NAME(Add)
#define MC_Take_FUN MC_NAME(Take)
#endif

#ifdef MC_IMPLEMENTATION

#undef MC_IMPLEMENTATION
#ifndef MC_TYPE
#define MC_TYPE MC_NAME()*
#endif

#else

#define MC_DO_NOT_UNDEF
#include "mclist.h"
#undef MC_DO_NOT_UNDEF

#endif

#ifndef MC_NEXT
#define MC_NEXT next
#endif

MC_TYPE MC_First_FUN(MC_List_TYPE * list)
{
  list->ptr = &list->head;
  return list->head;
}

MC_TYPE MC_This_FUN(MC_List_TYPE * list)
{
  if (list->head == NULL) {
    list->ptr = &list->head;
    return NULL;
  }
  return *list->ptr;
}

MC_TYPE MC_Next_FUN(MC_List_TYPE * list)
{
  MC_TYPE node;
  if (list->head == NULL) {
    list->ptr = &list->head;
    return NULL;
  }
  node = *list->ptr;
  if (node) {
    list->ptr = &node->MC_NEXT;
  }
  return *list->ptr;
}

void MC_End_FUN(MC_List_TYPE * list)
{
  MC_TYPE node;
  if (list->head == NULL) {
    list->ptr = &list->head;
  }
  node = *list->ptr;
  if (node) {
    while (node->MC_NEXT != NULL) {
      node = node->MC_NEXT;
    }
    list->ptr = &node->MC_NEXT;
  }
}

void MC_Insert_FUN(MC_List_TYPE * list, MC_TYPE node)
{
  if (list->head == NULL) {
    list->ptr = &list->head;
  }
  node->MC_NEXT = *list->ptr;
  *list->ptr = node;
}

void MC_Add_FUN(MC_List_TYPE * list, MC_TYPE node)
{
  node->MC_NEXT = NULL;
  if (list->head == NULL) {
    list->head = node;
    list->ptr = &list->head;
  } else {
    if (*list->ptr != NULL) {
      MC_End_FUN(list);
    }
    *list->ptr = node;
  }
}

MC_TYPE MC_Take_FUN(MC_List_TYPE * list)
{
  MC_TYPE node;
  node = *list->ptr;
  if (node != NULL) {
    *list->ptr = node->MC_NEXT;
  }
  return node;
}

void MC_Delete_FUN(MC_List_TYPE * list, void (*deleteFunc) (MC_TYPE n))
{
  MC_TYPE node;
  MC_TYPE victim;
  node = list->head;
  while (node != NULL) {
    victim = node;
    node = node->next;
    deleteFunc(victim);
  }
  list->head = NULL;
  list->ptr = &list->head;
}

#undef MC_NAME
#undef MC_TYPE
#undef MC_NEXT
