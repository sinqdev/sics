
/*-----------------------------------------------------------------------
  Automatic update of NeXus files a scheduled time intervalls. 
  For more information see nxudpate.tex.

  copyright: see file COPYRIGHT

  Mark Koennecke, December 2003
----------------------------------------------------------------------*/
#ifndef NXUPDATE
#define NXUPDATE

int UpdateAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                 int argc, char *argv[]);
int UpdateFactory(SConnection * pCon, SicsInterp * pSics, void *pData,
                  int argc, char *argv[]);

void KillUpdate(void *pData);
#endif
