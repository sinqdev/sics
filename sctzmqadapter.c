/*
 * Script Context to ZeroMQ Socket Adapter
 *
 * Douglas Clowes (dcl@ansto.gov.au) March 2018
 *
 * The purpose of this module is to provide the interface glue to allow
 * Script Context drivers to use a ZeroMQ request socket.
 *
 * The approach taken is to provide an Ascon device interface and state machine
 * on top of an ZeroMQ socket.
 */
#include "ascon.h"
#include "ascon.i"
#include "dynstring.h"
#include "splitter.h"
#include "logv2.h"
#include "sicszmq.h"
#include "nwatch.h"
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>

struct private_data {
  int readDone;
  int cmd_len;
  HANDLE_T zmqHandle;
  pNWContext nHandle;
  double read_start;
  double time_out;
};

static char *state_name(Ascon *a)
{
  switch (a->state) {
  case AsconNotConnected:
    return "AsconNotConnected";
    break;
  case AsconConnectStart:
    return "AsconConnectStart";
    break;
  case AsconConnecting:
    return "AsconConnecting";
    break;
  case AsconConnectDone:
    return "AsconConnectDone";
    break;
  case AsconWriteStart:
    return "AsconWriteStart";
    break;
  case AsconWriting:
    return "AsconWriting";
    break;
  case AsconWriteDone:
    return "AsconWriteDone";
    break;
  case AsconReadStart:
    return "AsconReadStart";
    break;
  case AsconReading:
    return "AsconReading";
    break;
  case AsconReadDone:
    return "AsconReadDone";
    break;
  case AsconIdle:
    return "AsconIdle";
    break;
  case AsconFailed:
    return "AsconFailed";
    break;
  case AsconTimeout:
    return "AsconTimeout";
    break;
  case AsconMaxState:
    return "AsconMaxState";
    break;
  default:
    return "Unknown Ascon State";
    break;
  }
}

static int sczmqProtHandler(Ascon *a)
{
  int rval;
  pid_t child;
  char buff[100];
  struct private_data *pp = (struct private_data *) a->private;
  switch (a->state) {
  case AsconNotConnected:
    return 0;
  case AsconConnectStart:
    a->state = AsconConnecting;
    return 0;
  case AsconConnecting:
    a->state = AsconConnectDone;
    return 0;
  case AsconConnectDone:
    /* should not get here */
    a->state = AsconIdle;
    return 0;
  case AsconWriteStart:
    a->state = AsconWriting;
    pp->readDone = 0;
    return 0;
  case AsconWriting:

    pp->cmd_len = GetDynStringLength(a->wrBuffer);
    if (pp->cmd_len > 0) {
      sics_zmq_dealer_send(pp->zmqHandle, DynStringGetArray(a->wrBuffer));
    }
    a->state = AsconWriteDone;
    return 0;
  case AsconWriteDone:
    /* should not get here */
    a->state = AsconReadStart;
    return 0;
  case AsconReadStart:
    DynStringClear(a->rdBuffer);
    pp->read_start = DoubleTime();
    a->state = AsconReading;
    return 0;
  case AsconReading:
    if (!pp->readDone) {
      if (pp->time_out > 0.0
          && (DoubleTime() - pp->read_start) > pp->time_out) {
        if (GetDynStringLength(a->rdBuffer) == 0) {
          a->state = AsconTimeout;
        } else {
          a->state = AsconReadDone;
        }
      }
      return 0;
    }
    a->state = AsconReadDone;
    return 0;
  case AsconReadDone:
    /* should not get here */
    return 0;
  case AsconIdle:
    return 0;
  case AsconFailed:
    return 0;
  case AsconTimeout:
    /* should not get here */
    a->state = AsconIdle;
    return 0;
  case AsconMaxState:
    return 0;
  default:
    return 0;
  }
}

/*
 * Kill the private storage
 *
 * Clean up and release all resources associated with the private object
 */
static void SCAQ_KillPrivate(void *vp)
{
  struct private_data *pp = (struct private_data *) vp;
  if (pp) {
    free(pp);
  }
}

int dealer_dispatch(void *context, int mode)
{
  Ascon *a = (Ascon *) context;
  struct private_data *pp = a->private;
  pDEALER_MESSAGE pMsg;
  while ((pMsg = sics_zmq_dealer_recv(pp->zmqHandle, 20))) {
    int len = sics_zmq_dealer_size(pMsg);
    DynStringConcatBytes(a->rdBuffer, sics_zmq_dealer_data(pMsg), len);
    sics_zmq_dealer_free(pMsg);
    pp->readDone = 1;
  }
  return 0;
}
/*
 * Initialize the Ascon object for this device, the async queue argument.
 */
static int sczmqAsconInit(Ascon *a, SConnection *pCon, int argc,
                          char *argv[])
{
  int i;
  struct private_data *pp;
  for (i = 0; i < argc; ++i) {
    SCPrintf(pCon, eStatus, "sczmqAsconInit: arg[%d] = %s\n", i, argv[i]);
  }
  if (argc < 1) {
    SCPrintf(pCon, eError,
             "Insufficient arguments to sczmqAsconInit: %d\n", argc);
    return 0;
  }
  a->hostport = strdup(argv[1]);
  a->killPrivate = SCAQ_KillPrivate;
  pp = (struct private_data *) calloc(sizeof(struct private_data), 1);
  a->private = pp;
  if (pp == NULL) {
    SCPrintf(pCon, eError,
             "Out of Memory in sczmqAsconInit: %d\n", argc);
    return 0;
  }
  pp->time_out = 60.0;
  pp->zmqHandle = sics_zmq_dealer(argv[1]);
  if (pp->zmqHandle) {
    if (!NetWatchRegisterZMQCallback(&pp->nHandle,
                                     sics_zmq_dealer_zsock(pp->zmqHandle),
                                     dealer_dispatch, a)) {
      SCPrintf(pCon, eLog, "zdealer netwatch fail:%s", argv[1]);
      sics_zmq_undealer(&pp->zmqHandle);
      return 0;
    }
    SCPrintf(pCon, eLog, "zdealer established to:%s", argv[1]);
  } else {
    SCPrintf(pCon, eLog, "zdealer establish fail:%s", argv[1]);
    return 0;
  }
  return 1;
}

/*
 * This procedure creates, initializes and registers the "zmqdealer" protocol
 * with the Ascon infrastructure
 */
void AddZMQDProtocol(void)
{
  AsconProtocol *prot = NULL;
  printf("AddZMQDProtocol\n");
  prot = calloc(sizeof(AsconProtocol), 1);
  prot->name = strdup("zmqdealer");
  prot->init = sczmqAsconInit;
  prot->handler = sczmqProtHandler;
  AsconInsertProtocol(prot);
}

