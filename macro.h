/*---------------------------------------------------------------------------
      SICS is going to have a macro facility. I.E. it will be able to
      evaluate command sequences specified in a file or from the command
      line. John Ousterhouts Tcl will be used as the Macro language.
      The SICS-objects will be accessed by redefining Tcl's "unknown"
      mechanism. For documentation on Tcl and the programmers API to
      Tcl see John Ousterhout's Book. No need to duplicate this here.
      
      Mark Koennecke, November 1996
      
      copyright: see implementation file.
      
---------------------------------------------------------------------------*/
#ifndef SICSMACRO
#define SICSMACRO
#include <tcl.h>
#include "sics.h"
#include "SCinter.h"

void *MacroInit(SicsInterp * pInter);
void MacroDelete(void * pInter);
void WhereKill(void *pData);

int MacroEval(SConnection * pCon, SicsInterp * pSics, void *pData,
              int argc, char *argv[]);
int MacroFileEval(SConnection * pCon, SicsInterp * pInter, void *pData,
                  int argc, char *argv[]);
int InternalFileEval(SConnection * pCon, SicsInterp * pInter, void *pData,
                     int argc, char *argv[]);
int MacroWhere(SConnection * pCon, SicsInterp * pInter, void *pData,
               int argc, char *argv[]);
int ClientPut(SConnection * pCon, SicsInterp * pInter, void *pData,
              int argc, char *argv[]);
int GumPut(SConnection * pCon, SicsInterp * pInter, void *pData,
           int argc, char *argv[]);
int Broadcast(SConnection * pCon, SicsInterp * pInter, void *pData,
              int argc, char *argv[]);
int TransactAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                   int argc, char *argv[]);
int CaptureAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                   int argc, char *argv[]);

int TclPublish(SConnection * pCon, SicsInterp * pInter, void *pData,
               int argc, char *argv[]);
int AllowExec(SConnection * pCon, SicsInterp * pInter, void *pData,
              int argc, char *argv[]);

#define MACRO_OK 1
#define MACRO_ERROR 0
int MacroEvalString(SConnection *pCon,
                    SicsInterp *pInter,
                    const char *command);

int MacroEvalArgs(SConnection *pCon,
                    SicsInterp *pInter,
                    int argc,
                    char *argv[]);

const char *MacroGetResult(SConnection *pCon,
                    SicsInterp *pInter);

void MacroSetResult(SConnection *pCon,
                    SicsInterp *pInter,
                    char *result);
   /* for maintaining the Connection stack at Unknown. Should only be used
      by SicsInterp module
    */
int MacroPush(SConnection * pCon);
int MacroPop(void);
SConnection *MacroPeek(void);

void KillSicsUnknown(void);

#endif
