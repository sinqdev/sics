
/*-----------------------------------------------------------------------
        S i t e  A b s t r a c t  i o n  L a y e r

With ANSTO using SICS as well it became necessary to separate the
general parts  of SICS from the installation specific components. Each
installation will have a separate set of drivers and, to some
extent,  instrument specific commands. Such code has to be in a
separate library. Access to this library is through an interface which
consists of a structure containing pointers to functions which allow
for the creation of site specific drivers and commands. Moreover, the
site specific library has to implement a function,  getSite, which
returns the appropriate data structure for the site for which SICS is
being compiled. 
------------------------------------------------------------------------*/
#ifndef SICSSITE
#define SICSSITE
#include "sics.h"
#include "motor.h"
#include "countdriv.h"
#include "HistDriv.i"
#include "stringdict.h"
#include "velo.h"
#include "codri.h"
#include "evcontroller.h"
#include "scan.h"

typedef struct {
  void (*AddSiteCommands) (SicsInterp * pSics);
  void (*RemoveSiteCommands) (SicsInterp * pSics);
   pMotor(*CreateMotor) (SConnection * pCon, int argc, char *argv[]);
   pCounterDriver(*CreateCounterDriver) (SConnection * pCon,
                                         int argc, char *argv[]);
  HistDriver *(*CreateHistogramMemoryDriver) (char *name,
                                              pStringDict pOption);
   pVelSelDriv(*CreateVelocitySelector) (char *name, char *array,
                                         void * pTcl);
   pCodri(*CreateControllerDriver) (SConnection * pCon, int argc,
                                    char *argv[]);
   pEVControl(*InstallEnvironmentController) (SicsInterp * pSics,
                                              SConnection * pCon, int argc,
                                              char *argv[]);
  int (*ConfigureScan) (pScanData self, char *option);
  void (*KillSite) (void *pData);
} Site, *pSite;

/*-------------------------------------------------------------------*/
pSite getSite(void);
#endif
