/**
 * This is the implemetation of some common support functions for single 
 * crystal diffraction.
 * 
 * copyright: see file COPYRIGHT
 * 
 * Mark Koenencke, August 2008
*/
#include <stdio.h>
#include "singlediff.h"
/*-----------------------------------------------------------------------*/
MATRIX calculateScatteringVector(pSingleDiff pSingle, double fHKL[3])
{
  MATRIX z1, hkl;
  int i;

  hkl = mat_creat(3, 1, ZERO_MATRIX);
  for (i = 0; i < 3; i++) {
    hkl[i][0] = fHKL[i];
  }
  z1 = mat_mul(pSingle->UB, hkl);
  mat_free(hkl);

  return z1;
}
