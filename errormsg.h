#ifndef ERRORMSG_H
#define ERRORMSG_H

#include <time.h>

/** \file
 * \brief Error message collection
 */
/** \brief Error message item
 */
typedef struct ErrMsg {
  struct ErrMsg *next;
  char *text;   /**< the message text */
  long cnt;     /**< count */
  time_t last;  /**< time of last message */
  long dirty;   /**< count since last reset */
  char *itemId; /**< an id for the item where the error occured */
  long id;
} ErrMsg;

typedef struct {
  ErrMsg *current;
} ErrList;

/** \brief Put a formatted message to the error message list
 *
 * The error message list contains only one entry for all messages
 * with the same text, storing only the count and the last used time.
 * Characters within square brackets are not taken into account
 * when comparing messages.
 * The new message is always at the head of the list.
 *
 *  \param list the error message list
 *  \param fmt the format for the message
 *  \return the new error message list head
 */

#if __GNUC__ > 2
#define G_GNUC_PRINTF( format_idx, arg_idx )    \
  __attribute__((__format__ (__printf__, format_idx, arg_idx)))
#else
#define G_GNUC_PRINTF( format_idx, arg_idx )
#endif
void ErrPutMsg(ErrList *list, char *fmt, ...) G_GNUC_PRINTF (2, 3);
#undef G_GNUC_PRINTF

/** \brief Get the most recent error message
 *  \param list the error list
 *  \return the most recent error message
 */
char *ErrGetLastMsg(ErrList *list);

#endif
