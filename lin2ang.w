\subsection{lin2ang}
lin2ang is a virtual motor object which allows to drive an angle
through a translation table. In ist first incarnation this is meant
for TOPSI but this may be useful in other places as well. The
displacement x is calculated from the formula: $ x = L sin 2\theta $
with L being the length of the arm  around which the angle
pivots. This must be a configurable parameter. 

lin2ang's datastructure is quite simple:
\begin{verbatim}
 typedef struct __LIN2ANG {
                             pObjectDescriptor pDes;
                             pIDrivable pDriv;
                             pMotor lin;
                             float length;
                             float zero;
                }Lin2Ang;
\end{verbatim}
The fields are:
\begin{description}
\item[pDes] The standard SICS object descriptor.
\item[pDriv] The drivable interface which hides most of the
functionality of this object.
\item[lin] The translation table motor to use for driving the angle.
\item[length] The length of the arm around which the angle pivots.  
\item[zero] The angular zero point of this virtual motor.  
\end{description}

The interface to this is quite simple, most of the functionality is
hidden in the drivable interface. Basically there are only the
interpreter interface functions.

@d lin2angint @{
  int MakeLin2Ang(SConnection *pCon, SicsInterp *pSics, void *pData,
                  int argc, char *argv[]);
  int Lin2AngAction(SConnection *pCon, SicsInterp *pSics, void *pData,
                  int argc, char *argv[]);

@}

@o lin2ang.h @{
/*-------------------------------------------------------------------------
                          L I N 2 A N G

  Drive an angle through a translation table. As of now only for
  TOPSI.

  copyright: see copyright.h

  Mark Koennecke, February 2000
--------------------------------------------------------------------------*/
#ifndef LIN2ANG
#define LIN2ANG
#include "motor.h"
@<lin2angint@>
#endif

@}

