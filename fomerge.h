/*---------------------------------------------------------------------------
  
                               F o M e r g e

 This module is for the FOCUS instrument. FOCUS has three detector 
 banks. These are sent to SICS as one big array by the histogram memory.
 However, depending on the situation these need to be accessed either:
 - as single banks.
 - or as a merged bank, where detector counts from all three banks are
   summed onto a common two theta scale. 

 This module is responsible for accessing single detectors and for doing
 the merging operation.


 Mark Koennecke, March 2000
 ---------------------------------------------------------------------------*/
#ifndef FOMERGE
#define FOMERGE

#define UPPER   1
#define MIDDLE  2
#define LOWER   3
#define MERGED  4
#define TIMEBIN 5

int setFMDataPointer(HistInt * lData, int timeBins, int bank);
   /*
      sets the data array with the unmerged data. timeBins is the
      length of the time binning used. upper, medium and lower are flags
      which indicate the presence of the detector bank.
    */

void setFMconfiguration(int upper, int medium, int lower);
   /*
      sets the configuration of the histogram memory
    */

HistInt *getFMBankPointer(int which);
   /*
      returns a pointer to the counted data for the detector bank 
      specified by which. Possible values are given above.  

    */
float *getFMBankTheta(int which);
   /*
      returns a pointer to a float array with the two theta values for
      the detector bank specified by which.
    */

int initializeFM(char *mergefile);
   /*
      initializes the two-theta and merging data from the file
      mergefile. This must have been called before anything else.
    */

void killFM(void);
    /*
       frees all merging data structures.
     */

int getFMdim(int which);
   /*
      returns the dimension in two theta for the detector banks of
      the length of the time binning if TIMEBIN has been specified.
    */

int InstallFocusMerge(SConnection * pCon, SicsInterp * pSics, void *pData,
                      int argc, char *argv[]);
  /*
     install the standalone FocusMerge module
   */
int FocusMergeAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                     int argc, char *argv[]);

  /*
     FocusMerge interpreter wrapper function
   */

#endif
