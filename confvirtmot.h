
/*-----------------------------------------------------------------------
  A configurable virtual motor object. At motor start a script is called
  which returns the motors and positions to drive as a comma separated 
  list holding entries of the form mot=value.

  COPYRIGHT: see file COPYRIGHT

  Mark Koennecke, August 2004
-------------------------------------------------------------------------*/
#ifndef CONFIGURABLEVIRTUALMOTOR
#define CONFIGURABLEVIRTUALMOTOR

#include "sics.h"

int MakeConfigurableVirtualMotor(SConnection *pCon, SicsInterp *pSics,
        void *data, int aargc, char *argv[]);
int ConfigurableVirtualMotorAction(SConnection *pCon, SicsInterp *pSics,
        void *data, int argc, char *argv[]);
#endif

