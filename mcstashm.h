/*--------------------------------------------------------------------------
  This is a driver for a histogram memory which is meant to interoperate
  with a McStas simulation. For reasons of lazyness the control of counting
  operations is delegated to a single counter object and this driver only
  provides data space and means to access and copy data. This driver also
  works without a counter connected in order to support multi HM control via
  the hmcontrol module. 
  
  copyright: see file COPYRIGHT
  
  Mark Koennecke, June 2005
  -------------------------------------------------------------------------*/
#ifndef _MCSTASHM_H_
#define _MCSTASHM_H_
pHistDriver NewMcStasHM(pStringDict pOpt);
#endif                          //_MCSTASHM_H_
