
/*----------------------------------------------------------------------
 SICS interface to the triple axis spectrometer calculation 
 module.

 copyright: see file COPYRIGHT
 
 Mark Koennecke, April-May 2005
----------------------------------------------------------------------*/
#ifndef TASUB
#define TASUB
#include <stdio.h>
#include "tasublib.h"
#include "cell.h"
#include "motor.h"

/*------------------- defines for tasMode -----------------------------------*/
        typedef struct{
                pObjectDescriptor pDes;
                tasMachine machine;
                int reflectionList;
                lattice cell;
                tasQEPosition target;
                tasQEPosition current;
                int tasMode;
                int outOfPlaneAllowed;
                double targetEn, actualEn;
                int mustRecalculate;
                int mustDrive; 
                int mustDriveQ;
                pMotor motors[12];
                char motname[12][32];
                tasReflection r1, r2;
		pIDrivable mono;
                TaskGroupID monoTaskID;
		void *monoData;
                int ubValid;
                int silent;
                int stopFixed; /* flag to stop multiple fixed messages in scans*/
                char *updater;
		TaskGroupID groupID;
}tasUB, *ptasUB;


/*--------------------- the tas virtual motor data structure ---------------------*/
typedef struct {
        pObjectDescriptor pDes;
        pIDrivable pDriv;
        ptasUB math;
        int code;       
        }tasMot, *ptasMot;

/*--------------------------------------------------------------------*/

int TasUBFactory(SConnection *pCon,SicsInterp *pSics, void *pData,                
                int argc, char *argv[]);
int TasUBWrapper(SConnection *pCon,SicsInterp *pSics, void *pData,                
                int argc, char *argv[]);


int findReflection(int list, int idx, ptasReflection r);
int tasUpdate(SConnection *pCon, ptasUB self);
void setStopFixed(ptasUB self, int val);
#endif 
