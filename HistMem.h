
#line 443 "histogram.w"

/*--------------------------------------------------------------------------
                    H I S T M E M 

  header for the histogram memory object for SICS.

  copyright: see implementation file.
  Mark Koennecke, April 1997
-----------------------------------------------------------------------------*/
#ifndef SICSHISTMEM
#define SICSHISTMEM
#define MAXDIM 3

  typedef struct __HistDriver *pHistDriver;
  typedef struct __HistMem    *pHistMem;
/*-------------------------------------------------------------------------*/
  typedef int HistInt;
/*
  32 bit integer on a DigitalUnix
*/

#line 9 "histogram.w"

  typedef enum {
                 eHTransparent,
                 eHNormal,
                 eHTOF,
                 eHStrobo,
                 eHRPT,
                 ePSD,
                 eSANSTOF
               } HistMode;

#line 36 "histogram.w"

   typedef enum {
                 eOIgnore,
                 eOCeil,
                 eOCount,
                 eReflect
                } OverFlowMode;

#line 463 "histogram.w"

/*--------------------------------------------------------------------------*/

#line 292 "histogram.w"

   pHistMem CreateHistMemory(char *drivername);
   void     DeleteHistMemory(void *self);

#line 308 "histogram.w"

   int HistGetOption(pHistMem self, char *name, char *result, int iResultLen);
   int HistSetOption(pHistMem self, char *name, char *value);
   int HistConfigure(pHistMem self, SConnection *pCon, SicsInterp *pSics);

#line 336 "histogram.w"

  float GetHistPreset(pHistMem self);
  int   SetHistPreset(pHistMem self, float fVal);
  CounterMode GetHistCountMode(pHistMem self);
  int         SetHistCountMode(pHistMem self, CounterMode eNew);
  long GetHistMonitor(pHistMem self, int i, SConnection *pCon);
  const float *GetHistTimeBin(pHistMem self, int *iLength);
  int GetHistLength(pHistMem self);
  int GetHistDim(pHistMem self, int iDim[MAXDIM], int *nDim);
  float GetHistCountTime(pHistMem self,SConnection *pCon);
  int HistDoCount(pHistMem self, SConnection *pCon);
  int HistBlockCount(pHistMem self, SConnection *pCon);
  void HistDirty(pHistMem self);        

  int isSecondGen(pHistMem self);
  pHistMem FindHM(SicsInterp *pSics, char *name);


#line 369 "histogram.w"

   int SetHistogram(pHistMem self, SConnection *pCon,
                    int i,int iStart, int iEnd, HistInt *lData);
   int GetHistogram(pHistMem self, SConnection *pCon,
                    int i,int iStart, int iEnd, HistInt *lData, int iDataLen);
   HistInt *GetHistogramPointer(pHistMem self,SConnection *pCon);
   int GetHistogramDirect(pHistMem self, SConnection *pCon,
                          int i, int iStart, int iEnd, 
                          HistInt *lData, int iDataLen);
   int PresetHistogram(pHistMem self, SConnection *pCon, HistInt lVal);

#line 412 "histogram.w"

   int MakeHistMemory(SConnection *pCon, SicsInterp *pSics, void *pData,
                      int argc, char *argv[]);

   int HistAction(SConnection *pCon, SicsInterp *pSics, void *pData,
                      int argc, char *argv[]);
    

#line 465 "histogram.w"


#endif
