
/*--------------------------------------------------------------------------
                          V A R L O G
  
  A module which keeps  a log of a variable.

  Mark Koennecke, September 1997

  copyright: see implementation file
 
----------------------------------------------------------------------------*/
#ifndef SICSVARLOG
#define SICSVARLOG
#include <time.h>

/*---------------------------- birth and death ----------------------------*/
typedef struct __VarLog *pVarLog;

int VarlogInit(pVarLog * self);
int VarlogDelete(pVarLog self);
/*----------------------------- operation -------------------------------*/
int VarlogClear(pVarLog self);
int VarlogAdd(pVarLog self, float fVal);
/*------------------------------ data recovery -------------------------*/
int VarlogLength(pVarLog self, int *iLength);
int VarlogGetTime(pVarLog self, time_t * tTime);
int VarlogGetVal(pVarLog self, float *fValues);
int VarlogGetMean(pVarLog self, float *fMean, float *fStdDev);
/*------------------------------ interpreter ---------------------------*/
int VarlogWrapper(pVarLog self, SConnection * pCon,
                  char *subcommand, char *sub2, char *pVarName);

#endif
