/*
 * singlebinb.h.
 *
 * Documentation: see singlebinb.c
 *
 *
 *  Created on: Feb 13, 2012
 *      Author: koennecke
 */

#ifndef SINGLEBINB_H_
#define SINGLEBINB_H_


void initializeSingleBINB(pSingleDiff diff);


#endif /* SINGLEBINB_H_ */
