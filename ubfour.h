/**
 * This is a library for calculating UB matrices for four circle diffraction.
 * The algorithm and setting definitions is from:
 *            Busing & Levy, Acta Cryst. (1967), 22, 457ff 
 *
 * Implemented:
 * - UB from cell cell constants and two reflections.
 * - UB from three reflections
 * - Brute force index search
 *
 * Mark Koennecke, March 2005
 * 
 * Added some general crystallographic calculations and stuff for conus scans 
 * 
 * Mark Koennecke, March 2006
 */
#ifndef SICSUBFOUR
#define SICSUBFOUR

#include <stdio.h>
#include "matrix/matrix.h"
#include "cell.h"

/**
 * error codes: also see cell.h
 */
#define UBNOMEMORY -200
#define INVALID_LAMBDA -201
#define NOTRIGHTHANDED -202
/**
 * a reflection data structure holding the indices h,k,l and
 * the magic four circle angles two_theta, om, chi and phi.
 */
typedef struct {
  double h, k, l;
  double s2t, om, chi, phi; /* bisecting */
  double gamma, omnb, nu; /* normal beam */
} reflection;
/**
 * calculate a UB matrix from cell constants and two reflections
 * @param direct The direct cell constants
 * @param r1 The first reflection.
 * @param r2 The second reflection.
 * @param errCode an error indicator if things go wrong.
 * @return The resulting UB matrix or NULL on errors
 */
MATRIX calcUBFromCellAndReflections(lattice direct, reflection r1,
                                    reflection r2, int *errCode);
/**
 * calculate a UB matrix for normal beam geometry from cell constants and 
 * two reflections
 * @param direct The direct cell constants
 * @param r1 The first reflection.
 * @param r2 The second reflection.
 * @param errCode an error indicator if things go wrong.
 * @return The resulting UB matrix or NULL on errors
 */
MATRIX calcNBUBFromCellAndReflections(lattice direct, reflection r1,
                                    reflection r2, int *errCode);
/**
 * calculate the UB matrix from three reflections. The three reflections must not be 
 * coplanar and must reflect a right handed 
 * @param r1 The first reflection
 * @param r2 The second reflection
 * @param r3 The third reflection.
 * @param errCode a code indictang errors which happened.
 * @return A UB matrix on success or NULL on errors. Then errcode will indicate
 * the type of teh error.
 */
MATRIX calcUBFromThreeReflections(reflection r1, reflection r2,
                                  reflection r3, double lambda,
                                  int *errCode);
/**
 * calculate the UB matrix from three reflections for normal beam geomtry.
 *  The three reflections must not be coplanar and must reflect a right handed set.
 * @param r1 The first reflection
 * @param r2 The second reflection
 * @param r3 The third reflection.
 * @param errCode a code indictang errors which happened.
 * @return A UB matrix on success or NULL on errors. Then errcode will indicate
 * the type of teh error.
 */
MATRIX calcNBUBFromThreeReflections(reflection r1, reflection r2,
                                  reflection r3, double lambda,
                                  int *errCode);
/**
 * a data structure holding an indexing suggestion
 */
typedef struct {
  double h, k, l;               /* suggested index */
  double t2obs, t2calc, t2diff; /*  2 theta observed and calculated and the difference */
} refIndex, *prefIndex;
/**
 * search for possible indexes matching the two theta value given. This is a brute force search
 * @param direct The lattice constants of the crystal
 * @param lambda The wavelength used
 * @param two_theta The two theta value of the reflection 
 * @param max_deviation maximum allowed diviation of calculated two thetas from tw-Theta given
 * @param limit Index limit for the search
 * @param index Preallocated array for storing index suggestions. The procedure will
 * sort the entries in this array according to the difference in two theta
 * @param maxIndex The number of entries allowed for index.
 * @return The number of indexes in index or a negative error code when an error
 * occurs.
 */
int searchIndex(lattice direct, double lambda, double two_theta,
                double max_deviation, int limit, refIndex index[],
                int maxIndex);
/**
 * calculate the angle between two reflections, given their miller indices
 * @param B The B metric matrix
 * @param r1 first reflection
 * @param r2 second reflection
 * @return angle between reflections
 */
double angleBetweenReflections(MATRIX B, reflection r1, reflection r2);
/**
 * calculate the length of the scattering vector belonging to r
 * @param B The B metric matrix to use
 * @param r The reflection for which to calculate
 * @return The length of the scattering vector
 */
double scatteringVectorLength(MATRIX B, reflection r);
/**
 * build the conversion MATRIX from the Busing Levy psi system (z = UB*h)
 * to the coordinate system of a given center reflection for a cone
 * @param r The reflection around which the cone is situated
 * @param lambda The wavelength
 * @return A sutiable conversion matrix or NULL in case of errors
 */
MATRIX makeInstToConeVectorMatrix(reflection r, double lambda);
/**
 * calculate a scattering vector on a cone around a given reflection at a
 * specified cone rotation angle and a cone opening angle. The center 
 * reflection of the cone is hidden in the conversion matrix.
 * @param openingAngle The opening angle of the cone
 * @param coneAngle The angle on the cone
 * @param coneToPsi The matrix for the conversion from the cone coordinate
 * system to the psi instrument coordinate system.
 * @return a scattering vector on the cone
 */
MATRIX calcConeVector(double openingAngle, double coneAngle,
                      double length, MATRIX coneToPsi);
#endif
