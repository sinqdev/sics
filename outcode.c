/* ------------------------------------------------------------------------
  The OutCode's for SICS have to be translated from text at several
  places in SICS. Wherever necessary such code should include this file.
  This restricts changes to Scommon.h and this file
  
  Mark Koennecke, November 1996
----------------------------------------------------------------------------*/
#include "Scommon.h"
#include "outcode.h"
#include <strings.h>

const char* OutCodeToTxt(OutCode eOut)
{
  switch (eOut) {
  case eInternal: return "Int"; /* internal */
  case eCommand:  return "Cmd"; /* reserved, not used */
  case eHWError:  return "ErH"; /* reserved, used only for SICSLog */
  case eInError:  return "ErI"; /* reserved, used as a mark in the handling of output codes */
  case eStatus:   return "Sta"; /* reserved, deprecated */
  case eValue:    return "Val"; /* value reponse: copied into Tcl */
  case eStart:    return "Beg"; /* start message */
  case eFinish:   return "End"; /* finish message */
  case eEvent:    return "Evt"; /* some callback messages */
  case eWarning:  return "Wrn"; /* warnings */
  case eError:    return "Err"; /* error: copied into Tcl */
  case eHdbValue: return "HVa"; /* hipadaba value chnage */
  case eHdbEvent: return "HEv"; /* Hipadaba event */
  case eLog:      return "Log"; /* log message: is always written to client */
  case eLogError: return "ErL"; /* error message to log: is always written to client */
  }
  return "???";
}

int OutCodeFromText(const char *text, OutCode *outcode)
{
  int i;
  for (i = 0; i < iNoCodes; ++i) {
    if (strcasecmp(text, pCode[i]) == 0) {
      if (outcode)
        *outcode = i;
      return i;
    }
  }
  return -1;
}
