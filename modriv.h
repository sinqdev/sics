/*------------------------------------------------------------------------
	Header file for an EL734 motor driver 
	
	
	Mark Koennecke, November 1996
	
	copyright: see implementation file
----------------------------------------------------------------------------*/

#ifndef SICSEL734
#define SICSEL734
#include "SCinter.h"

#define MOTREDO -1
#define MOTFAIL  0
#define MOTOK    1
#define TEXTPARLEN 1024

typedef struct __AbstractMoDriv {
  /* general motor driver interface 
     fields. REQUIRED!
   */
  float fUpper;                 /* upper limit */
  float fLower;                 /* lower limit */
  char *name;
  int (*GetPosition) (void *self, float *fPos);
  int (*RunTo) (void *self, float fNewVal);
  int (*GetStatus) (void *self);
  void (*GetError) (void *self, int *iCode, char *buffer, int iBufLen);
  int (*TryAndFixIt) (void *self, int iError, float fNew);
  int (*Halt) (void *self);
  int (*GetDriverPar) (void *self, char *name, float *value);
  int (*SetDriverPar) (void *self, SConnection * pCon,
                       char *name, float newValue);
  void (*ListDriverPar) (void *self, char *motorName, SConnection * pCon);
  void (*KillPrivate) (void *self);
  int (*GetDriverTextPar) (void *self, char *name, char *textPar);
} MotorDriver;

 /* the first fields above HAVE to be IDENTICAL to those below */


typedef struct ___MoSDriv {
  /* general motor driver interface 
     fields. REQUIRED!
   */
  float fUpper;                 /* upper limit */
  float fLower;                 /* lower limit */
  char *name;
  int (*GetPosition) (void *self, float *fPos);
  int (*RunTo) (void *self, float fNewVal);
  int (*GetStatus) (void *self);
  void (*GetError) (void *self, int *iCode, char *buffer, int iBufLen);
  int (*TryAndFixIt) (void *self, int iError, float fNew);
  int (*Halt) (void *self);
  int (*GetDriverPar) (void *self, char *name, float *value);
  int (*SetDriverPar) (void *self, SConnection * pCon,
                       char *name, float newValue);
  void (*ListDriverPar) (void *self, char *motorName, SConnection * pCon);
  void (*KillPrivate) (void *self);
  int (*GetDriverTextPar) (void *self, char *name, char *textPar);

  /* Simulation  specific fields */
  float fFailure;               /* percent random failures */
  float fSpeed;
  time_t iTime;
  float fPos;                   /* position */
  float fTarget;                /* target position */
} SIMDriv;


/* ----------------------- Simulation -----------------------------------*/
MotorDriver *CreateSIM(SConnection * pCon, int argc, char *argv[]);
void KillSIM(void *pData);
MotorDriver *RGMakeMotorDriver(void);
#endif
