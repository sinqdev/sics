/**
 * This is a generalized polling module for SICS. With this module
 * SICS variables can be polled regulary for updates. For different types of
 * SICS variables different polling mechanisms are required. In order to cope with 
 * this requirement a polling interface and different drivers are defined in the
 * sister module polldriv.h and polldriv.c. This module implements the interface 
 * to configure polling and the SICS task to run polling.  
 * 
 * Copyright: see COPYRIGHT
 * 
 * Mark Koennecke, November-December 2006
 */
#ifndef SICSPOLL_H_
#define SICSPOLL_H_

#include "SCinter.h"
#include "conman.h"

/*=================== interpreter interface ================================*/
/**
 * the factory function
 */
int InstallSICSPoll(SConnection * pCon, SicsInterp * pSics, void *pData,
                    int argc, char *argv[]);
/*
 * the actual wrapper which allows to configure and query the polling 
 * module
 */
int SICSPollWrapper(SConnection * pCon, SicsInterp * pSics, void *pData,
                    int argc, char *argv[]);
/*================== Internal Interface ===================================*/
typedef struct __SICSPOLL SicsPoll, *pSicsPoll;
/**
 * add an object to the list of pollable objects.
 * @param self A pointer to a sicsPoll object managing the poll loop.
 * @param pCon a connection to report errors to 
 * @param objectidentifier A string describing the object to poll. 
 *  This parameter will be used by the poll driver to locate the 
 * pollable obejct.
 * @param The driver to use for polling
 * @param argc The number of additional parameters to pass to the 
 *  poll driver
 * @param argv[] The parameters to pass to the poll driver.
 * @return 1 on success or a negative error code when things go wrong.
 */
int addPollObject(SicsPoll * self, SConnection * pCon,
                  char *objectIdentifier, char *driver, int argc,
                  char *argv[]);
/**
 * remove an object from the polling loop.
 * @param self A pointer to a sicsPoll object managing the poll loop. 
 * @param  objectIdentifier The identifier of the object to remove from 
 *  the poll loop.
 * @return 1 on success, a negative error code on failure.
 */
int removePollObject(SicsPoll * self, char *objectIdentifier);

#endif                          /*SICSPOLL_H_ */
