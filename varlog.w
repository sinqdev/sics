\subsection{VarLog}
This is a lillte helper class which can be used to keep  a log of a
variable. A variables value is stored together with time information in a
list. Various functions allow to interact with this list and retrieve the
log. For the list the lld  package is used again. Thus the only data
required by this module is the integer ID of the list. 

The following functions are provided:

@d logint @{
/*---------------------------- birth and death ----------------------------*/  
   typedef struct __VarLog *pVarLog;

   int VarlogInit(pVarLog *self);
   int VarlogDelete(pVarLog self);
/*----------------------------- operation -------------------------------*/
   int VarlogClear(pVarLog self);
   int VarlogAdd(pVarLog self, float fVal);
/*------------------------------ data recovery -------------------------*/
   int VarlogLength(pVarLog self, int *iLength);
   int VarlogGetTime(pVarLog self, time_t *tTime);
   int VarlogGetVal(pVarLog self, float *fValues);
   int VarlogGetMean(pVarLog self, float *fMean, float *fStdDev);
/*------------------------------ interpreter ---------------------------*/
   int VarlogWrapper(pVarLog self,SConnection *pCon, 
                     char *subcommand, char *sub2,char *pVarName);
@}

All functions return 1 on success, 0 on failure if not mentioned otherwise.

\begin{description}
\item [ValogInit] creates a new varlog and prepares for data collection.
Only parameter is a pointer to an int which will be initialised to the list
ID to use.
\item [VarlogDelete] deletes a varlog and frees the list. Single parameter
is a pointer to an int holding the ID of the list to kill.
\item [VarlogClear] clears old logged data but keeps the varlog alive for
further data collection.
\item [VarlogAdd] adds the float value fVal to the log denoted by iList. The
time stamp will be generated automatically. 
\item [VarlogLength] retrieves the length of the current log.
\item [VarlogGetTime] retrieves the array of time stamps as provided by the
system function time  from the log.
\item [VarlogGetVal] retrieves the array of  values from the log.
\item [VarlogGetMean] retrieves the mean value of the values from the log
denoted by iList  and their standard deviation.
\item [VarlogWrapper] is a function which may be invoked from an object 
wrapper function in order to interpret and handle the var logs subcommands.  
Parameters are: the varlog list, a switch variable which will be set to 1
if logging is enabled and to 0 if not, a connection object to which data
 will be written and finally the subcommand to evaluate.
\end{description}

@o varlog.h @{
/*--------------------------------------------------------------------------
                          V A R L O G
  
  A module which keeps  a log of a variable.

  Mark Koennecke, September 1997

  copyright: see implementation file
 
----------------------------------------------------------------------------*/
#ifndef SICSVARLOG
#define SICSVARLOG
#include <time.h>
@<logint@>
#endif
@}




