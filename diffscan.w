\subsection{Differential Scan}
This is a special very fast but inaccurate scan method. The scan motor is 
set into motion and counts are collected on the fly, while the motor
is moving. As neither the motor speed, nor the counters response nor
the source stability can be relied upon to result in equally spaced
and well defined counts, the counts have to be scaled according to the
difference in monitor counts towards the previous count. This is why
this is called differential scan. This is not a
precise scan. But is a very fast one which helps locating peaks or
during alignment. 

This is implemented as an operator on top of the standard scan
module. All scan details, scan variables etc have to be configured in
the main scan module. This object then just runs the diff scan  and
stores the result in the main scan object for further processing. For
instance peak location. The end of the scan is calculated from the
start, step and NP of the main scan module. be aware that these values
have no well defined meaning during this kind of scan, NP will be
corrected to account for the points actually measured.  

As motors cannot be guaranteed to run simulataneously, only one scan
variable is suported for this.

The actual control of this scan is hidden in a task function which is
responsible for storing the data and stopping when the motor has
finished driving. 

In order to do a differential scan a data structure is required:
@d diffscandat @{
typedef struct {
	pObjectDescriptor pDes;
	ObPar             *parArray;
	int normalizationScale;
	int scaleMonitor;
	CountEntry last;
	int skip;
        int skipCount;
	pScanData scanObject;
	} DiffScan, *pDiffScan;
@} 
The fields:
\begin{description}
\item[pDes] The standard object descriptor.
\item[parArray] An array of parameters for the module.
\item[normalizationScale] The scale to which to scale counts during
counting.This will be the monitor difference between the first and the
second point. 
\item[lastMonitor] is the last monitor read for caluclating differences.
\item[scaleMonitor] The monitor to use for scaling. This should better
be a monitor with a high count rate for acurate scaling. 
\item[skip] How many cycles of the main loop to skip between
recordings.
\item[skipCount] Counter for skipped cycles. Together with skip this
is a means to limit the sampling rate of diffscan. 
\item[scanObject] The scan object we are operating upon. 
\end{description}

The external interface to this module is like this:
@d diffscanint @{
  /**
    * RunDiffScan runs a differential scan.
    * @@param self The Diffscan object to use
    * @@param pScan The scan object to use for configuration and for
    * for storing the results.
    * @@param pCon The connection to use for output and errors.
    * @@param fEnd The end value for the diffscan
    */
  int RunDiffScan(pDiffScan self, pScanData pScan, 
	SConnection *pCon, float fEnd);
/*==================== interpreter wrappers ==========================*/
  int DiffScanWrapper(SConnection *pCon, SicsInterp *pSics, void *pData,
                  int argc, char *argv[]);
  int MakeDiffScan(SConnection *pCon, SicsInterp *pSics, void *pData,
                  int argc, char *argv[]);

  
@}

@o diffscan.h @{
/*-------------------------------------------------------------------
  diffscan is an operator which can perform a fast differential scan 
  while a motor is running. 

  copyright: see file COPYRIGHT

  Mark Koennecke, November 2004
---------------------------------------------------------------------*/
#ifndef SICSDIFFSCAN
#define SICSDIFFSCAN
#include "obpar.h"
#include "scan.h"
#include "scan.i"
@<diffscandat@> 
/*==================================================================*/
@<diffscanint@>
#endif
@}