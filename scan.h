
/*---------------------------------------------------------------------------
                               S C A N

  Header file for the SICS scan object.

  Mark Koennecke, October 1997

  Extracted scan variable: Mark Koennecke, November 2004

  copyright: see copyright.h
-----------------------------------------------------------------------------*/
#ifndef SICSSCAN1
#define SICSSCAN1
typedef struct __ScanData *pScanData;
/*--------------------------------------------------------------------------*/
#include "counter.h"

/*------------------------- live & death ----------------------------------*/
pScanData CreateScanObject(char *pRecover, char *pHeader,
                           pCounter pCount, char *objName);
void DeleteScanObject(void *self);
/*-------------------------------------------------------------------------*/
int AddScanVar(pScanData self, SicsInterp * pSics, SConnection * pCon,
               char *name, float fStart, float fStep);
int ClearScanVar(pScanData self);

int DoScan(pScanData self, int iNP, int iMode, float fPreset,
           SicsInterp * pSics, SConnection * pCon);
int SilentScan(pScanData self, int iNP, int iMode, float fPreset,
               SicsInterp * pSics, SConnection * pCon);
int RecoverScan(pScanData self, SicsInterp * pSics, SConnection * pCon);

int GetScanCounts(pScanData self, long *lData, int iDataLen);
int GetScanVar(pScanData self, int iWhich, float *fData, int iDataLen);
int GetSoftScanVar(pScanData self, int iWhich, float *fData, int iDataLen);

int GetScanVarName(pScanData self, int iWhich, char *pName, int iLength);
int GetScanVarStep(pScanData self, int iWhich, float *fStep);
int GetScanMonitor(pScanData self, int iWhich, long *lData, int iDataLen);
int GetScanNP(pScanData self);
float GetScanPreset(pScanData self);
int isScanRunning(pScanData self);

int ScanIntegrate(pScanData self, float *fSum, float *fVariance);

int SimScan(pScanData self, float fPos, float FHWM, float fHeight);
    /* 
       creates a simulated gaussian shaped peak with the parameters given.
     */
int ResetScanFunctions(pScanData self);
    /*
       resets the configurable scan functions to their default values.
     */
int NonCheckPrepare(pScanData self);
    /*
       a function for the PrepareScan field in the scan data structure
       which does not check the boundaries of the scan as the default
       PrepareScan does.
     */
int AppendScanLine(pScanData self, char *line);
    /*
       AppendScanLine appends a line to the scan data file. When finished
       it updates the position pointer in the file to point behind the
       added line.      
     */
int StoreScanCounts(pScanData self, char *data);
    /*
       parses the numbers in data and stores them as the count and
       monitor data for the current scan point.
     */
/*------------------------ Interpreter Interface --------------------------*/
int ScanFactory(SConnection * pCon, SicsInterp * pSics, void *pData,
                int argc, char *argv[]);

int ScanWrapper(SConnection * pCon, SicsInterp * pSics, void *pData,
                int argc, char *argv[]);


#endif
