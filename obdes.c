/*--------------------------------------------------------------------------
	Implementation file for the two little functions dealing with
	ObjectDescriptors
	

	Mark Koennecke,    November 1996

        revised: Mark Koennecke, June 1997

	Copyright:

	Labor fuer Neutronenstreuung
	Paul Scherrer Institut
	CH-5423 Villigen-PSI


        The authors hereby grant permission to use, copy, modify, distribute,
        and license this software and its documentation for any purpose, provided
        that existing copyright notices are retained in all copies and that this
        notice is included verbatim in any distributions. No written agreement,
        license, or royalty fee is required for any of the authorized uses.
        Modifications to this software may be copyrighted by their authors
        and need not follow the licensing terms described here, provided that
        the new terms are clearly indicated on the first page of each file where
        they apply.

        IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
        FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
        ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
        DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
        POSSIBILITY OF SUCH DAMAGE.

        THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
        INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
        IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
        NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
        MODIFICATIONS.
------------------------------------------------------------------------------*/
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "fortify.h"
#include "obdes.h"
#include "conman.h"
#include "hipadaba.h"
#include "logv2.h"
/*-------------------------------------------------------------------------*/
static void *DefaultGetInterface(void *pData, int iID)
{
  return NULL;
}

/*-------------------------------------------------------------------------*/
static int DefaultSave(void *self, char *name, FILE * fd)
{
  return 1;
}

/*--------------------------------------------------------------------------*/
pObjectDescriptor CreateDescriptor(char *name)
{
  pObjectDescriptor pRes = NULL;

  pRes = (pObjectDescriptor) malloc(sizeof(ObjectDescriptor));
  if (!pRes) {
    return NULL;
  }
  pRes->name = strdup(name);
  pRes->pKeys = NULL;
  pRes->parNode = NULL;
  pRes->SaveStatus = DefaultSave;
  pRes->GetInterface = DefaultGetInterface;
  return pRes;
}

/*---------------------------------------------------------------------------*/
void DeleteDescriptor(pObjectDescriptor self)
{
  assert(self);
  if (self->name)
    free(self->name);
  if (self->pKeys)
    IFDeleteOptions(self->pKeys);
  /*
   * delete a parameter node only when not linked elsewhere
   */
  if (self->parNode != NULL) {
    if (self->parNode->mama == NULL) {
      DeleteHipadabaNode(self->parNode, NULL);
    }
  }
  free(self);

}

/*-------------------------------------------------------------------------*/
pDummy CreateDummy(char *name)
{
  pDummy pRes = NULL;

  pRes = (pDummy) malloc(sizeof(Dummy));
  if (!pRes) {
    Log(ERROR,"sys","Out of Memory  in CreateDummy");
    return NULL;
  }

  pRes->pDescriptor = CreateDescriptor(name);
  if (!pRes->pDescriptor) {
    free(pRes);
    Log(ERROR,"sys","%s","Out of Memory  in CreateDummy");
    return NULL;
  }
  return pRes;
}

/*--------------------------------------------------------------------------*/
void KillDummy(void *pData)
{
  pDummy pVictim;

  if (!pData)
    return;

  pVictim = (pDummy) pData;
  if (pVictim->pDescriptor) {
    DeleteDescriptor(pVictim->pDescriptor);
  }
  free(pData);
}

/*-------------------------------------------------------------------------*/
int iHasType(void *pData, char *Type)
{
  pDummy pTest;

  assert(pData);
  pTest = (pDummy) pData;
  if (!pTest->pDescriptor) {
    return 0;
  }
  if (strcmp(pTest->pDescriptor->name, Type) == 0) {
    return 1;
  }
  return 0;
}

/*------------------------------------------------------------------------*/
pObjectDescriptor FindDescriptor(void *pData)
{
  pDummy pDum = NULL;

  assert(pData);
  pDum = (pDummy) pData;
  return pDum->pDescriptor;
}

/*--------------------------------------------------------------------------*/
void SetDescriptorKey(pObjectDescriptor self, char *keyName,
                      char *eltValue)
{
  if (NULL != self) {
    self->pKeys = IFSetOption(self->pKeys, keyName, eltValue);
  }
}

/*--------------------------------------------------------------------------*/
void SetDescriptorGroup(pObjectDescriptor self, char *group)
{
  if (NULL == self) {
    return;
  }
  self->pKeys = IFSetOption(self->pKeys, "group", group);
}

/*--------------------------------------------------------------------------*/
void SetDescriptorDescription(pObjectDescriptor self, char *description)
{
  if (NULL == self) {
    return;
  }
  self->pKeys = IFSetOption(self->pKeys, "description", description);
}

  /*--------------------------------------------------------------------------*/
char *GetDescriptorKey(pObjectDescriptor self, char *keyName)
{
  if (NULL == self) {
    return NULL;
  }
  return IFindOption(self->pKeys, keyName);
}

/*--------------------------------------------------------------------------*/
char *GetDescriptorGroup(pObjectDescriptor self)
{
  if (NULL == self) {
    return NULL;
  }
  return IFindOption(self->pKeys, "group");
}

/*--------------------------------------------------------------------------*/
char *GetDescriptorDescription(pObjectDescriptor self)
{
  if (NULL == self) {
    return NULL;
  }
  return IFindOption(self->pKeys, "description");
}
