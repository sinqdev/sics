\subsection{The SICS Server Object}
This objects responsability is the proper initialisation and shutdown
 of the SICS server and the provision of some utility functions. 
The Server's data structure holds pointers to the most important parts of
the system:

@d servdat @{

   typedef struct __SicsServer {
                    SicsInterp *pSics;
                    pTaskMan   pTasker;
                    pExeList   pExecutor;
                    pEnvMon    pMonitor;
                    mkChannel  *pServerPort;
                    pNetRead   pReader;
                    int        simMode;
                    SConnection *dummyCon;
                   } SicsServer;
@} 

The fields are:
\begin{description}
\item[pSics] is a pointer to the SICS interpreter.
\item[pTasker] is a pointer to the task scheduler.
\item[pExecutor] is a pointer to the device executor. This module monitors 
 variables during counting and driving operations.
\item[pMonitor] is a pointer to an environment device controller monitor.
This module monitors sample environment controllers.
\item[pServerPort] points to a data structure describing the port at which
the SICS server is listening for connections.
\item[pReader] points to a data structure which defines the network
communication object.  
\item[simMode] a flag which is true when the SICS server is a simulation
 server.
\item[dummyCon] A dummy connection to use when no other connection is
available for some reason. 
\end{description}


In terms of a function interface the server module supplies.  

@d servint @{
/*----------------------------------------------------------------------*/
    int     InitServer(char *file, pServer *pServ);
    void    RunServer(pServer self);
    void    StopServer(pServer self);
/*----------------------------------------------------------------------*/
    SicsInterp  *GetInterpreter(void);
    pExeList    GetExecutor(void);
    pTaskMan    GetTasker(void);
    void ServerWriteGlobal(char *pMessage, int iCode);
@}

For most functions the name already says what it does. {\bf
InitServer} initializes the SICS servers data structures. {\bf
RunServer} implements the main loop and essentially calls the task
modules ScheduleTask function for running all the SICS tasks which
will handle the rest of the job. {\bf Stopserver} is responisble for
closing the SICS server down in a sensible manner and in a  well
defined sequence of events. 
{\bf ServerWriteGlobal}
is special. This function sends the message pMessage to all clients
currently connected to the SICS server with the output code iCode. iCode has
the same meaning as in the connection object.
@o nserver.h @{
/*--------------------------------------------------------------------------
                        N S E R V E R

  The SICS server main data structure and functions.

  Restructured: September 1997

  Mark Koennecke.
  
  copyright: see copyright.h
----------------------------------------------------------------------------*/
#ifndef SICSNSERVER
#define SICSNSERVER
#include "conman.h"
#include "SCinter.h"
#include "emon.h"
#include "devexec.h"
#include "task.h"
#include "network.h"

 typedef struct __SicsServer *pServer;

#include "nread.h"
@<servdat@>
@<servint@>
     int UserWait(SConnection *pCon, SicsInterp *pSics, void *pData,
                  int argc, char *argv[]);

     int SicsWait(long lTime);
#endif
@}
