
/*------------------------------------------------------------------------------------------------------
 Virtual motor interface to reciprocal space coordinates H, K and L for a four circle diffractometer.
 Requires a HKL object for calculations.

 copyright: see file COPYRIGHT

 Mark Koennecke, February 2005
--------------------------------------------------------------------------------------------------------*/
#ifndef SICSHKLMOT
#define SICSHKLMOT
/*====================== data structure ==============================================================*/

typedef         struct __HKLMOT {
                        pObjectDescriptor pDes;
                        pHKL pHkl;
                        pIDrivable pDriv;
                        int index;
                        }HKLMot, *pHKLMot;

/*======================= interpreter interface ======================================================*/
int HKLMotAction(SConnection *pCon, SicsInterp *pSics, void *pData, int argc, char *argv[]);
int HKLMotInstall(SConnection *pCon, SicsInterp *pSics, void *pData, int argc, char *argv[]);

#endif

        
