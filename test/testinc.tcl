#------------------------------------------------------------------------------
# This is a prelude to source into tcl for testing regression tests.
# copyright: see file COPYRIGHT
#
# Mark Koennecke, July 2006
#------------------------------------------------------------------------------
source tcltest.tcl
namespace import tcltest::*
source testutil.tcl
source sicstcldebug.tcl
