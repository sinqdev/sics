#--------------------------------------------------------------------------
# This file contains some regression tests for the SICS scan module. 
# This relies on the presence of a suitable configured multicounter in 
# the test initialization file. That multi counters script must have been
# configured to generate an gaussian based on a a4 position. This gaussian
# will then be used to check data and for testing peak based algorithms 
# such as optimization routines, peak and center etc.
#
# Mark Koennecke, October 2006
#--------------------------------------------------------------------------

clientput "Testing scan routines.."

config rights Mugger Mugger
aba setpar errortype 1 0
config rights User User

proc testScanResults {} {
    set result [eval peak data]
    set l [split $result ,]
    if { abs([lindex $l 0] - 5.3) > .1} {
	error "Bad peak position"
    }
    if { abs([lindex $l 1] - 1.4) > .3} {
	error "Bad peak FWHM"
    }
    if { abs([lindex $l 2] - 288) > 7} {
	error "Bad peak Value"
    }
}

test scan-1.0 {Test Normal Scan} -body {
    testOK "xxxscan clear"
    testOK "xxxscan add a4 2. .2"
    set result [eval xxxscan run 30 timer 2]
    if {[string first ERROR $result] >= 0} {
	set idx [string first ERROR $result]
	set errText [string range  $result $idx end]
	error "ERROR occurred during scan: $errText"
    } 
    testScanResults 
    return OK
} -result OK

test scan-1.1 {Test cscan} -body {
    set result [eval cscan a4 5.3 .2 15 3]
    if {[string first ERROR $result] >= 0} {
	set idx [string first ERROR $result]
	set errText [string range  $result $idx end]
	error "ERROR occurred during scan: $errText"
    } 
    testScanResults 
    return OK
} -result OK

test scan-1.2 {Test sscan} -body {
    set result [eval sscan a4 2 7 30 2]
    if {[string first ERROR $result] >= 0} {
	set idx [string first ERROR $result]
	set errText [string range  $result $idx end]
	error "ERROR occurred during scan: $errText"
    } 
    testScanResults 
    return OK
} -result OK

test scan-1.3 {Test scan interest} -body {
    testOK "xxxscan interest"
    set result [eval xxxscan run 3 timer 2]
    if {[string first ERROR $result] >= 0} {
	set idx [string first ERROR $result]
	set errText [string range  $result $idx end]
	error "ERROR occurred during scan: $errText"
    } 
    if {[string first NewScan $result] < 0} {
	error " NewScan missing"
    }
    if {[string first scan.Counts $result] < 0} {
	error "count data missing"
    }
    if {[string first ScanEnd $result] < 0} {
	error "ScanEnd missing"
    }
    xxxscan uninterest
    return OK
} -result OK

test scan-1.3.1 {Test scan uuinterest} -body {
    testOK "xxxscan uuinterest"
    set result [eval xxxscan run 3 timer 2]
    if {[string first ERROR $result] >= 0} {
	set idx [string first ERROR $result]
	set errText [string range  $result $idx end]
	error "ERROR occurred during scan: $errText"
    } 
    if {[string first NewScan $result] < 0} {
	error " NewScan missing"
    }
    if {[string first ScanData $result] < 0} {
	error "count data missing"
    }
    if {[string first ScanEnd $result] < 0} {
	error "ScanEnd missing"
    }
    xxxscan uninterest
    return OK
} -result OK

test scan-1.3.2 {Test scan dyninterest} -body {
    testOK "xxxscan dyninterest"
    set result [eval xxxscan run 3 timer 2]
    if {[string first ERROR $result] >= 0} {
	set idx [string first ERROR $result]
	set errText [string range  $result $idx end]
	error "ERROR occurred during scan: $errText"
    } 
    if {[string first NewScan $result] < 0} {
	error " NewScan missing"
    }
    if {[string first xxxscan.scanpoint $result] < 0} {
	error "scan point data missing"
    }
    if {[string first ScanEnd $result] < 0} {
	error "ScanEnd missing"
    }
    xxxscan uninterest
    return OK
} -result OK

test scan-1.4 {Test scan log} -body {
    testOK "xxxscan log brumm"
    return OK
} -result OK

test scan-1.5 {Test scan getfile} -body {
    testCommand "xxxscan getfile" scan.File
} -result OK

test scan-1.6 {Test scan getcounts} -body {
    testCommand "xxxscan getcounts" xxxscan.Counts
} -result OK

test scan-1.7 {Test scan getmonitor } -body {
    testCommand "xxxscan getmonitor 1" xxxscan.mon01
} -result OK

test scan-1.8 {Test scan gettimes } -body {
    testCommand "xxxscan gettimes" xxxscan.scantimes
} -result OK

test scan-1.9 {Test scan np } -body {
    testCommand "xxxscan np" xxxscan.nP
} -result OK

test scan-1.10 {Test scan getvardata } -body {
    testCommand "xxxscan getvardata 0" scan.a4
} -result OK

test scan-1.11 {Test scan noscanvar } -body {
    testCommand "xxxscan noscanvar" xxxscan.noscanvar
} -result OK

test scan-1.12 {Test scan getvarpar} -body {
    testCommand "xxxscan getvarpar 0" xxxscan.a4
} -result OK

test scan-1.13 {Test scan aborting} -body {
    exec interrupt.tcl &
    set result [eval cscan a4 5.3 .2 15 3]
    if {[string first "Scan aborted" $result] < 0} {
	error "Scan did not interrupt!"
    } 
    return OK
} -result OK



