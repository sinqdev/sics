#---------------------------------------------------------------------------
# Regression tests for the SICS nxscript module.
#
# Mark Koennecke, November 2006
#---------------------------------------------------------------------------

puts stdout "Testing NXScript"

test nxscript-1.0 {Test opening file} -body {
    config rights Spy Spy 
    testCommand "nxscript createxml test.xml test.dic" ERROR
    config rights User User
    testOK "nxscript createxml test.xml test.dic"
} -result OK

test nxscript-1.1 {Write text} -body {
    testNoError "nxscript puttext text Hugo ist eine Nassnase"
} -result OK

test nxscript-1.2 {Write float} -body {
    testNoError "nxscript putfloat testfloat 27.8"
} -result OK

test nxscript-1.3 {Write int} -body {
    testNoError "nxscript putint testint 177"
} -result OK

drive a4 15
a4 softzer0 1.

test nxscript-1.4 {Write motor} -body {
    testNoError "nxscript putmot testmot a4"
} -result OK

aba count 10
test nxscript-1.5 {Write counter} -body {
    testNoError "nxscript putcounter testcter aba"
} -result OK

hm initval 55
test nxscript-1.6 {Write HM} -body {
    testNoError "nxscript puthm testhm hm"
} -result OK

config rights Mugger Mugger
tof genbin 500 300 20
tof init
config rights User User

test nxscript-1.7 {Write time binning} -body {
    testNoError "nxscript puttimebinning testhmtb tof"
} -result OK


test nxscript-1.8 {Write array } -body {
    makearray
    set t [nxscript putarray testar ar 10]
    if {[string first ERROR $t] >= 0 || [string first WARNING $t] >= 0} {
	error "Failed to write array with: $t"
    }	
    return OK
} -result OK

test nxscript-1.9 {Write int array } -body {
    makeintarray
    set t [nxscript putintarray testintar ar 10]
    if {[string first ERROR $t] >= 0 || [string first WARNING $t] >= 0} {
	error "Failed to write intarray with: $t"
    }	
   return OK
} -result OK

test nxscript-1.10 {Write global } -body {
    testNoError "nxscript putglobal Instrument Washmaschine"
} -result OK

test nxscript-1.11 {Write attribute } -body {
    testNoError "nxscript putattribute testhm signal 1"
} -result OK

test nxscript-1.12 {Writing link } -body {
    testNoError "nxscript makelink testlink text"
} -result OK

test nxscript-1.13 {Writing sicsdata } -body {
    hm initval 23
    data clear
    data copyhm 0 hm
    testNoError "nxscript putsicsdata testsd data"
} -result OK

test nxscript-1.14 {Writing slabs } -body {
    data clear
    data putfloat 0 1.1
    testNoError "nxscript putslab testslab [list 0] [list 1] data"
    data putfloat 0 2.2
    testNoError "nxscript putslab testslab [list 1] [list 1] data"
    data putfloat 0 3.3
    testNoError "nxscript putslab testslab [list 2] [list 1] data"
} -result OK


test nxscript-1.20 {Close file} -body {
    testOK "nxscript close"
} -result OK

test nxscript-1.21 {Testing file content } -body {
    set status [catch {exec diff --ignore-matching-lines=file_time test.xml testsoll.xml} msg]
    if {$status != 0} {
	error "Difference in NXSCRIP-XML file: $msg"
    }
    return OK
} -result OK



 