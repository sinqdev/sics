#------------------------------------------------------------------
# This is a helper file in order to debug SICS Tcl scripts. The idea
# is that a connection to a SICS interpreter at localhost:2911 is opened.
# Then unknown is reimplemented to send unknown commands (which must be
# SICS commands) to the SICS interpreter for evaluation. This is done
# with transact in order to figure out when SICS finished processing.
# Thus is should be possible to debug SICS Tcl scripts in a normal
# standalone interpreter without the overhead of restarting SICS
# all the time. It may even be possible to use one of the normal
# Tcl debuggers then....
#
# Mark Koennecke, February 2006
#------------------------------------------------------------------

set socke [socket localhost 2911]
gets $socke
puts $socke "Spy Spy"
flush $socke
gets $socke
#------------------------------------------------------------------
proc unknown args {
	global socke
	append com "transact " [join $args]
	puts $socke $com
	flush $socke
	set reply ""
	while {1} {
		set line [gets $socke]
		if {[string first TRANSACTIONFINISHED $line] >= 0} {
			return $reply
		} else {
			append reply $line "\n"
		}
	}
}
#------------------------------------------------------------------
proc clientput args {
	puts stdout [join $args]
}
#------------------------------------------------------------------
