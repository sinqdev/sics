#-------------------------------------------------------------------------
# This is a regression test for the SICS data module
#
# Mark Koennecke, November 2006
#-------------------------------------------------------------------------
puts stdout "Testing SicsData"
data clear

test sicsdata-1.0 {Test writing int} -body {
    config rights User User
    for {set i 0} {$i < 5} { incr i} {
	testOK "data putint $i $i"
    }
    for {set i 0} {$i < 5} { incr i} {
	set val [SICSValue "data get $i"]
	if {$val != $i} {
	    error "SicsData returned a bad value: expected $i received $val" 
	}
    }
    return OK
} -result OK

test sicsdata-1.1 {Test writing float} -body {
    for {set i 0} {$i < 5} { incr i} {
	set v [expr $i * 1.0]
	testOK "data putfloat $i $v"
    }
    for {set i 0} {$i < 5} { incr i} {
	set val [SICSValue "data get $i"]
	if {abs($val - $i) > .000001} {
	    error "SicsData returned a bad value: expected $i received $val" 
	}
    }
    return OK
} -result OK

test sicsdata-1.2 {Test used} -body {
    set val [SICSValue "data used"]
    if {$val != 5} {
	error "Expected data used to be 5, not $val"
    }
    return OK
} -result OK

test sicsdata-1.3 {Test clear} -body {
    testOK "data clear"
    set val [SICSValue "data used"]
    if {$val != 0} {
	error "Expected data used to be 0, not $val"
    }
    return OK
} -result OK

xxxscan clear
xxxscan add a4 2. .2
xxxscan run 30 timer 2

test sicsdata-1.4 {Testing scancounts} -body {
    testOK "data copyscancounts 0 xxxscan"
    set val [SICSValue "data used"]
    if {$val != 30} {
	error "Expected data used to be 30, not $val"
    }
    set val [SICSValue "data get 0"]
    if {$val != 10} {
	error "Expected data 0 to be 10, not $val"
    }
    set val [SICSValue "data get 10"]
    if {$val != 41} {
	error "Expected data 10 to be 41, not $val"
    }
    set val [SICSValue "data get 20"]
    if {$val != 171} {
	error "Expected data 10 to be 171, not $val"
    }
    return OK
} -result OK

test sicsdata-1.5 {Testing scanmonitor} -body {
    testOK "data clear"
    testOK "data copyscanmon 0 xxxscan 2"
    set val [SICSValue "data used"]
    if {$val != 30} {
	error "Expected data used to be 30, not $val"
    }
    set val [SICSValue "data get 0"]
    if {$val != 0} {
	error "Expected data 0 to be 0, not $val"
    }
    return OK
} -result OK

test sicsdata-1.6 {Testing scanvar} -body {
    testOK "data copyscanvar 0 xxxscan 0"
    set val [SICSValue "data used"]
    if {$val != 30} {
	error "Expected data used to be 30, not $val"
    }
    set val [SICSValue "data get 0"]
    if {abs($val -  2.0) > .001} {
	error "Expected data 0 to be 2.0, not $val"
    }
    set val [SICSValue "data get 20"]
    if {abs($val -  6.0) > .001} {
	error "Expected data 20 to be 6.0, not $val"
    }
    set val [SICSValue "data get 29"]
    if {abs($val -  7.8) > .001} {
	error "Expected data 29 to be 7.8, not $val"
    }
    return OK
} -result OK

config rights Mugger Mugger
tof genbin 20 10 50
tof init

test sicsdata-1.7 {Testing timebin} -body {
    testOK "data clear"
    testOK "data copytimebin 0 tof"
    set val [SICSValue "data used"]
    if {$val != 50} {
	error "Expected data used to be 50, not $val"
    }
    set val [SICSValue "data get 0"]
    if {abs($val -  20.0) > .001} {
	error "Expected data 0 to be 20.0, not $val"
    }
    set val [SICSValue "data get 49"]
    if {abs($val -  510.0) > .001} {
	error "Expected data 49 to be 510.0, not $val"
    }
    return OK
} -result OK

hm initval 32

test sicsdata-1.8 {Testing hm} -body {
    testOK "data clear"
    testOK "data copyhm 0 hm"
    set val [SICSValue "data used"]
    if {$val != 23} {
	error "Expected data used to be 23, not $val"
    }
    set val [SICSValue "data get 0"]
    if {abs($val -  32.0) > .001} {
	error "Expected data 0 to be 32.0, not $val"
    }
    set val [SICSValue "data get 22"]
    if {abs($val -  32.0) > .001} {
	error "Expected data 22 to be 32.0, not $val"
    }
    return OK
} -result OK

test sicsdata-1.8 {Testing UU write} -body {
    set text [data writeuu hugo]
    if {[string first "begin 622" $text] < 0} {
	error "Bad reply on uuwrite: $text"
    }
    return OK
} -result OK

test sicsdata-1.9 {Testing file dump} -body {
    data clear
    data copyhm 0 hm
    testOK "data dumpxy test.dat"
    set status [catch {exec diff test.dat sicsdatasoll.dat} msg]
    if {$status != 0} {
	error "Difference in dump file: $msg"
    }
    return OK
} -result OK

test sicsdata-1.10 {Copying sicsdata} -body {
    duta clear
    data clear
    data copyhm 0 hm
    testNoError "duta copydata 0 data 0 23"
    set val [SICSValue "duta used"]
    if {$val != 23} {
	error "Expected data used to be 23, not $val"
    }
    for {set i 0} {$i < 23} {incr i} {
	set val [SICSValue "duta get $"]
	if {abs($val -  32.0) > .001} {
	    error "Expected data $i to be 32.0, not $val"
	}
    }
    return OK
} -result OK

test sicsdata-1.11 {Division} -body {

    config rights Mugger Mugger
    duta clear
    data clear
    hm initval 32
    data copyhm 0 hm
    hm initval 16
    duta copyhm 0 hm
    testNoError "data divideby duta"
    set val [SICSValue "data used"]
    if {$val != 23} {
	error "Expected data used to be 23, not $val"
    }
    for {set i 0} {$i < 23} {incr i} {
	set val [SICSValue "data get $"]
	if {abs($val -  2.0) > .001} {
	    error "Expected data $i to be 2.0, not $val"
	}
    }
    return OK
} -result OK




