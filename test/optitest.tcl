#--------------------------------------------------------------------
# This file contaisn test for the peak optimization routines in SICS
#
# Mark Koennecke, October 2006
#-------------------------------------------------------------------
clientput "Testing optimization routines...."

config rights Mugger Mugger
aba setpar errortype 1 0
config rights User User

test opt-1.0 {Test Normal Optimizer} -body {
    testOK "opti clear"
    testOK "opti addvar a4 .3 20 .3"
    testOK "opti preset 2"
    testOK "opti countmode timer"
    drive a4 4.
    set result [eval opti run]
    if { [string first ERROR $result] > 0} {
        puts stdout $result
	error "Error occurred during optimization run"
    }
    if {[string first finished $result] < 0} {
	error "Optimization did not finish normally"
    }
    set val [SICSValue a4]
    if {abs($val - 5.3) > .1} {
	error "Optimisation did not arrive at desired position"
    }
    return OK
} -result OK

test opt-1.1 {Test Normal Optimizer Aborting} -body {
    testOK "opti clear"
    testOK "opti addvar a4 .3 20 .3"
    testOK "opti preset 2"
    testOK "opti countmode timer"
    drive a4 4.
    exec interrupt.tcl &
    set result [eval opti run]
    if {[string first "Scan aborted" $result] < 0} {
	error "Optimiser did not abort
    }
    set result [status]
    if { [string first Eager $result] < 0} {
        puts stdout $result
	error "Optimiser did not interrupt!"
    }
    return OK
} -result OK

test opt-1.2 {Test Climbing } -body {
    testOK "opti clear"
    testOK "opti addvar a4 .3 20 .5"
    testOK "opti preset 2"
    testOK "opti countmode timer"
    drive a4 4.
    set result [eval opti climb]
    if { [string first ERROR $result] > 0} {
        puts stdout $result
	error "Error occurred during optimization run"
    }
    if {[string first finished $result] < 0} {
	error "Optimization did not finish normally"
    }
    set val [SICSValue a4]
    if {abs($val - 5.3) > .5} {
	error "Optimisation did not arrive at desired position"
    }
    return OK
} -result OK

test opt-1.3 {Test Climbing } -body {
    testOK "opti clear"
    testOK "opti addvar a4 .3 20 .5"
    testOK "opti preset 2"
    testOK "opti countmode timer"
    drive a4 6.
    set result [eval opti climb]
    if { [string first ERROR $result] > 0} {
        puts stdout $result
	error "Error occurred during optimization run"
    }
    if {[string first finished $result] < 0} {
	error "Optimization did not finish normally"
    }
    set val [SICSValue a4]
    if {abs($val - 5.3) > .5} {
	error "Optimisation did not arrive at desired position"
    }
    return OK
} -result OK

test opt-1.4 {Test Climbing Interruption} -body {
    testOK "opti clear"
    testOK "opti addvar a4 .3 20 .5"
    testOK "opti preset 2"
    testOK "opti countmode timer"
    drive a4 4.
    exec interrupt.tcl &
    set result [eval opti climb]
    if {[string first "Scan was aborted" $result] < 0} {
	error "Optimiser did not abort"
    }
    set result [status]
    if { [string first Eager $result] < 0} {
        puts stdout $result
	error "Optimiser did not interrupt!"
    }
    return OK
} -result OK


test opt-1.5 {Test Maximizer} -body {
    drive a4 4.
    set result [eval max a4 .2 timer 2]
    if { [string first ERROR $result] > 0} {
        puts stdout $result
	error "Error occurred during maximization"
    }
    if {[string first Found $result] < 0} {
	error "Optimization did not finish normally"
    }
    set val [SICSValue a4]
    if {abs($val - 5.3) > .1} {
	error "Maximization did not arrive at desired position"
    }
    return OK
} -result OK

test opt-1.6 {Test Maximizer} -body {
    drive a4 6.
    set result [eval max a4 .2 timer 2]
    if { [string first ERROR $result] > 0} {
        puts stdout $result
	error "Error occurred during maximization"
    }
    if {[string first Found $result] < 0} {
	error "Optimization did not finish normally"
    }
    set val [SICSValue a4]
    if {abs($val - 5.3) > .1} {
	error "Maximization did not arrive at desired position"
    }
    return OK
} -result OK

test opt-1.6 {Test Maximizer Aborting} -body {
    drive a4 6.
    exec interrupt.tcl &
    set result [eval max a4 .2 timer 2]
    if { [string first "Full Stop" $result] < 0} {
        puts stdout $result
	error "Maximize did not interrupt!"
    }
    set result [status]
    if { [string first Eager $result] < 0} {
        puts stdout $result
	error "Maximize did not interrupt!"
    }
    return OK
} -result OK

test opt-1.7 {Test Maximizer Parameter} -body {
    testOK "max in360 1"
    testOK "max maxpts 50"
    testCommand "max in360" max.in360
    testCommand "max maxpts" max.maxpts
    return OK
} -result OK

