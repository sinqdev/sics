#!/usr/bin/tclsh
#------------------------------------------------------------
# This is a little script which issues an interrupt to SICS
# after five seconds
#
# Mark Koennecke, October 2006
#------------------------------------------------------------
source sicstcldebug.tcl
config rights Mugger Mugger
exec sleep 5
puts $socke "INT1712 3"
exit 1

 