#--------------------------------------------------------------------
# This is for testing odd bits and pieces
#
# Mark Koennecke, October 2006
#--------------------------------------------------------------------

puts stdout "Testing variables and aliases"

test misc-1.0 {Test Variables} -body {
        testPar lotte Uuuuuurgs User 
	return OK
} -result OK

test misc-1.1 {Test Alias} -body {
    config rights User User
    miau errortype 0
    testDrive miau 10 User
    return OK
} -result OK


