#-------------------------------------------------------------
# Testing of the counter module
#
# The regression counter has various errortypes which can be simulated:
#  0   = none
#  1   = failed start
#  2   = status failure
#  3   = pause fail
#  4   = continue fail
#  5   = failed read
#
# Another parameter is recover which causes the problem to go away 
# when 1
#
# TODO: What shall happen when pausing fails? Currently it continues 
# counting. This may be exactly what we need, but????
#
# This code needs the counter name two times: once as countername and 
# as errorname. The purpose is that this module may be used for testing
# both the real and the multi counter. 
#
# Mark Koennecke, September 2006
#-------------------------------------------------------------
#set countername aba
#set errorname aba

puts stdout "Testing counter: $countername"

#---------------------------------------------------------------
config rights Mugger Mugger
$errorname setpar errortype 1 0

test counter-1.0 {Test Mode Setting} -body {
	config rights Spy Spy
        set res [eval $countername setmode monitor]
	if {[string first ERROR $res] < 0} {
		error "Managed to set parameter even if not allowed"
	}
	config rights User User
	set res [eval $countername setmode monitor]
	if {[string first ERROR $res] >= 0} {
		error "Setting parameter failed with $res"
	}
	set readback [SICSValue "$countername getmode"]
        compareValue [string tolower $readback] monitor
	config rights Spy Spy
        set res [eval $countername setmode timer]
	if {[string first ERROR $res] < 0} {
		error "Managed to set parameter even if not allowed"
	}
	config rights User User
	set res [eval $countername setmode timer]
	if {[string first ERROR $res] >= 0} {
		error "Setting parameter failed with $res"
	}
	set readback [SICSValue "$countername getmode"]
        compareValue [string tolower $readback] timer
	return "OK"
} -result OK
#-------------------------------------------------------------------
test counter-1.1 {Test Preset Setting} -body {
	config rights Spy Spy
        set val 12
        set res [eval $countername setpreset $val]
	if {[string first ERROR $res] < 0} {
		error "Managed to set parameter even if not allowed"
	}
	config rights User User
	set res [eval $countername setpreset $val]
	if {[string first ERROR $res] >= 0} {
		error "Setting parameter failed with $res"
	}
	set readback [SICSValue "$countername getpreset"]
        compareValue $readback $val
	return "OK"
} -result OK
#---------------------------------------------------------------------
test counter-1.3 {Test Normal Counting} -body {
    config rights Spy Spy 
    set status [catch {testNBCounting "$countername countnb 10" 11} msg]
    if {$status == 0} {
	error "Counted in spite of lacking privilege"
    }
    config rights User User
    testNBCounting "$countername countnb 10" 11
} -result OK
#---------------------------------------------------------------------
test counter-1.4 {Test Blocking  Counting} -body {
    config rights Spy Spy 
    set status [catch {testBlockCounting "$countername countnb 10" 11} msg]
    if {$status == 0} {
	error "Counted in spite of lacking privilege"
    }
    config rights User User
    testBlockCounting "$countername countnb 10" 11
} -result OK
#--------------------------------------------------------------------
test counter-1.5 {Interrupted Counting} -body {
    testInterruptedCount "$countername countnb 100"
} -result OK
#--------------------------------------------------------------------
config rights User User

test counter-1.51 {Pause Counting Test} -body {
    global socke
    $countername countnb 300
    exec sleep 1
    set ans [status]
    if {[string first Counting $ans] < 0} {
	error "Failed to start counting: $ans"
    }
    pause
    exec sleep 1
    set ans [status]
    if {[string first Paus $ans] < 0} {
	error "Failed to pause counting: $ans"
    }
    puts $socke continue
    flush $socke
    exec sleep 1
    set ans [status]
    if {[string first Count $ans] < 0} {
	error "Failed to continue counting: $ans"
    }
    puts $socke "INT1712 3"
    flush $socke
    set ans [status]
    return OK
} -result OK
#---------------------------------------------------------
test counter-1.52 {Pause Interrupt Test} -body {
    global socke
    $countername countnb 300
    exec sleep 2
    set ans [status]
    if {[string first Counting $ans] < 0} {
	error "Failed to start counting: $ans"
    }
    pause
    exec sleep 1
    set ans [status]
    if {[string first Paus $ans] < 0} {
	error "Failed to pause counting: $ans"
    }
    puts $socke "INT1712 3"
    flush $socke
    set ans [status]
    if {[string first Eager $ans] < 0} {
	error "Failed to interrupt paused counting: $ans"
    }
    return OK
} -result OK
#-------------------------------------------------------------------
test counter-1.53 {Counter Value Read Test} -body {
    config rights User User
    $countername count 10
    set ans [SICSValue "$countername gettime"]
    compareValue $ans 10
    set ans [SICSValue "$countername getcounts"]
    compareValue $ans 5
    set ans [SICSValue "$countername getmonitor 1"]
    compareValue $ans 10
    set ans [SICSValue "$countername getmonitor 2"]
    compareValue $ans 25
    set ans [SICSValue "$countername getmonitor 3"]
    compareValue $ans 35
    set ans [SICSValue "$countername getmonitor 4"]
    compareValue $ans 45
    set ans [SICSValue "$countername getmonitor 5"]
    compareValue $ans 55
    set ans [SICSValue "$countername getmonitor 6"]
    compareValue $ans 65
    return OK
} -result OK
#--------------------------------------------------------------------
config rights Mugger Mugger
$errorname setpar errortype 1 1
$errorname setpar recover 1 0

test counter-1.6 {Counting Start Failure} -body {
    set ans [$countername countnb 100]
    if { [string first "Counting aborted" $ans] < 0} {
	error "Failed to trigger count start failure: $ans"
    }
    return OK
} -result OK
#---------------------------------------------------------------
$errorname setpar errortype 1 1
$errorname setpar recover 1 1

test counter-1.7 {Counting Start Failure with Recovery} -body {
    set ans [$countername countnb 10]
    if { [string first "WARNING" $ans] < 0} {
	error "Failed to trigger count start failure: $ans"
    }
    set ans [SICSValue status]
    if {[string first Counting $ans] < 0} {
	error "Did not recover from start failure"
    }
    exec sleep 12
    set ans [SICSValue status]
    if {[string first Eager $ans] < 0} {
	error "Did not stop counting after start failure"
    }
    return OK
} -result OK
#----------------------------------------------------------------------
$errorname setpar errortype 1 2
$errorname setpar recover 1 0

test counter-1.8 {Counting Status Failure} -body {
    set ans [$countername countnb 100]
    set ans [status]
    if { [string first "Full Stop called" $ans] < 0} {
	error "Failed to trigger count start failure: $ans"
    }
    return OK
} -result OK
#---------------------------------------------------------------
$errorname setpar errortype 1 2
$errorname setpar recover 1 1

test counter-1.9 {Counting Status Failure with Recovery} -body {
    set ans [$countername countnb 10]
    set ans [status]
    if { [string first "WARNING" $ans] < 0} {
	error "Failed to trigger count start failure: $ans"
    }
    if {[string first Counting $ans] < 0} {
	error "Did not recover from status failure"
    }
    exec sleep 12
    set ans [SICSValue status]
    if {[string first Eager $ans] < 0} {
	error "Did not stop counting after status failure"
    }
    return OK
} -result OK
#-------------------------------------------------------------------
$errorname setpar errortype 1 5
$errorname setpar recover 1 0

test counter-1.10 {Counter Read Failure} -body {
    set ans [$countername count 2]
    if { [string first "Full Stop" $ans] < 0} {
	error "Failed to trigger count read failure: $ans"
    }
    set ans [SICSValue status]
    if {[string first Eager $ans] < 0} {
	error "Did not stop counting after read failure"
    }
    return OK
} -result OK

#----------------------------------------------------------------
$errorname setpar errortype 1 5
$errorname setpar recover 1 1

test counter-1.10 {Counter Read Recover} -body {
    set ans [$countername count 2]
    if { [string first "WARN" $ans] < 0} {
	error "Failed to trigger count read failure: $ans"
    }
    set ans [SICSValue status]
    if {[string first Eager $ans] < 0} {
	error "Did not stop counting after read failure"
    }
    return OK
} -result OK


