
#line 271 "velo.w"

/*--------------------------------------------------------------------------

  Internal header file describing the velocity selector data structure.

  Mark Koennecke, Juli, 1997

----------------------------------------------------------------------------*/
#ifndef VELOINTERNAL
#define VELOINTERNAL

#line 54 "velo.w"

/*------------------ The velocity selector datastructure -----------------*/ 
 
     typedef struct __VelSel {
                        pObjectDescriptor  pDes;
                        char *pName;
                        pIDrivable         pDrivInt;
                        pICallBack         pCall;
                        int                iForbidden;
                        ObPar              *pPar;
                        pMotor             pTilt;
                        float              fRot;
                        float              fTilt;
                        pEVControl         pMonitor;
                        pVelSelDriv        pDriv;
                      } VelSel;  
/*----------------- Forbidden region single item -------------------------*/
    typedef struct __Verbot {
                       float fMin;
                       float fMax;
                    } Verbot, *pVerbot;


#line 281 "velo.w"

#define VELOREDO  2
#define VELOFAIL  0
#define VELOOK    1

#define VSNOCON 0
#define VSOK    1
#define VSACCEL -7
#define VSFAIL -2

#endif

