/*--------------------------------------------------------------------------

	iFile: A simple tool for configuration management. Reads a list
	of name value pairs from a file and allows to seach this list
	for configuration options.
	
	
	Mark Koennecke, October 1996
	
	copyright: see Implementation file
---------------------------------------------------------------------------*/
#ifndef MKIFILE
#define MKIFILE
#include <stdio.h>

typedef struct __IFileE {
  char *name;
  char *value;
  struct __IFileE *pNext;
} IPair;

IPair *IFReadConfigFile(FILE * fp);
char *IFindOption(IPair * pList, char *name);
         /* returns a value for a name
          */
IPair *IFAddOption(IPair * pList, char *name, char *value);
IPair *IFSetOption(IPair * pList, char *name, char *value);

int IFSaveOptions(IPair * pList, FILE * fp);
void IFDeleteOptions(IPair * pList);

extern IPair *pSICSOptions;
#endif
