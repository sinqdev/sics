
/*-----------------------------------------------------------------------
  Automatic update of NeXus files a scheduled time intervalls. 
  For more information see nxudpate.tex.

  Internal, automatically generated file, do not modify. Modify in 
  nxudpate.w

  copyright: see file COPYRIGHT

  Mark Koennecke, December 2003
----------------------------------------------------------------------*/

        typedef struct __NXUPDATE {
        pObjectDescriptor pDes;
        char *startScript;
        char *updateScript;
        char *linkScript;
        int updateIntervall;
        time_t nextUpdate;
        int iEnd;
        SConnection *pCon;
        int onOff;
}NXupdate, *pNXupdate;


