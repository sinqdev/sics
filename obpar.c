/*-----------------------------------------------------------------------
  A helper class for Sics-objects internal parameter management
  
  
  

	Mark Koennecke,    November 1996

	Copyright:

	Labor fuer Neutronenstreuung
	Paul Scherrer Institut
	CH-5423 Villigen-PSI


        The authors hereby grant permission to use, copy, modify, distribute,
        and license this software and its documentation for any purpose, provided
        that existing copyright notices are retained in all copies and that this
        notice is included verbatim in any distributions. No written agreement,
        license, or royalty fee is required for any of the authorized uses.
        Modifications to this software may be copyrighted by their authors
        and need not follow the licensing terms described here, provided that
        the new terms are clearly indicated on the first page of each file where
        they apply.

        IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
        FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
        ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
        DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
        POSSIBILITY OF SUCH DAMAGE.

        THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
        INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
        IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
        NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
        MODIFICATIONS.
---------------------------------------------------------------------------*/
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "sics.h"
#include "fortify.h"
#include "conman.h"
#include "status.h"
#include "obpar.h"
#include "devexec.h"

/*------------------------------------------------------------------------*/
int ObParLength(ObPar * self)
{
  int i = 0;

  assert(self);

  while (self[i].iCode != -100) {
    i++;
  }
  return i;
}

/*-------------------------------------------------------------------------*/
ObPar *ObParCreate(int iArrayLong)
{
  ObPar *pRes = NULL;
  int i;

  assert(iArrayLong > 0);

  /* allocate memory */
  pRes = (ObPar *) malloc((iArrayLong + 1) * sizeof(ObPar));
  if (!pRes) {
    return NULL;
  }

  /* initialise all to 0 */
  for (i = 0; i < iArrayLong; i++) {
    pRes[i].name = NULL;
    pRes[i].fVal = .0;
    pRes[i].iCode = usSpy;
  }

  /* have a sentinel at the end */
  pRes[iArrayLong].iCode = -100;

  return pRes;
}

/*--------------------------------------------------------------------------*/
void ObParDelete(ObPar * self)
{
  int i;
  int iLong;

  assert(self);

  /* free the names */
  iLong = ObParLength(self);
  for (i = 0; i < iLong; i++) {
    if (self[i].name)
      free(self[i].name);
  }

  free(self);
}

/*---------------------------------------------------------------------------*/
ObPar *ObParFind(ObPar * self, char *name)
{
  int i;

  assert(self);

  for (i = 0; self[i].iCode != -100; i++) {
    if (strcmp(name, self[i].name) == 0) {
      return &self[i];
    }
  }

  return NULL;
}

/*---------------------------------------------------------------------------*/
int ObParIndex(ObPar * self, char *name)
{
  int i;

  assert(self);

  for (i = 0; self[i].iCode != -100; i++) {
    if (strcmp(name, self[i].name) == 0) {
      return i;
    }
  }

  return -1;
}

/*---------------------------------------------------------------------------*/
int ObParInit(ObPar * self, int i, char *name, float fVal, int iCode)
{

  assert(self);

  /* check i */
  if ((i < 0) && (i >= ObParLength(self))) {
    return 0;
  }

  if (self[i].name) {
    free(self[i].name);
  }
  self[i].name = strdup(name);
  self[i].fVal = fVal;
  self[i].iCode = iCode;
  return 1;
}

/*------------------------------------------------------------------------*/
int ObParSet(ObPar * self, char *obname, char *name, float fVal,
             SConnection * pCon)
{
  char pBueffel[512];
  ObPar *pPar = NULL;
  Status eStat;

  assert(self);

  /* find the parameter */
  pPar = ObParFind(self, name);
  if (pPar == NULL) {
    snprintf(pBueffel,sizeof(pBueffel)-1, "ERROR: %s.%s parameter not found", obname, name);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }

  /* are we running? */
  if(DevExecLevelRunning(pServ->pExecutor, RUNDRIVE)){
    snprintf(pBueffel,sizeof(pBueffel)-1,
        "ERROR: Cannot change %s.%s parameter while running",
            obname, name);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }

  /* check permission */
  if (!SCMatchRights(pCon, pPar->iCode)) {
    snprintf(pBueffel,sizeof(pBueffel)-1, "ERROR: Insufficient privilege to change %s.%s",
            obname, name);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }

  /* passed all tests: do It! */
  pPar->fVal = fVal;
  return 1;
}

/*------------------------------------------------------------------------*/


float ObVal(ObPar * self, int i)
{
  assert(self);
  assert(i > -1);
  assert(i < ObParLength(self));

  return self[i].fVal;
}
