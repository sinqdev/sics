
/*--------------------------------------------------------------------------
                   S T A N D A R D S C A N
                    
  This is a library of scan functions for the SICS standard scan.           

  copyright: see copyright.h

  Extracted from scan.c: Mark Koennecke, November 2004 
----------------------------------------------------------------------------*/
#ifndef SICSSTDSCAN
#define SICSSTDSCAN

  /**
    * make a filename according to SICS rules for this scan
    */
char *ScanMakeFileName(SicsInterp * pSics, SConnection * pCon);
  /*
   * write the header bits from the template
   */
void WriteTemplate(FILE * fd, FILE * temp, char *filename, pScanData pScan,
                   SConnection * pCon, SicsInterp * pSics);
  /**
    * write the header of the scan file
    */
int WriteHeader(pScanData self);
  /**
    * WriteScanPoints is called for each point to write the scan data
    * to screen and to file.
    */
int WriteScanPoints(pScanData self, int iPoint);
  /**
    * Called before the scan to prepare. The default implementation
    * checks if all scan positions are available and configures the
    * counter.
    */
int PrepareScan(pScanData self);
  /**
        * allocate a new data file
        */
int prepareDataFile(pScanData self);
  /**
    * second version of PrepareScan which does not check scan limits
    */
int NonCheckPrepare(pScanData self);
  /**
    * prepare for a scan without complaining...
    */
int SilentPrepare(pScanData self);
  /**
    * ScanDrive handles driving to the scan point iPoint.
    */
int ScanDrive(pScanData self, int iPoint);
  /**
    * ScanFastDrive starts driving to the scan point iPoint, but
    * does not wait. Use this for implementing slightly faster
    * scans.
    */
int ScanFastDrive(pScanData self, int iPoint);
  /**
    * ScanCount is called at each scan step to do the counting.
    */
int ScanCount(pScanData self, int iPoint);
  /**
    * CollectScanData stores the scans count results into 
    * the scan data structure and prints the information about the
    * scan progress.
    */
int CollectScanData(pScanData self, int iPoint);
int CollectScanDataJochen(pScanData self, int iPoint);
int CollectSilent(pScanData self, int iPoint);
/*===================================================================*/
  /**
    * Script invocation for writing the scan header.
    */
int ScriptWriteHeader(pScanData self);
  /**
    * Script writing each scan point
    */
int ScriptWriteScanPoints(pScanData self, int iPoint);
  /**
    * Script preparation of the scan.
    */
int ScriptPrepareScan(pScanData self);
  /**
    * Script driving to a scan point
    */
int ScriptScanDrive(pScanData self, int iPoint);
  /**
    * Script counting a scan point
    */
int ScriptScanCount(pScanData self, int iPoint);
  /**
    * Script collecting scan data for each scan point
    */
int ScriptScanCollect(pScanData self, int iPoint);
  /**
    * ScriptScanFinish invokes a script after the scan has finished
    */
int ScriptScanFinish(pScanData self);
  /**
    * ConfigureScript assigns the script invocation functions for
    * scan
    */
void ConfigureScript(pScanData self);
/*=====================================================================*/
int StandardScanWrapper(SConnection * pCon, SicsInterp * pSics,
                        void *pData, int argc, char *argv[]);


#endif
