
/*------------------------------------------------------------------------
  A busy flag module for SICS. 

  Mark Koennecke, July 2002
-------------------------------------------------------------------------*/
#ifndef SICSBUSY
#define SICSBUSY


typedef struct BUSY__ *busyPtr;

busyPtr makeBusy(void);
void killBusy(void *self);

void incrementBusy(busyPtr self);
void decrementBusy(busyPtr self);
void clearBusy(busyPtr self);
void setBusy(busyPtr self, int val);

int isBusy(busyPtr self);

int BusyAction(SConnection * pCon, SicsInterp * pSics, void *pData,
               int argc, char *argv[]);

busyPtr findBusy(SicsInterp * pInter);
#endif
