\subsubsection{HMdata}
This is a data class for histogram memories. This class has the
following tasks: 
\begin{itemize}
\item Maintain the dimensions of the histogram memory.
\item Keep track of the time binning, when appropriate.
\item In many cases the histogram memory data is buffered in order to
prevent excessive access to the histogram memory through status
display clients gone mad. This task is also handled through this class.  
\end{itemize} 

In order to do this, the following data structure is needed:
@d hmdatadat @{
	typedef struct __hmdata{
			int rank;
			int iDim[MAXDIM];
			int nTimeChan;
			float timeBinning[MAXCHAN];
			int tofMode;
			time_t nextUpdate;
			int updateIntervall;
			int updateFlag;
			HistInt *localBuffer;
			struct __hmdata *timeslave; 
} HMdata, *pHMdata;			
@}

The following functions work on this data structure:
@d hmdatfunc @{
	pHMdata makeHMData(void);
	void killHMData(pHMdata self);
	void clearHMData(pHMdata self);
	
	int configureHMdata(pHMdata self, pStringDict pOpt,
				SConnection *pCon);
        int resizeBuffer(pHMdata self);
	int genTimeBinning(pHMdata self, float start, float step, 
				int noSteps);
	int setTimeBin(pHMdata self, int index, float value);

	int getNoOfTimebins(pHMdata self);
	float *getTimeBinning(pHMdata self);
	int isInTOFMode(pHMdata self);
	void clearTimeBinning(pHMdata self);

	void getHMDataDim(pHMdata self, int iDIM[MAXDIM], int *rank);
	long getHMDataLength(pHMdata self); 

	int getHMDataHistogram(pHistMem hist, SConnection *pCon,
				int bank, int start, int length,
				HistInt *lData);
	void updateHMData(pHMdata self);
	HistInt *getHMDataBufferPointer(pHistMem hist, SConnection *pCon);

	long sumHMDataRectangle(pHistMem self, SConnection *pCon, 
				int start[MAXDIM], int end[MAXDIM]);
	int loadHMData(pHMdata self, SConnection *pCon, char *filename);			
@}

\begin{description}
\item[makeHMData] allocate a new HMdata structure.
\item[killHMData] properly release all memory used by the HMdata
structure.
\item[configureHMdata] configures the HMdata from the configuration
options in pOpt.
\item[getTimeBinning] generate a equidistant time binning starting at
start. Create noSteps time bins of size step.
\item[isInTOFMode] returns true if we are in TOF mode, 0 else.
\item[setTimeBin] sets a single time bin at index to value. Used for
irregular time binnings.
\item[clearTimeBinning] removes all time binnings.
\item[getNoOfTimeBins] returns the number of time bins.
\item[getTimeBinning] returns a pointer to the time binning array. Do
not free or modify in any other form.
\item[getHMDataDim] retrieves the rank and dimensions of histogram
memory data.
\item[getHMDataLength] gets the size of the histogram memory array.
\item[getHMDataHistogram] copies histogram memory data to
lData. Dependent on the status, data is copied either from a local
buffer or retrieved from the histogram memory. lData must have been
allocated large enough to cope with all the data before this works
properly.
\item[forceHMDataUpdate] makes sure that the histogram is read from
the histogram memory and not from the buffer the next time round.
\item[getHMDataBufferPointer] returns a pointer to the internal buffer
pointer of HMdata. Use with extra care!
\item[sumHMDataRectangle] sums a rectangular box delimted by start and end
from the histogram memory.
\item[loadHMData] loads histogram memory data from a file. This is for 
 debugging purposes. The file must contain enough numbers to fill the HM.
\end{description}


@o hmdata.h @{
/*-----------------------------------------------------------------------
  This is a data handling class for histogram memory data.
  For more information see hmdata.tex.

  copyright: see file COPYRIGHT

  Mark Koennecke, January 2003
-------------------------------------------------------------------------*/
#ifndef SICSHMDATA
#define SICSHMDATA
#include "sics.h"
#include "HistMem.h"
#include "stringdict.h"
#define MAXCHAN 16834 
#define MAXDIM 3

@<hmdatadat@>

@<hmdatfunc@>

#endif

@}