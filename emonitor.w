\subsection{SICS Sample Environment  Handling}
In addition to driving motors and counting SICS has to take care of sample
environment devices as well. These can be temperature controllers, current
controllers etc. It can be assumed, that SICS has to take care of
intelligent sample environment devices only, i. e. devices which do their
own realtime processing. SICS, however, has to implement the logic to drive
an environment controller to a new value. Furthermore SICS has to do the
error handling when an environment controller fails. In order to do this the
following software components will be needed:
\begin{itemize}
\item An environment monitor. This component will register any active
environment control device. A test function will be called from the main
loop which checks for controller out of range and initiates error handling.
\item A new interface, the environment interface (see \ref{interface}).
\item Controller objects. One for each environment controller. Again, the
SICS distinction between logical objects and hardware drivers will be
maintained.
\end{itemize}

Controller objects will be able to share some code among them. Each
environemnt controller will hold a target value, a permissable tolerance,
 a selctor variable which defines the type of error handling to perform and
 an interrupt code to issue when that form of error handling is choosen.
Furthermore each environment controller needs to implement the drivable
interface (for driving to new values) and the environment interface (for
 monitoring the controller).   Controller object will also share the error
handling during the monitor phase between them. The scheme is that each controller will have a
variable which selects the error handling mechanism. Depending on this
variable an error managment routine will be invoked. Current two schemes for
handling errors are supported:
\begin{itemize}
\item Print a warning but do nothing else.
\item Interrupt: an interrupt will be issued.
\item Pause: the measurement will be paused.
\item Try to drive to a safe value. This may be used to rescue a oven
gone haywire by setting a low setpoint where the device is safe. 
\end{itemize}
This scheme allows for adding more error handling strategies when necessary
and to implement them in one place for all environment controllers. 
Consequently, there will be some some general controller code in files
evcontrol.*. In the object oriented paradigm environment controller would
inherit from this general controller. This is implemented by delegation in
SICS.

Please note, that any environment device has three modes of
operation. The first, trivial one, is idle. The second one is the driving
phase. This phase is entered when a drive or run command tries to set the
controller to a new value. Once this target has been reached a environment
controller will be registered in the environment monitor which will take
care of monitoring the controller during its operation. This is the
third stage: inMonitor. 


\subsection{The Environment Monitor}
The environment monitors (emon) task is to register all active environment
controllers. It will be called from the main loop to check if each
registered environment controller is still within tolerances. If not, emon
has to enforce error processing. Else it does nothing. 

The environment monitor uses the following datastructures:
@d emondat @{
   typedef struct __EnvMon {
                            pObjectDescriptor pDes;
                            int iList;
                            int iEnd;
                            time_t nextRun;
                            } EnvMon;
/*---------------------------------------------------------------------------*/
   typedef struct {
                    pDummy pDum;
                    char *pName;
                    pEVInterface pInter;
                  } EVEntry, *pEVEntry;     


@}
As it it will be accessible as SICS object emon needs an object descriptor.
The other data member is a list of active controllers.
 
The EVEntry structure contains the data held in the list for each active
controller. It is, disguised as pDum, a pointer to the controllers
datastructure, the name of the controller, and the environment interface of
the controller. 

The following interface is implemented by the environment monitor:
@d emonint @{

   pEnvMon CreateEnvMon(void);
   void    DeleteEnvMon(void *pData);

   int EVRegisterController(pEnvMon self, char *pName, void *pData,
                            SConnection *pCon);
   int EVUnregister(pEnvMon self, char *name);
   
   int EVMonitorControllers(pEnvMon self);

   int EVList(pEnvMon self, SConnection *pCon);

   int EVWrapper(SConnection *pCon, SicsInterp *pSics, void *pData,
                 int argc, char *argv[]);

   pEnvMon FindEMON(SicsInterp *pSics);
   
   int EnvMonTask(void *pEv);
   void EnvMonSignal(void *pEndv, int iSignal, void *pSigData);
@}

{\bf CreateEnvMon} initialises a emon data structure. As usual it returns
NULL on error, a pointer to a new structure on success.

{\bf DeleteEnvMon} deletes and emon data structure and frees all memory
associated with it. 

{\bf EVRegisterController} registers a controller for monitoring. The
parameters are:
\begin{itemize}
\item {\bf self}. A pointer to the environment monitor where to register the
controller.
\item {\bf pName}. The name of the controller object.
\item {\bf pData}. A pointer to the data structure belonging to the
controller object.
\item {\bf pCon}. A pointer to the client connection to which erros shall be
reported.
\end{itemize}

{\bf EVUnregister} Removes the controller name from the monitoring list.

{\bf EVMonitorControllers} is the function which will be called from the
main loop and which is responsible for monitoring the registered
controllers.


{\bf EVList} prints a status report about the emon to the Connection object
specified. 

{\bf EVWrapper} is the wrapper function necessary to make the emon
available from SICS. The user may interact with the emon through SICS. The
user will be enabled to list the currently registered environment
controllers and their current state and to register and unnregister
controllers.

{\bf EnvMonTask} is the task function associated with the environment
monitor. This is called by the task handling object and eventually
invokes EVMonitorControllers for checking on the environment
controllers configured into SICS. 

{\bf EnvMonSignal} is the signale handler for the environment monitor task.

@o emon.h -d @{
/*--------------------------------------------------------------------------
                E N V I R O N M E N T   M O N I T O R

  The environment monitor is responsible for monitoring active environment
  devices. 

  Mark Koennecke, Juli 1997

  copyright: see implementation file.
  doc: programmers reference.
----------------------------------------------------------------------------*/
#ifndef SICSEMON
#define SICSEMON
  typedef struct __EnvMon *pEnvMon;
/*-------------------------------------------------------------------------*/
@<emonint@>
#endif
@}

@o emon.i @{
/*--------------------------------------------------------------------------
   Internal header file defining the datastructure for the environment
   controller.

   Mark Koennecke, Juli 1997
---------------------------------------------------------------------------*/
@< emondat@>
@}

