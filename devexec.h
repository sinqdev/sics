
#line 204 "devexec.w"

/*----------------------------------------------------------------------------

                D E V I C E     E X E C U T O R

  Joachim Kohlbrecher wants to give several commands to the server
  and than wait for them to happen before proceeding. Actually a useful
  thing. A command will map to one or several positioning commands for a
  device. This scheme is also useful for implementing objects which
  drive several motors simulataneously, such as Monochromators. etc.
  However, the whole thing is rather complicated.
  
  It is forbidden, that several clients drive the instrument. In order
  to ensure this the Executor remembers the connection which emitted the
  first command. Subsequent AddExeEntries from different clients will
  be rejected. The owner will be reset when the execution is found finished
  in calls to CheckExe, Wait4Success or StopExe.
  
  
  Mark Koennecke, December 1996
  
  copyright: see implementation file
---------------------------------------------------------------------------*/
#ifndef SICSDEVEXEC
#define SICSDEVEXEC

#include "SCinter.h"
#include "conman.h"
#include "obdes.h"
#include "task.h"

  typedef struct __EXELIST  *pExeList;

/* Returncodes */

#define DEVDONE 1
#define DEVINT  0
#define DEVERROR 2
#define DEVBUSY  3

/* run level codes */
#define RUNRUN   0
#define RUNDRIVE 1
/*------------------------------------------------------------------------
                  B I R T H       &      D E A T H
*/
  pExeList CreateExeList(pTaskMan pTask);
  void DeleteExeList(void *self);
  
/* ================= Functions to talk to the above ====================== */

#line 43 "devexec.w"

  int StartDevice(pExeList self, char *name, pObjectDescriptor pDes,
                  void *pData, SConnection *pCon, int level, float fNew);
  int StartMotor(pExeList self, SicsInterp *pSics, SConnection *pCon,
                 char *name, int level, float fNew);
  int StartCounter(pExeList self, SicsInterp *pSics, SConnection *pCon,
                   int level, char *name);                

#line 251 "devexec.w"

/*------------------------------------------------------------------------*/

#line 89 "devexec.w"

  int CheckExeList(pExeList self);
  /*
    checks the entries for success and deletes entries which have finished
    operation. If there are none left, the pOwner will be set to NULL.
  */ 
  int Wait4Success(pExeList self);

   TaskTaskID GetDevexecID(pExeList self);

   int DevExecLevelRunning(pExeList self, int level);

  int DevExecTask(void *pEL);
  void DevExecSignal(void *pEL, int iSignal, void *pSigData);

  int GetDevExecInstStatus(pExeList self);


#line 253 "devexec.w"


  /*
    Waits for execution to finish. returns 1 on Success, 0 if problems
    ocurred. Than the Interrupt code shall be checked and acted upon
    accordingly.
  */

/*-------------------------------------------------------------------------*/
  SConnection *GetExeOwner(pExeList self);
/*-------------------------------------------------------------------------*/
  int isInRunMode(pExeList self);
/*--------------------------------------------------------------------------*/
  int ListPending(pExeList self, SConnection *pCon);
  /*
    lists the Operations still pending on pCon. 
  */
/*-------------------------------------------------------------------------*/

#line 144 "devexec.w"

  int StopExe(pExeList self, char *name);
  int StopExeWait(pExeList self);
   /*
     will stop the entry name and its subentries from executing.
     If ALL is specified as name, everything will be stopped and
     the Executor cleared.
     StopExeWait will stop all running things and wait for the stop
     to complete.
   */
  int StopByData(pExeList self, void *data);
  /* stop the entry with the given data from execution */
/*------------------------------------------------------------------------*/
  void ClearExecutor(pExeList self);
  /*
    clears the executor without sending commands to the devices.
  */
/*-----------------------------------------------------------------------*/
  int IsCounting(pExeList self);
  int PauseExecution(pExeList self);
  int ContinueExecution(pExeList self);


#line 271 "devexec.w"

/*-------------------------- Commands ------------------------------------*/
  int DevexecAction(SConnection *pCon, SicsInterp *pSics, void *pData,
              int argc, char *argv[]);
  int StopCommand(SConnection *pCon, SicsInterp *pSics, void *pData,
              int argc, char *argv[]);
  /*
   implements the stop command
  */
              
  int ListExe(SConnection *pCon, SicsInterp *pSics, void *pData,
              int argc, char *argv[]);
  /*
    lists all currently executing objects
  */
  int SicsIdle(SConnection *pCon, SicsInterp *pSics, void *pData,
              int argc, char *argv[]);
        /*
        prints the seconds since the device executor was running the last time
        */

  int Success(SConnection *pCon, SicsInterp *pSics, void *pData,
              int argc, char *argv[]);
  /*
    waits until completion of all pending operations. Used in
    connection with non blocking operation  such as motors started
    with run.
  */
  int PauseAction(SConnection *pCon, SicsInterp *pSics, void *pData,
              int argc, char *argv[]);
  /*
    pauses execution
  */
  int ContinueAction(SConnection *pCon, SicsInterp *pSics, void *pData,
              int argc, char *argv[]);
  /*
   continues execution
  */    
   
/*--------------------------- Locking ---------------------------------*/
 
#line 192 "devexec.w"
 
         void LockDeviceExecutor(pExeList self);
         void UnlockDeviceExecutor(pExeList self);
 
 
#line 311 "devexec.w"
 
/* -------------------------- Executor management -------------------------*/
        
        pExeList GetExecutor(void);
        void     SetExecutor(pExeList pExe);
        
  /**
  * This is only used in sicshdbadapter.c
  * It became a void pointer because of circular dependencies in the 
  * header files.
  */       
  void *GetExecutorCallback(pExeList self);
/*----------------------- Logging -----------------------------------------*/
   void DevexecLog(char *op, char *device);
   void ExeInterest(pExeList pExe, char *name, char *action);   
   void InvokeNewTarget(pExeList pExe, char *name, float fTarget);
   void SetDevexecStatus(pExeList pExe, int code);
#endif  
