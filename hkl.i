
/*-------------------------------------------------------------------------
                                H K L

  Internal data structure description. See hkl.h, c,w for more details.

  Mark Koennecke, February 1998
----------------------------------------------------------------------------*/

   typedef struct __HKL {
                         pObjectDescriptor pDes;
                         MATRIX UBinv;
                         int iManual;
                         double fLastHKL[5];
                         int iNOR;
                         int iQuad;
                         int iHM;
                         long lID;
                         float scanTolerance;
                         float targetHKL[3];
                         int targetDirty;
                         pIDrivable pMotDriv;
                         pIDrivable pMotList;
                         } HKL;


