/*-----------------------------------------------------------------------
                       D A T A N U M B E R
                      
   Implementation file for the data number module.
   
   Mark Koennecke, Juli 1997

   Added callbacks, stepping to next thousand 

   Mark Koennecke, December 2003
 

       Copyright:

       Labor fuer Neutronenstreuung
       Paul Scherrer Institut
       CH-5423 Villigen-PSI


      The authors hereby grant permission to use, copy, modify, distribute,
      and license this software and its documentation for any purpose, provided
      that existing copyright notices are retained in all copies and that this
      notice is included verbatim in any distributions. No written agreement,
      license, or royalty fee is required for any of the authorized uses.
      Modifications to this software may be copyrighted by their authors
      and need not follow the licensing terms described here, provided that
      the new terms are clearly indicated on the first page of each file where
      they apply.

      IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
      FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
      ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
      DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
      POSSIBILITY OF SUCH DAMAGE.

      THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
      INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
      FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
      IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
      NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
      MODIFICATIONS.
----------------------------------------------------------------------------*/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <assert.h>
#include "fortify.h"
#include "sics.h"
#include "danu.h"

/* ------------------ the data structure ----------------------------------*/
typedef struct __DataNumber {
  pObjectDescriptor pDes;
  pICallBack pCall;
  char *pFileName;
} DataNumber;
/*----------------------------------------------------------------------*/
static int readDataNumber(pDataNumber self)
{
  FILE *fd = NULL;
  int iNum = 0;

  /* open file */
  fd = fopen(self->pFileName, "r");
  if (!fd) {
    return -1;
  }

  /* get and increment number */
  fscanf(fd, "%d", &iNum);
  fclose(fd);
  return iNum;
}

/*-----------------------------------------------------------------------*/
 static int writeDataNumber(pDataNumber self, int iNum)
{
  FILE *fd = NULL;

  /* reopen for rewriting */
  fd = fopen(self->pFileName, "w");
  if (fd == NULL) {
    return -1;
  }

  /* write file and leave */
  fprintf(fd, " %d \n", iNum);
  fprintf(fd, "NEVER, EVER modify or delete this file\n");
  fprintf(fd,
          "You'll risk eternal damnation and a reincarnation as a cockroach!\n");
  fclose(fd);
  return 1;
}

/*------------------- The CallBack function for interest ------------------*/
static int InterestCallback(int iEvent, void *pEvent, void *pUser)
{
  pDataNumber self = NULL;
  SConnection *pCon = NULL;
  char pBueffel[132];
  int iNum;

  pCon = (SConnection *) pUser;
  if (pCon == NULL || !SCisConnected(pCon)) {
    return -1;
  }

  if (iEvent != VALUECHANGE) {
    return 1;
  }

  assert(pEvent);
  assert(pUser);

  self = (pDataNumber) pEvent;

  /*
     read number
   */
  iNum = readDataNumber(self);
  if (iNum > 0) {
    snprintf(pBueffel, 131, "sicsdatanumber = %d", iNum);
    SCWrite(pCon, pBueffel, eEvent);
  }
  return 1;
}
/*-------------------------------------------------------------------------*/
static void *GetDanuInterface(void *data, int interfaceID)
{
  pDataNumber self = (pDataNumber)data;
  if(self == NULL){
    return NULL;
  }
  if(interfaceID == CALLBACKINTERFACE){
    return self->pCall;
  }
  return NULL;
} 
/*-------------------------------------------------------------------------*/
pDataNumber CreateDataNumber(char *pFileName)
{
  pDataNumber pNew = NULL;
  FILE *fd = NULL;

  pNew = (pDataNumber) malloc(sizeof(DataNumber));
  if (!pNew) {
    return NULL;
  }
  memset(pNew, 0, sizeof(DataNumber));

  pNew->pDes = CreateDescriptor("DataNumber");
  pNew->pDes->GetInterface = GetDanuInterface;
  pNew->pCall = CreateCallBackInterface();
  if (!pNew->pDes || !pNew->pCall) {
    free(pNew);
    return NULL;
  }

  /* check filename */
  fd = fopen(pFileName, "r");
  if (!fd) {
    printf("Serious error: cannot open file for Data Number!!!!\n");
    printf("I continue, but you should not write data files!\n");
    pNew->pFileName = strdup("default.num");
    return pNew;
  }
  fclose(fd);
  pNew->pFileName = strdup(pFileName);
  return pNew;
}

/*--------------------------------------------------------------------------*/
void DeleteDataNumber(void *pData)
{
  pDataNumber self = NULL;

  self = (pDataNumber) pData;
  assert(self);

  if (self->pDes) {
    DeleteDescriptor(self->pDes);
  }
  if (self->pCall) {
    DeleteCallBackInterface(self->pCall);
    self->pCall = NULL;
  }
  if (self->pFileName) {
    free(self->pFileName);
  }
  free(self);
}

/*-------------------------------------------------------------------------*/
int IncrementDataNumber(pDataNumber self, int *iYear)
{
  FILE *fd = NULL;
  int iNum;
  time_t iTime;
  struct tm *psTime;


  iNum = readDataNumber(self);
  if (iNum < 0) {
    return iNum;
  }

  iNum++;

  /* get year */
  iTime = time(NULL);
  psTime = localtime(&iTime);
  *iYear = psTime->tm_year + 1900;

  if (writeDataNumber(self, iNum) < 0) {
    return -1;
  }

  InvokeCallBack(self->pCall, VALUECHANGE, self);

  return iNum;
}

/*-------------------------------------------------------------------------*/
int DecrementDataNumber(pDataNumber self)
{
  FILE *fd = NULL;
  int iNum, currentThousand;

  iNum = readDataNumber(self);
  if (iNum < 0) {
    return iNum;
  }

  /*
     decrement DataNumber with restrictions:
     - not at all lower 0
     - do not understep a thousand boundary
   */
  currentThousand = (int) floor(iNum / 1000.);
  iNum--;
  if ((int) floor(iNum / 1000.) < currentThousand) {
    iNum++;
  }

  if (writeDataNumber(self, iNum) < 0) {
    return -1;
  }

  return iNum;
}

/*------------------------------------------------------------------------*/
int NewThousand(pDataNumber self)
{
  int iNum, currentThousand;

  iNum = readDataNumber(self);
  if (iNum < 0) {
    return iNum;
  }

  /* set to next thousand number */
  currentThousand = (int) floor(iNum / 1000.);
  iNum = (currentThousand + 1) * 1000;

  if (writeDataNumber(self, iNum) < 0) {
    return -1;
  }

  return iNum;
}

/*-------------------------------------------------------------------------*/
int DNWrapper(SConnection * pCon, SicsInterp * pSics, void *pData,
              int argc, char *argv[])
{
  pDataNumber self = NULL;
  FILE *fd = NULL;
  int iNum, iYear;
  char pBueffel[512];
  long lID;

  self = (pDataNumber) pData;
  assert(self);
  assert(pCon);

  argtolower(argc, argv);
  if (argc < 2) {               /* value request */
    iNum = readDataNumber(self);
    if (iNum < 0) {
      snprintf(pBueffel,511, "ERROR: cannot open file %s", self->pFileName);
      SCWrite(pCon, pBueffel, eError);
      return 0;
    }
    sprintf(pBueffel, "%s = %d", argv[0], iNum);
    SCWrite(pCon, pBueffel, eValue);
    return 1;
  }

  if (strcmp(argv[1], "incr") == 0) {
    iNum = IncrementDataNumber(self, &iYear);
    if (iNum > 0) {
      SCSendOK(pCon);
      return 1;
    } else {
      sprintf(pBueffel, "ERROR: cannot increment %s", argv[0]);
      SCWrite(pCon, pBueffel, eError);
      return 0;
    }
  }
  if (strcmp(argv[1], "nextthousand") == 0) {
    if (!SCMatchRights(pCon, usMugger)) {
      return 0;
    }
    iNum = NewThousand(self);
    if (iNum > 0) {
      SCSendOK(pCon);
      return 1;
    } else {
      sprintf(pBueffel, "ERROR: cannot increment %s", argv[0]);
      SCWrite(pCon, pBueffel, eError);
      return 0;
    }
  }
  if (strcmp(argv[1], "interest") == 0) {
    lID = RegisterCallback(self->pCall,
                           VALUECHANGE, InterestCallback,
                           SCCopyConnection(pCon), SCDeleteConnection);
    SCSendOK(pCon);
    return 1;
  }
  if (strcmp(argv[1], "uninterest") == 0) {
    RemoveCallbackCon(self->pCall, pCon);
    SCSendOK(pCon);
  }

  snprintf(pBueffel,511, "ERROR: unknown command %s supplied to %s",
          argv[1], argv[0]);
  SCWrite(pCon, pBueffel, eError);
  return 0;
}

/*------------------------------------------------------------------------*/
int DEWrapper(SConnection * pCon, SicsInterp * pSics, void *pData,
              int argc, char *argv[])
{
  pDataNumber self = NULL;
  int iNum;
  char pBueffel[512];

  self = (pDataNumber) pData;
  assert(self);
  assert(pCon);

  if (SCMatchRights(pCon, usMugger)) {
    iNum = DecrementDataNumber(self);
    snprintf(pBueffel, 511, "Data file %d killed", iNum + 1);
    SCWrite(pCon, pBueffel, eWarning);
    return 1;
  } else {
    SCWrite(pCon, "ERROR: you are not authorized to kill data files",
            eError);
    return 0;
  }
}

/*-------------------------------------------------------------------------*/
int DNFactory(SConnection * pCon, SicsInterp * pSics, void *pData,
              int argc, char *argv[])
{
  pDataNumber self = NULL;
  char pBueffel[512];
  int iRet;

  if (argc < 3) {
    SCWrite(pCon,
            "ERROR: not enough arguments provided to make DataNumber",
            eError);
    return 0;
  }

  self = CreateDataNumber(argv[2]);
  if (!self) {
    SCWrite(pCon, "ERROR: no memory to create data number", eError);
    return 0;
  }

  iRet = AddCommand(pSics, argv[1], DNWrapper, DeleteDataNumber, self);
  if (!iRet) {
    snprintf(pBueffel, 511,"ERROR: duplicate command %s not created", argv[1]);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }
  iRet = AddCommand(pSics, "killfile", DEWrapper, NULL, self);
  if (!iRet) {
    snprintf(pBueffel, 511,"ERROR: duplicate command %s not created", argv[1]);
    SCWrite(pCon, pBueffel, eError);
    return 0;
  }
  return 1;
}
