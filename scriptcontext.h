#ifndef SCRIPTCONTEXT_H
#define SCRIPTCONTEXT_H

#include "sics.h"
#include "devser.h"

/* \brief an sct controller
 */
typedef struct SctController SctController;

/** \brief queue node action to a controller
 *
 * \param controller the controller
 * \param node the node
 * \param prio the priority
 * \param action the initial state
 * \param con an optional connection to be used by the action scripts
 */
void SctQueueNode(SctController * controller, Hdb * node,
                  DevPrio prio, char *action, SConnection * con);

/** \brief call a script and configure the sct command to be used
 * in connection with the given node and controller
 *
 * \param con the connection
 * \param script a tcl script
 * \param the node to which the sct command 
 * \return 0 when this was a new action, > 0 when an action was overwritten
 */
int SctCallInContext(SConnection * con, char *script, Hdb * node,
                     SctController * controller, char **resPtr);
/**
 * get the controller debug connection (if any)
 * \param c The SctController
 * \return the connection for verbose, NULL for silent
 */
SConnection *SctDebugConn(SctController * c);
/**
 * retrieve the device serializer
 * \param data A pointer to a SICSObject representing a SctController
 * \param A pointer the the device serializer
 */
DevSer *SctGetDevser(void *data);
#endif
