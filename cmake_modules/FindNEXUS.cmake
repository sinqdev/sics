# NEXUS_FOUND         - TRUE if NeXus found
# NEXUS_INCLUDE_DIRS  - include directories
# NEXUS_LIBRARIES     - library directories

find_package(PkgConfig)
pkg_check_modules(PC_NEXUS nexus)


set(NEXUS_LIBRARIES ${PC_NEXUS_LIBRARIES})
set(NEXUS_LIBRARY_DIRS ${PC_NEXUS_LIBRARY_DIRS})
set(NEXUS_INCLUDE_DIRS ${PC_NEXUS_INCLUDE_DIRS})

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(NEXUS DEFAULT_MSG NEXUS_LIBRARIES NEXUS_LIBRARY_DIRS NEXUS_INCLUDE_DIRS)

mark_as_advanced(NEXUS_INCLUDE_DIRS NEXUS_LIBRARIES)
