/*------------------------------------------------------------------------
                      E U R O D R I V
  
  A environment control driver for the SANS Eurotherm temperature
  controller.

  Mark Koennecke, May 1999 
   
   copyright: see copyright.h
---------------------------------------------------------------------------*/
#ifndef EURODRIV
#define EURODRIV
pEVDriver CreateEURODriv(int argc, char *argv[]);

      /* 
         these are hooks to implement further functionality which, 
         I'am sure, Joachim Kohlbrecher will request.
       */
int EuroGetParameter(void **pData, char *pPar, int iLen, float *fVal);
int EuroSetParameter(void **pData, char *pPar, int iLen,
                     char *pFormat, float fVal);

#endif
