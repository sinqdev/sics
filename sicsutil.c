  /**
 * Some general functions for SICS. Most part is moved from ofac.c
 * 
 * copyright: see file COPYRIGHT
 * 
 * moved from acces.c and ascon.c Markus Zolliker Jan 2010
 */
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/time.h>
#include "fortify.h"
#include "sics.h"
#include "HistMem.h"
#include "sicsdata.h"
#include "SCinter.h"
#include "sicshipadaba.h"
#include "sicsget.h"

static char *aCode[] = {
  "internal",
  "mugger",
  "user",
  "spy",
  NULL
};
static int iCodes = 4;
/*--------------------------------------------------------------------------*/
int decodeSICSPriv(char *privText)
{
  int code = 0;

  strtolower(privText);
  if(strcmp(privText,"manager") == 0){
	  return 1;
  }
  while (aCode[code] != NULL) {
    if (strcmp(aCode[code], privText) == 0) {
      return code;
    }
    code++;
  }
  if (code >= iCodes) {
    return -1;
  }
  return -1;
}


/*-------------------------------------------------------------------------*/
double DoubleTime(void)
{
  struct timeval now;
  /* the resolution of this function is usec, if the machine supports this
     and the mantissa of a double is 51 bits or more (31 bits for seconds
     and 20 for microseconds)
   */
  gettimeofday(&now, NULL);
  return now.tv_sec + now.tv_usec / 1e6;
}
/*--------------------------------------------------------------------------*/
unsigned short fletcher16( char *data, size_t len)
{
        unsigned short sum1 = 0xff, sum2 = 0xff, result;
        unsigned char checkA, checkB;

        if(data == NULL){
        	return 0;
        }
        while (len) {
                size_t tlen = len > 21 ? 21 : len;
                len -= tlen;
                do {
                        sum1 += *data++;
                        sum2 += sum1;
                } while (--tlen);
                sum1 = (sum1 & 0xff) + (sum1 >> 8);
                sum2 = (sum2 & 0xff) + (sum2 >> 8);
        }
        /* Second reduction step to reduce sums to 8 bits */
        sum1 = (sum1 & 0xff) + (sum1 >> 8);
        sum2 = (sum2 & 0xff) + (sum2 >> 8);
        checkA = (unsigned char)sum1;
        checkB = (unsigned char)sum2;
        result = checkA;
        result = result << 8 | checkB;
        return result ;
}
/*--------------------------------------------------------------------------*/
int CheckSum(SConnection * pCon, SicsInterp * pSics, void *pData,
                     int argc, char *argv[])
{
	pHdb node = NULL;
	unsigned short checksum;
	pObjectDescriptor pDes = NULL;
	pHistMem pHM;
	int length;
	char *data = NULL;
	pSICSData sidata = NULL;

	if(argc < 2){
		SCWrite(pCon,"ERROR: need object to checksum", eError);
		return 0;
	}

	node = FindHdbNode(NULL,argv[1],NULL);
	if(node != NULL){
		checksum = getHdbCheckSum(&node->value);
		SCPrintf(pCon,eValue,"%d", checksum);
		return 1;
	}

	pDes  = FindCommandDescriptor(pSics, argv[1]);
	if(pDes == NULL){
		SCPrintf(pCon,eError,"ERROR: object %s not found", argv[0]);
		return 0;
	}

	if(strcmp(pDes->name, "HistMem") == 0){
		pHM = (pHistMem)FindCommandData(pSics,argv[1],"HistMem");
		if(argc < 3){
			length = GetHistLength(pHM);
			data = (char *)GetHistogramPointer(pHM,pCon);
			checksum = fletcher16(data,length*sizeof(HistInt));
			SCPrintf(pCon,eValue,"%d", checksum);
			return 1;
		} else {
			data = (char *)GetHistTimeBin(pHM,&length);
			checksum = fletcher16(data,length*sizeof(float));
			SCPrintf(pCon,eValue,"%d", checksum);
			return 1;
		}
	}

	if(strcmp(pDes->name, "SICSData") == 0){
		sidata = (pSICSData)FindCommandData(pSics,argv[1],"SICSData");
		checksum = fletcher16((char *)sidata->data, sidata->dataUsed*sizeof(int));
		SCPrintf(pCon,eValue,"%d", checksum);
		return 1;
	}
	SCWrite(pCon,"ERROR: object type not recognized", eError);
	return 0;
}
/*-----------------------------------------------------------------------------*/
void LogUserInfo(void *pData)
{
  hdbValue user, proposal;
  SConnection *pCon = (SConnection *)pData;
  pDynString data;

  if(pData == NULL){
    return;
  }
  user = MakeHdbText(strdup("Martina Notconfigured"));
  proposal = MakeHdbText(strdup("Unproposed"));

  sget("user",&user);
  sget("proposalid",&proposal);
  data = formatValue(proposal,NULL);
  SCPrintf(pCon,eLog,"WARNING: Saving data for %s, proposal %s", user.v.text, 
	   GetCharArray(data));
  DeleteDynString(data);
  ReleaseHdbValue(&user);
  ReleaseHdbValue(&proposal);

}
/*--------------------------------------------------------------------------*/
int LogUserInfoWrapper(SConnection * pCon, SicsInterp * pSics, void *pData,
                     int argc, char *argv[])
{
  LogUserInfo(pCon);
  return 1;
} 

