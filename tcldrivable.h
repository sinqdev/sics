
/*-----------------------------------------------------------------------
  The code in this file allows to override or define Drivable
  interfaces through tcl scripts. More more details, see
  tcldrivable.tex

  copyright: see file COPYRIGHT
  
  Mark Koennecke, September 2003
  ----------------------------------------------------------------------*/
#ifndef SICSTCLDRIVABLE
#define SICSTCLDRIVABLE

/*
  Function codes for  functionIndex
*/
#define TCLHALT    1
#define TCLCHECK   2
#define TCLSET     3
#define TCLSTATUS  4
#define TCLGET     5

  /*
     This installs a Tcl replacement for a drivable function
   */
int TclReplaceDrivable(SConnection * pCon, SicsInterp * pSics,
                       void *pData, int argc, char *argv[]);
  /*
     This allows to invoke a replaced function for debugging
     purposes
   */
int TclDrivableInvoke(SConnection * pCon, SicsInterp * pSics,
                      void *pData, int argc, char *argv[]);

  /*
     This is for use from C
   */
int replaceDrivableByTcl(void *sicsObject, int functionIndex,
                         char *scriptName, char *tclName);

  /*
     map name to a functionIndex for use in replaceDrivableByTcl.
     Returns 0 on failure, a usefule index > 0 in the case of success
   */
int mapDrivableFunctionNames(char *name);

  /*
     This is called from StopServer/nserver.c in order to remove
     all memory related to this class
   */
void killTclDrivable(void);
#endif
