/*---------------------------------------------------------------------------
                        S E L V A R
                        
   This is closely related to a crystla monochromator. A crystal monochromator
   can be used to select a wavelength or an energy. In Sics these three things
   are kept separate. The actual monochromator and its variables are dealt 
   with in selector.*. This file contains prototypes for functions to deal
   with Wavelength and Energy variables.
   
   Mark Koennecke, January 1996
   
   updated to support callbacks. Mark Koennecke, February 1998
   
   copyright: see implementation file.
----------------------------------------------------------------------------*/
#ifndef SICSSELVAR
#define SICSSELVAR

typedef struct __SelVar *pSelVar;

/*------------------------ birth and death --------------------------------*/
pSelVar CreateWLVar(char *name, pSicsSelector pSel);
pSelVar CreateEnergy(char *name, pSicsSelector pSel);
void DeleteSelVar(void *pSelf);
/* ----------------------- Factory functions for initialization -----------*/
int MakeWaveLengthVar(SConnection * pCon, SicsInterp * pSics, void *pData,
                      int argc, char *argv[]);
int MakeEnergyVar(SConnection * pCon, SicsInterp * pSics, void *pData,
                  int argc, char *argv[]);
/*-------------------------------------------------------------------------*/
float GetSelValue(pSelVar self, SConnection * pCon);
/*------------------------ Action functions -------------------------------*/
int WaveLengthAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                     int argc, char *argv[]);
int EnergyAction(SConnection * pCon, SicsInterp * pSics, void *pData,
                 int argc, char *argv[]);
#endif
