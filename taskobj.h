/**
 *  This is the SICS interface object to the tasker module
 *
 * copyright: GPL
 * 
 * Mark Koennecke, December 2012
 */
#ifndef TASKOBJ_
#define TASOBJ_
int TaskOBJFactory(SConnection *pCon, SicsInterp *pSics, void *pData, 
		int argc, char *argv[]);

#endif /*TASKOBJ_*/
