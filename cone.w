\subsection{Cone}
This module is a virtual motor for driving on a cone around a give center reflection. 
The application is in four circle diffractometry. Consider, a reflection has been 
found and indexed. If the cell parameters are known, we now know that at a certain 
two theta and at a certain agle around the first reflection there should be 
another one. Serching this cone would speed up UB matrix determination. Driving on such
a cone and therewith scanning it is implemented in this module. 

The math for doing this is detailed in a separate internal paper: conescan.tex.
For its implementation, cone works very closely with the UBCALC module. UBCALC 
 provides the cell constants and it also provides the list of reflections from 
 which to get the center reflection of the cone.

The module requires a data structure:
@d conedat @{
typedef struct {
	pObjectDescriptor pDes;
	pIDrivable pDriv;
	reflection target;
	float lastConeAngle;
	float qScale;
    pUBCALC ubi;
    int center;
    pHKL pHkl;
} coneData, *pConeData;
@} 
The fields are:
\begin{description}
\item[pDes] The standard object descriptor
\item[pDriv] The drivable interface which implements most of this modules 
 functionality.
 \item[lastConeAngle] The last cone angle set. Used instead of the not implemented 
  back calculation.
 \item[qScale] An adaptor factor to multiply onto the scattering vector length. This is 
  in order to find the target reflection even if the lattice constants are inaccurate. 
\item[target] The target reflection of the cone. This determines the length
 of the scattering vector and the opening angle of the cone.
\item[ubi] A pointer to the UBCALC module which holds things like lattice constants. 
 \item[center] The reflection number in UBCALCS reflection list to use as the
  center of the cone. 
\end{description}

The external interface to cone is mostly defined by the drivable interface. In 
addition there are only the usal interpreter interface functions to install and
configure cone.

@o cone.h @{
/*----------------------------------------------------------------------
  SICS cone module for cone scans. Form more details see conescan.tex
  and cone.tex.
  
  COPYRIGHT: see file COPYRIGHT
  
  Mark Koennecke, March 2006
------------------------------------------------------------------------*/
#ifndef SICSCONE
#define SICSCONE
#include "sics.h"
#include "ubcalc.h"

/*-----------------------------------------------------------------------*/
@<conedat@>
/*----------------------------------------------------------------------*/
int MakeCone(SConnection *pCon, SicsInterp *pSics, void *pData,
	int argc, char *argv[]);
int ConeAction(SConnection *pCon, SicsInterp *pSics, void *pData,
	int argc, char *argv[]);
#endif
@}
