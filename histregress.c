/*----------------------------------------------------------------------------

				H I S T S I M
				
  A simulated histogram memory for regression tests. 
 
  All the counting error stuff is redirected to a regression counter; see
  documentation there. This just adds data handling.
  
 copyright: see file COPYRIGHT
 
 Mark Koennecke, October 2006 
 ----------------------------------------------------------------------------*/
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include "fortify.h"
#include "sics.h"
#include "countdriv.h"
#include "counter.h"
#include "stringdict.h"
#include "HistMem.h"
#include "HistDriv.i"
#include "histsim.h"

static int iSet = 0;
static HistInt iSetVal = 0;
static HistMode eHistMode;
/*--------------------------------------------------------------------------*/
static int RegressConfig(pHistDriver self, SConnection * pCon,
                         pStringDict pOption, SicsInterp * pSics)
{
  int i, iLength = 1, status;
  char pData[132];
  float fFail;
  pCounterDriver count;

  count = (pCounterDriver) self->pPriv;

  if (eHistMode == eHTOF) {
    for (i = 0; i < self->data->rank; i++) {
      iLength *= self->data->iDim[i];
    }
    iLength *= self->data->nTimeChan;
  }

  /*
     deal with error settings
   */
  status = StringDictGet(pOption, "errortype", pData, 131);
  if (status) {

    fFail = atof(pData);
    count->Set(count, "errortype", 1, fFail);
  }
  status = StringDictGet(pOption, "recover", pData, 131);
  if (status) {

    fFail = atof(pData);
    count->Set(count, "recover", 1, fFail);
  }
  status = StringDictGet(pOption, "finish", pData, 131);
  if (status) {

    fFail = atof(pData);
    count->Set(count, "finish", 1, fFail);
  }

  /*
     configured test value
   */
  status = StringDictGet(pOption, "testval", pData, 131);
  if (status) {
    iSet = 1;
    iSetVal = atoi(pData);
  }

  return 1;
}

/*-------------------------------------------------------------------------*/
static int RegressStart(pHistDriver self, SConnection * pCon)
{
  pCounterDriver pDriv;

  pDriv = (pCounterDriver) self->pPriv;
  pDriv->fPreset = self->fCountPreset;
  pDriv->eMode = self->eCount;
  return pDriv->Start(pDriv);
}

/*-------------------------------------------------------------------------*/
static int RegressPause(pHistDriver self, SConnection * pCon)
{
  pCounterDriver pDriv;

  pDriv = (pCounterDriver) self->pPriv;
  pDriv->fPreset = self->fCountPreset;
  pDriv->eMode = self->eCount;
  return pDriv->Pause(pDriv);
}

/*------------------------------------------------------------------------*/
static int RegressContinue(pHistDriver self, SConnection * pCon)
{
  pCounterDriver pDriv;

  pDriv = (pCounterDriver) self->pPriv;
  pDriv->fPreset = self->fCountPreset;
  pDriv->eMode = self->eCount;
  return pDriv->Continue(pDriv);
}

/*-------------------------------------------------------------------------*/
static int RegressHalt(pHistDriver self)
{
  pCounterDriver pDriv;

  pDriv = (pCounterDriver) self->pPriv;
  return pDriv->Halt(pDriv);
}

/*-------------------------------------------------------------------------*/
static int RegressGetCountStatus(pHistDriver self, SConnection * pCon)
{
  pCounterDriver pDriv;
  float fControl;

  pDriv = (pCounterDriver) self->pPriv;
  return pDriv->GetStatus(pDriv, &fControl);
}

/*-------------------------------------------------------------------------*/
static int RegressGetError(pHistDriver self, int *iCode, char *pError,
                           int iLen)
{
  pCounterDriver pDriv;

  pDriv = (pCounterDriver) self->pPriv;
  return pDriv->GetError(pDriv, iCode, pError, iLen);
}

/*-------------------------------------------------------------------------*/
static int RegressTryAndFixIt(pHistDriver self, int iCode)
{
  pCounterDriver pDriv;

  pDriv = (pCounterDriver) self->pPriv;
  return pDriv->TryAndFixIt(pDriv, iCode);
}

/*--------------------------------------------------------------------------*/
static int RegressGetData(pHistDriver self, SConnection * pCon)
{
  pCounterDriver pDriv;

  pDriv = (pCounterDriver) self->pPriv;

  return pDriv->ReadValues(pDriv);
}

/*--------------------------------------------------------------------------*/
static int RegressGetHistogram(pHistDriver self, SConnection * pCon,
                               int i, int iStart, int iEnd,
                               HistInt * lData)
{
  int ii;

  if (i < 0) {
    SCWrite(pCon, "ERROR: histogram out of range", eError);
    return 0;
  }

  if (iSet == 1) {
    for (ii = iStart; ii < iEnd; ii++) {
      lData[ii - iStart] = iSetVal;
    }
  } else {
    for (ii = iStart; ii < iEnd; ii++) {
      lData[ii - iStart] = random();
    }
  }
  return 1;
}

/*------------------------------------------------------------------------*/
static int RegressSetHistogram(pHistDriver self, SConnection * pCon,
                               int i, int iStart, int iEnd,
                               HistInt * lData)
{
  iSet = 1;
  iSetVal = lData[0];
  return 1;
}

/*-------------------------------------------------------------------------*/
static int RegressPreset(pHistDriver self, SConnection * pCon,
                         HistInt iVal)
{
  iSet = 1;
  iSetVal = iVal;
  return 1;
}

/*------------------------------------------------------------------------*/
static int RegressFreePrivate(pHistDriver self)
{
  pCounterDriver pDriv;

  pDriv = (pCounterDriver) self->pPriv;
  DeleteCounterDriver(pDriv);
  return 1;
}

/*------------------------------------------------------------------------*/
static long RegressGetMonitor(pHistDriver self, int i, SConnection * pCon)
{
  pCounterDriver pDriv;
  long lVal;

  pDriv = (pCounterDriver) self->pPriv;
  return pDriv->lCounts[i];

}

/*------------------------------------------------------------------------*/
static float RegressGetTime(pHistDriver self, SConnection * pCon)
{
  pCounterDriver pDriv;
  long lVal;

  pDriv = (pCounterDriver) self->pPriv;
  return pDriv->fTime;

}

/*-------------------------------------------------------------------------*/
pHistDriver CreateRegressHM(pStringDict pOpt)
{
  pHistDriver pNew = NULL;

  /* create the general driver */
  pNew = CreateHistDriver(pOpt);
  if (!pNew) {
    return NULL;
  }

  /* put a Regresscounter in */
  pNew->pPriv = (void *) NewRegressCounter("HistoRegress");
  if (!pNew->pPriv) {
    DeleteHistDriver(pNew);
    return NULL;
  }

  /* configure all those functions */
  pNew->Configure = RegressConfig;
  pNew->Start = RegressStart;
  pNew->Halt = RegressHalt;
  pNew->GetCountStatus = RegressGetCountStatus;
  pNew->GetError = RegressGetError;
  pNew->TryAndFixIt = RegressTryAndFixIt;
  pNew->GetData = RegressGetData;
  pNew->GetHistogram = RegressGetHistogram;
  pNew->SetHistogram = RegressSetHistogram;
  pNew->GetMonitor = RegressGetMonitor;
  pNew->GetTime = RegressGetTime;
  pNew->Preset = RegressPreset;
  pNew->FreePrivate = RegressFreePrivate;
  pNew->Pause = RegressPause;
  pNew->Continue = RegressContinue;
  StringDictAddPair(pOpt, "errortype", "0");
  StringDictAddPair(pOpt, "recover", "1");
  StringDictAddPair(pOpt, "testval", "0");

  return pNew;
}
