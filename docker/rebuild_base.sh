#!/bin/bash
set -x
docker rmi local/c7-base
docker build --rm \
  --build-arg HTTP_PROXY=${http_proxy} \
  --tag=local/c7-base \
  --file=Dockerfile_base .
