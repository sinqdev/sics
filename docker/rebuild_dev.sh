#!/bin/bash
set -x
docker stop dev7
docker rm dev7
docker rmi local/c7-dev
docker build --rm \
  --volume=${PWD}/rpm:/rpm:ro \
  --build-arg UID=$(id -u) \
  --build-arg GID=$(id -g) \
  --file=Dockerfile_dev \
  --tag=local/c7-dev .
docker run -d -it --name dev7 \
  -v ${HOME}:/home/sics \
  -v /etc/localtime:/etc/localtime:ro \
  --hostname dev7 \
  --user=sics \
  --workdir=/home/sics \
  local/c7-dev


